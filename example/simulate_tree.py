# -*- python -*-
# -*- coding: utf-8 -*-
#
#		example/simulate_tree
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
	This example file shows how to generate a random tree
"""
from treex.tree import *
from treex.simulation import gen_random_tree, galton_watson

# Example 1: tree with 20 nodes
print('Example 1')

t = gen_random_tree(20)
print( t.get_attribute() )

print('-------------------------------------')

# Example 2: tree with 20 nodes and outdegree less than 2
print('Example 2')

t = gen_random_tree(20,2)
print( t.get_attribute() )

print('-------------------------------------')

# Example 3: Galton-Watson tree with birth distribution [0.3,0.3,0.4]
print('Example 3')

t = galton_watson('',[0.3,0.3,0.4],max_generation = 20)
print(t)

print('-------------------------------------')

# Example 4: Galton-Watson tree with Poisson birth distribution
print('Example 4')

t = galton_watson('poisson',2,max_generation = 5)
print(t)

# galton_watson('geometric',0.5,max_generation = 5)
# galton_watson('geometric with shift',0.5,max_generation = 5)
# galton_watson('geometric with dirac',[0.1,0.6],max_generation = 5)
# galton_watson('binomial',[5,0.2],max_generation = 5)