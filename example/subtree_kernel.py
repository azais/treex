# -*- python -*-
# -*- coding: utf-8 -*-
#
#       test/test_subtree_kernel
#
#       File author(s):
#           Florian Ingels <florian.ingels@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex import *

# We start creating a (fictional) data set of trees

tree_forest = [gen_random_tree(50) for i in range(15)]

# We convert the tree forest into a DAG forest. Here, we choose to consider ordered trees. Due to the artificial
# nature of this example, we also assign fake origins and classes to our dataset. There will be 15 trees of class
# 'a', 15 of class 'b' and 15 of class 'c'.

classes = {'a': [], 'b': [], 'c': []}
dag_list = []

for i in range(5):
    t = tree_forest.pop()
    classes['a'].append('a_' + str(i))
    dag_list.append(tree_to_dag(t, tree_type='ordered', origin='a_' + str(i)))

    t = tree_forest.pop()
    classes['b'].append('b_' + str(i))
    dag_list.append(tree_to_dag(t, tree_type='ordered', origin='b_' + str(i)))

    t = tree_forest.pop()
    classes['c'].append('c_' + str(i))
    dag_list.append(tree_to_dag(t, tree_type='ordered', origin='c_' + str(i)))

# We now construct the recompressed DAG containing all the information of the dataset

dag = Dag()
dag.concatenate(dag_list)
dag.compress(tree_type='ordered') # here we stay consistent with the choice of ordered trees

# We now create a SubtreeKernelData object to process with

kd = SubtreeKernelData(dag, classes)

# To compute the standard subtree kernel, we have to split the data into half. Here the process is randomized.

s_train, s_pred = kd.sample_origins(2)

# We compute the gram matrices that will serve in the SVM classifier

gram_train = kd.compute_kernel(s_train, s_train)
gram_pred = kd.compute_kernel(s_pred, s_train)

# Here is the prediction for the testing data

s_predicted = svm_predict(gram_train, gram_pred, s_train, s_pred)

print(performance_measure(s_pred, s_predicted, 'macro'))

# We want to visualize the 'gram_train' matrix

fig, axes = plt.subplots(ncols=3)
view_heatmap(gram_train, s_train, axes[0])
axes[0].set_title('No normalization\n+ exponential decay')

# We will now do the same and normalize the kernels.

gram_train = kd.compute_kernel(s_train, s_train,normalized=True,kernel_function=polynomial_kernel, kernel_param=(1, 0),
                   weight_function=exponential_weight, weight_param=('height', 0.7))
gram_pred = kd.compute_kernel(s_pred, s_train,normalized=True,kernel_function=polynomial_kernel, kernel_param=(1, 0),
                   weight_function=exponential_weight, weight_param=('height', 0.7))

s_predicted = svm_predict(gram_train, gram_pred, s_train, s_pred)

print(performance_measure(s_pred, s_predicted, 'macro'))

view_heatmap(gram_train, s_train, axes[1])
axes[1].set_title('Normalized \n+ exponential decay')

# We continue with the subtree kernel using discriminance. To avoid overfitting, we split the data into 3

s_discr, s_train, s_pred = kd.sample_origins(3)

kd.assign_discriminance(s_discr) # We have to compute the discriminance

gram_train = kd.compute_kernel(s_train, s_train,normalized=True, kernel_function=polynomial_kernel, kernel_param=(1, 0),
                   discriminance=True)
gram_pred = kd.compute_kernel(s_pred, s_train,normalized=True, kernel_function=polynomial_kernel, kernel_param=(1, 0),
                   discriminance=True)

s_predicted = svm_predict(gram_train, gram_pred, s_train, s_pred)

print(performance_measure(s_pred, s_predicted, 'macro'))

view_heatmap(gram_train, s_train, axes[2])
axes[2].set_title('Normalized \n + discriminance')
fig.set_size_inches(10, 3)
fig.tight_layout()
fig.show()

# To visualize how the discriminance is distributed along the dag (based on the previous computation)

assign_discriminance_nodescale(dag)
color_dic = assign_discriminance_color(dag)
print(
    color_dic)  # This dictionary contains the information of which color in the next figure corresponds to which class

figure = view_dag(dag, node_scale=200, alpha=0.05)
figure.suptitle(
    'Recompressed DAG colored by the class that is best discriminated by the node, \nand scaled by the value of discriminance')
figure.set_size_inches(10, 5)
figure.tight_layout()
figure.show()
