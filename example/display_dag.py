# -*- python -*-
# -*- coding: utf-8 -*-
#
#		example/display_dag
#
#       File author(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
	This example file shows how to display a DAG
"""

# This script should be run in interactive mode

from treex.tree import *
from treex.dag import *
from treex.simulation import gen_random_tree
from treex.visualization.matplotlib_plot import view_dag, view_tree
from treex.visualization.options import assign_color_to_dag, assign_color_to_dag, assign_class_color_to_tree

# Example 1: display a non-optimal dag
print('Example 1')

t = gen_random_tree(100)
d = tree_to_treedag(t)

figure = view_dag(d,node_scale=40)
figure.set_size_inches(10,30)
figure.tight_layout()
# figure.savefig('dag_ex1.pdf') # if you want to save the file
figure.show()

print('-------------------------------------')

# Example 2: display an optimal dag
print('Example 2')

t = gen_random_tree(200)
d = tree_to_optimaldag(t,tree_type='unordered')

figure = view_dag(d,direction = 'up',node_scale=50)
figure.set_size_inches(10,30)
figure.tight_layout()
figure.show()

print('-------------------------------------')

# Example 3: display dag and associated tree with compatible colors
print('Example 3')

t = gen_random_tree(200)
d = tree_to_optimaldag(t,tree_type='unordered')

assign_class_color_to_tree(t)
assign_color_to_dag(d)

figure = view_tree(t,direction = 'left',node_scale=50)
figure.set_size_inches(10,30)
figure.tight_layout()
figure.show()

figure = view_dag(d,direction = 'left',node_scale=50)
figure.set_size_inches(10,30)
figure.tight_layout()
figure.show()