# -*- python -*-
# -*- coding: utf-8 -*-
#
#		example/sn_approximations
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
	This example file shows how to compute some selfnested approximations of a tree
"""

from treex.tree import *

from treex.selfnestedness.heightprofile import *
from treex.selfnestedness.nest import nest_min_max, nest_min
from treex.selfnestedness.averaging import averaging, sequential_averaging
from treex.selfnestedness.simulation import gen_random_selfnested_tree

from treex.simulation import gen_random_tree

from treex.edit_distance.add_del_leaf import constrained_unordered_edit_distance

print('Example 1')

t = gen_random_tree(20)

[nest_min_t , nest_max_t] = nest_min_max(t)

av_t = averaging(t)

print(constrained_unordered_edit_distance(t,[nest_min_t,nest_max_t,av_t]))

# These 3 approximations are selfnested:
hp_nest_min_t = heightprofile(nest_min_t)
hp_nest_max_t = heightprofile(nest_max_t)
hp_av_t = heightprofile(av_t)

print(hp_nest_max_t.is_selfnested())
print(hp_nest_min_t.is_selfnested())
print(hp_av_t.is_selfnested())

print('-------------------------------------')

print('Example 2')

seq_av_t = sequential_averaging(t)

for approx in seq_av_t:
	hp = heightprofile(approx)
	print(hp.is_selfnested())
# Only the last approximation is selfnested

print(constrained_unordered_edit_distance(t,seq_av_t))

print('-------------------------------------')

print('Example 3')

t = gen_random_selfnested_tree(6,3)
nest_min_t = nest_min(t)
print(t.is_isomorphic_to(nest_min_t,tree_type='unordered'))