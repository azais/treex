# -*- python -*-
# -*- coding: utf-8 -*-
#
#		example/display_tree
#
#       File author(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
	This example file shows how to display a tree
"""

# This script should be run in interactive mode

from treex.tree import *
from treex.simulation import gen_random_tree
from treex.visualization.matplotlib_plot import view_tree, view_tree_spline_2d
from treex.visualization.options import assign_random_color_to_tree, assign_random_nodescale_to_tree

# Example 1: display a tree 1/3
print('Example 1')

t = gen_random_tree(100)

figure = view_tree(t,node_scale=40)
figure.set_size_inches(10,3)
figure.tight_layout()
# figure.savefig('tree_ex1.pdf') # if you want to save the file
figure.show()

print('-------------------------------------')

# Example 2: display a tree 2/3
print('Example 2')

t = gen_random_tree(30)
assign_random_color_to_tree(t)
assign_random_nodescale_to_tree(t)

figure = view_tree(t,direction = 'left',node_scale=30)
figure.set_size_inches(10,3)
figure.tight_layout()
figure.show()

print('-------------------------------------')

# Example 3: display a tree 3/3
print('Example 3')

t = gen_random_tree(30)
assign_random_color_to_tree(t)
assign_random_nodescale_to_tree(t)

figure = view_tree_spline_2d(t)
figure.set_size_inches(10,3)
figure.tight_layout()
figure.show()