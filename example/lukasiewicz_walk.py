# -*- python -*-
# -*- coding: utf-8 -*-
#
#		example/lukasiewicz_walk
#
#       File author(s):
#           Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
	This example file shows how to compute the lukasiewicz walk of a tree (and how to go back to the tree)
"""

from treex.tree import *
from treex.simulation import gen_random_tree
from treex.coding_process.lukasiewicz import tree_to_lukasiewicz, lukasiewicz_to_tree

######################################################################
def display_processus (aDictionaryFunction,aTitle='Display function'):
	# This function needs matplotlib and displays a dictionary that
	# represents a function
	from matplotlib import pyplot as plt

	plt.plot(aDictionaryFunction['x'],aDictionaryFunction['y'],'ro-')
	plt.title(aTitle)
	plt.show()
######################################################################

t=gen_random_tree(15)

print('-------------------------------------')

print("initial tree")
t.get_attribute()
t.treeprint('one attribute','height')

print('-------------------------------------')

print("Lukasiewicz")
lukasiewicz=tree_to_lukasiewicz(t)
print ("x  : ",lukasiewicz['x'])
print ("y  : ",lukasiewicz['y'])

print("Kill the display window to continue")
display_processus(lukasiewicz,"Lukasiewicz walk")

print('-------------------------------------')

print("Tree from previous Lukasiewicz walk")
atree=lukasiewicz_to_tree(lukasiewicz)
atree.get_attribute()
atree.treeprint('one attribute','height')
