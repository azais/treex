============
Installation
============

Install from sources
************************

Download source from Gitlab then, at the command line::

    $ python setup.py



Install conda package
************************

In a new environment
------------------------

Install miniconda and create a new environment::

	$ conda create -n treex -c mosaic treex


In an existing environment
--------------------------

Activate the environment and install treex as follows::

	$ conda install -c mosaic treex