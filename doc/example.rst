=========
Examples
=========

How to create and read a tree?
****************************************

.. literalinclude:: ./../example/create_read_tree.py
	:lines: 18-

How to visualize a tree?
****************************************

.. literalinclude:: ./../example/display_tree.py
	:lines: 23-

.. plot:: ./../example/display_tree.py

How to use the subtree kernel module ?
*****************************************

The subtree kernel module provides an implementation of [paper reference].

Follow these examples to understand how the ``subtree_kernel`` module works.

The only thing you need to run all of these are:

* a forest of trees
* each tree has an origin
* some origins might be associated with a class

.. literalinclude:: ./../example/subtree_kernel.py
	:lines: 15-39

We then compute the DAG compression of the whole forest:

.. literalinclude:: ./../example/subtree_kernel.py
	:lines: 41-45

We can now link together the DAG of the dataset with the class-origins information:

.. literalinclude:: ./../example/subtree_kernel.py
	:lines: 47-49

We now introduce the full procedure for computing a kernel, and the SVM classifier that follows.

.. literalinclude:: ./../example/subtree_kernel.py
	:lines: 51-70

Note that in the ``compute_kernel`` method, you have many options you can choose, among them:

* the kernel function :math:`\kappa` to apply to integers, and its parameters
* the weight function :math:`\omega` to apply to subtrees, and its parameters
* if the Gram matrix should be normalized or not
* if you want to apply *discriminance* or not

Here, we will only explore a few options.

.. literalinclude:: ./../example/subtree_kernel.py
	:lines: 72-105

Concerning data visualization, we introduce the following:

.. literalinclude:: ./../example/subtree_kernel.py
	:lines: 107-

.. plot:: ./../example/subtree_kernel.py
