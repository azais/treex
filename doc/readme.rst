========================
Read me
========================

.. {# pkglts, doc

.. #}

treex is a Python library for manipulating rooted trees.
The trees can be ordered or not, with or without labels on their vertices.

:Coordination: Romain Azais

:Contributors: Guillaume Cerutti, Didier Gemmerle, Florian Ingels, Farah Ben Naoum, Salah Habibeche, Benoit Henry

:Team: Inria team MOSAIC

:Language: Python 3

:Supported OS: Linux, Mac OS, Windows

:Licence: LGPL


The package provides a data structure for rooted trees as well as the following main functionalities:

* **Random generation algorithms**
* **DAG compression** for ordered or not, labeled or not, trees
* **Approximation algorithms** for unordered trees
* **Edit distance** for unordered labeled trees
* **Computation of coding processes** (Harris path, Lukasiewicz walk and height process)
* **Visualization algorithms** in Matplotlib or in LaTeX
* **Subtree kernel computation**

************************
Requirements
************************

* Python >=3.6
* NetworkX: https://networkx.github.io/
* NumPy: http://www.numpy.org/
* matplotlib: https://matplotlib.org/
* scikit-learn: https://scikit-learn.org/stable/
* pandas: https://pandas.pydata.org/

************************
Contribute
************************

Fork this project on gitlab_.

.. _gitlab: https://gitlab.inria.fr/mosaic/treex