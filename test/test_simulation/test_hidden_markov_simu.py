# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_simulation/test_hidden_markov
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex import *
from treex.simulation.hidden_markov import *

initial_distribution = [0.3,0.4,0.3]
transition_matrix = [[0.7,0.2,0.1],[0.1,0.1,0.8],[0.1,0.8,0.1]]

parameters = [[0,0.5],[1,1],[3,1.5]]
def gen_emission(k , parameters): # Gaussian emission
 	return random.gauss(parameters[k][0],parameters[k][1])

def test_hmt_simulation():
	t = gen_hidden_markov_tree(50 , initial_distribution , transition_matrix , gen_emission , parameters , type_name = 'A' , obs_name = 'B')
	assert t.get_property('size') == 50
	assert t.get_attribute('A') in [0,1,2]
	assert 'B' in t.get_attribute()