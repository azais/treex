# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_simulation/test_selfnested
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
from treex.simulation.random_selfnested_trees import gen_random_selfnested_tree

def test_random_tree():
	t = gen_random_selfnested_tree(5,3)
	assert t.get_property('height')==5
