# -*- python -*-
# -*- coding: utf-8 -*-
#
#       test/test_enumeration
#
#       File author(s):
#			Florian Ingels <florian.ingels@inria.fr>
#
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
import os
import unittest

from treex.enumeration import *
from treex.simulation.random_recursive_trees import gen_random_tree


class TestEnumeration(unittest.TestCase):
    '''
    Tests the treex.enumeration module
    '''

    def setUp(self):
        self.tree = gen_random_tree(100)
        self.data_dir = 'tmp/test/'
        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)

    def tearDown(self):
        for file in os.listdir(self.data_dir):
            os.remove(self.data_dir+file)
        os.removedirs(self.data_dir)

    def test_enumeration(self):
        left_heavy_embedding(self.tree)
        assert(self.tree.get_property(prop='size',compute_level=1)==100)

        t=tree_random_walk(10,self.tree)
        assert t.get_property(prop='size',compute_level=1)==110

        l = tree_successors(size=6)
        assert(len(l)==36)

        l=tree_successors(height=3,outdegree=2)
        assert(len(l)==65)

        l = tree_successors(self.tree,nb=1)
        assert tree_predecessor(l[-1]).is_isomorphic_to(self.tree)

        t=tree_random_walk(100)
        assert(t.get_property(prop='size',compute_level=1)==102)

        dag = tree_to_dag(t)
        n=len(dag.nodes())
        dag_random_walk(10,dag)
        assert(n<=len(dag.nodes())<=n+10)

        l = dag_successors(dag,nb=1)
        for d in l[1:]:
            assert dag_predecessor(d).__str__()==dag.__str__()

        assert(len(dag_random_walk(nb=100,nodes=10).nodes())==10)
        dag_random_walk(height=2,outdegree=3)

        l = dag_successors(dag,1)
        l= dag_successors(nodes=3,outdegree=2)
        assert(len(l)==9)
        l=dag_successors(height=2,outdegree=2)
        assert(len(l)==144)



class TestFostExtraction(unittest.TestCase):
    '''
    Test
    '''

    def setUp(self):
        self.list = [gen_random_tree(5) for i in range(12)]
        self.data_dir = 'tmp/test/'
        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)

    def tearDown(self):
        for file in os.listdir(self.data_dir):
            os.remove(self.data_dir+file)
        os.removedirs(self.data_dir)


    def test_fost_extraction(self):

        n = 4

        dag_forest = [tree_to_dag(t) for t in self.list]
        index_list = ['a_' + str(i) for i in range(n)] + ['b_' + str(i) for i in range(n)] + ['c_' + str(i) for i in
                                                                                              range(n)]

        for dag in dag_forest:
            dag.assign_origin(index_list[dag_forest.index(dag)])

        dag = Dag()
        dag.concatenate(dag_forest)
        dag.compress()

        p=create_fost(dag,dag.nodes())
        p.__str__()
        d=p.to_dag()
        assert(dag.__str__()==d.__str__())

        a=p.get_attribute('test')
        assert a==None

        l1 = extract_fost(dag)
        l2 = extract_fost(dag, True)

        assert(len(l1)>=len(l2))
        l3 = extract_fost(dag, True, 0)
        assert(len(l2)==len(l3))
        l4 = extract_fost(dag, True, 0.3)
        assert(len(l3)>=len(l4))
        l5 = extract_fost(dag, True, 1)
        assert(len(l5)==0)