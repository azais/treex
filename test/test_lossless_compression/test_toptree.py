# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_lossless_compression/test_toptree
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
from treex.lossless_compression.toptree import *
from treex.simulation.random_recursive_trees import gen_random_tree

import random

def test_toptree_without_attribute():
	t = gen_random_tree(100)
	tt = toptree(t)
	list_leaves = tt.subtrees_sorted_by('height')[0]
	assert list_leaves[0].get_attribute('toptree') == 'XX'

def give_random_label(tree):
	tree.add_attribute_to_id('label',random.randint(0,2))
	for child in tree.my_children:
		give_random_label(child)

def test_toptree_with_attribute():
	t = gen_random_tree(100)
	give_random_label(t)
	tt = toptree(t,attribute_name = 'label')
	list_leaves = tt.subtrees_sorted_by('height')[0]
	assert isinstance( list_leaves[0].get_attribute('toptree') , tuple )

def test_random_toptree_without_attribute():
	t = gen_random_tree(100)
	rtt = random_toptree(t)
	list_leaves = rtt.subtrees_sorted_by('height')[0]
	assert list_leaves[0].get_attribute('toptree') == 'XX'

def test_random_toptree_with_attribute():
	t = gen_random_tree(100)
	give_random_label(t)
	rtt = random_toptree(t,attribute_name = 'label')
	list_leaves = rtt.subtrees_sorted_by('height')[0]
	assert isinstance( list_leaves[0].get_attribute('toptree') , tuple )