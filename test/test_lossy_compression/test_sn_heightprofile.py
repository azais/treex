# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_sn_heightprofile
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.lossy_compression.selfnestedness.heightprofile import *
from treex.simulation.random_selfnested_trees import gen_random_selfnested_tree

def test_heightprofile():
	t = gen_random_tree(20)
	hp = heightprofile(t)
	assert t.get_property('height') == hp.height()

def test_str_hp():
	t = gen_random_tree(20)
	hp = heightprofile(t)
	assert type(hp.__str__())==str

def test_hp_notselfnested():
	t = gen_random_tree(20)
	hp = heightprofile(t)
	SelfNestedHeightProfile.from_heightprofile(hp)

	t2 = gen_random_selfnested_tree(5,[2,2,1,2,2])
	hp2 = heightprofile(t2)

	assert hp2.is_selfnested()