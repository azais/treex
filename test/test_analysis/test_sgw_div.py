# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_analysis/test_sgw_div
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex.analysis.spinal_galton_watson.divergence import sgw_div

def test_sgw_div():
	out1 = sgw_div([0.3,0.3,0.4],[0.2,0.1,0.7])
	out2 = sgw_div([0.5,0.5],[0.5,0.5])
	assert out1>=0
	assert out2<0.00001