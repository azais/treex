# -*- python -*-
# -*- coding: utf-8 -*-
#
#       test/test_converters/test_html
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
from treex.tree import *
from treex.converters.html import *

def test_html1():
    html_str1 = '<a><b>Hello!</b></a>'
    t1 = htmlstring_to_tree(html_str1)
    assert t1.get_property('size')==2

def test_html2():
    html_str2 = '<a><b>Hello!</a></b></c>'
    t2 = htmlstring_to_tree(html_str2)
    assert 'HTML_tag' not in t2.get_attribute().keys()

def test_html_url():
    t = url_to_tree('https://gitlab.inria.fr/mosaic/treex', correction=True)
    d = t.dict_of_ids()
    errs = 0
    for i in d.keys():
        if 'HTML_tag' not in d[i]['attributes']:
            errs += 1
    assert errs == 0

