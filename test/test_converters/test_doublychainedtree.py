# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_converters/test_doublychainedtree
#
#       File author(s):
#           Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex.tree import *
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.converters.doublychainedtree import *

def test_tree_to_doublychainedtree():
	t = gen_random_tree(50)
	s = tree_to_doublychainedtree(t)
	assert s.get_property('size')==t.get_property('size')

def test_doublychainedtree_to_tree():
	t = gen_random_tree(50)
	s = tree_to_doublychainedtree(t)
	t2 = doublychainedtree_to_tree(s)
	assert t.is_isomorphic_to(t2)

def test_from_tree():
	t = gen_random_tree(100)
	s = DoublyChainedTree.from_tree(t)
	assert t.is_isomorphic_to(s)==False or t.get_property('height')==s.get_property('height')

def test_to_tree():
	t = gen_random_tree(50)
	s = DoublyChainedTree.from_tree(t)
	t2 = s.to_tree()
	assert t.is_isomorphic_to(t2)