# -*- python -*-
# -*- coding: utf-8 -*-
#
#       test/test_fost_kernel
#
#       File author(s):
#			Florian Ingels <florian.ingels@inria.fr>
#
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

import os
import unittest

from treex.enumeration.fost_extraction import extract_fost
from treex.kernels.fost_kernel import FostKernelData
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.kernels.support import *
from random import seed

seed(1234)

class TestFostKernel(unittest.TestCase):
    '''
    Tests the treex.kernels.stuff_kernel module
    '''

    def setUp(self):
        self.list = [gen_random_tree(20) for i in range(15)]
        self.data_dir = 'tmp/test/'
        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)

    def tearDown(self):
        for file in os.listdir(self.data_dir):
            os.remove(self.data_dir+file)
        os.removedirs(self.data_dir)

    def test_fost_kernel(self):

        classes={'a': [], 'b':[], 'c':[]}
        dag_list=[]

        for i in range(5):
            t=self.list.pop()
            classes['a'].append('a_'+str(i))
            dag_list.append(tree_to_dag(t,origin='a_'+str(i)))

            t = self.list.pop()
            classes['b'].append('b_' + str(i))
            dag_list.append(tree_to_dag(t, origin='b_' + str(i)))

            t = self.list.pop()
            classes['c'].append('c_' + str(i))
            dag_list.append(tree_to_dag(t, origin='c_' + str(i)))

        dag=Dag()
        dag.concatenate(dag_list)
        dag.compress()

        l = extract_fost(dag,True)

        kd = FostKernelData(l, classes)

        assert 2*len(kd.matching_fost.keys())==len(kd.origins)*(len(kd.origins)+1)
        assert type(kd.matching_fost[('a_0','a_0')][0])==int

        s = kd.sample_origins(1)

        assert s.keys()==classes.keys()

        for k in s:
            assert sorted(s[k])==sorted(classes[k])

        s_discr, s_train, s_pred = kd.sample_origins(3)

        kd.assign_discriminance(s_discr)
        assert 'discriminance' in kd.fost[0].attributes

        gram_train = kd.compute_kernel(s_train,s_train,discriminance=True)
        gram_pred = kd.compute_kernel(s_pred,s_train,discriminance=True)

        gram_train = kd.compute_kernel(s_train, s_train,discriminance=True,kernel_function = sigmoid_kernel,kernel_param=(1,0),weight_function=discriminance_weight)
        gram_train = kd.compute_kernel(s_train, s_train,kernel_function = gaussian_kernel,discriminance=True)
        gram_train = kd.compute_kernel(s_train, s_train,discriminance=True,normalized=True,kernel_param=(1,0),weight_param=(threshold,0.5))
