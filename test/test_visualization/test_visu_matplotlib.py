# -*- python -*-
# -*- coding: utf-8 -*-
#
#       test/test_visualization/test_visu_matplotlib
#
#       File author(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
import unittest

import matplotlib as mpl

from treex.simulation.random_recursive_trees import gen_random_tree
from treex.lossless_compression.dag import *
from treex.visualization.options import *
from treex.visualization.matplotlib_plot import *

import treex
import os


class TestVisuMatplotlib(unittest.TestCase):
    '''
    Tests the treex.visualization.matplotlib_plot module
    '''

    def setUp(self):    
        self.t = gen_random_tree(20)
        self.data_dir = 'tmp/test/'
        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)

    def tearDown(self):
        for file in os.listdir(self.data_dir):
            os.remove(self.data_dir+file)
        os.removedirs(self.data_dir)

    def test_view_tree(self):
        assign_random_color_to_tree(self.t)
        assign_random_nodescale_to_tree(self.t)

        figure_l = view_tree(self.t,direction = 'left',node_scale=30)
        figure_r = view_tree(self.t,direction = 'right',node_scale=30,bg='black')
        figure_t = view_tree(self.t,direction = 'up',node_scale=30)
        figure_d = view_tree(self.t,direction = 'down',node_scale=30)

        figure_l.savefig(self.data_dir+"tree_l.pdf")
        figure_r.savefig(self.data_dir+"tree_r.pdf")
        figure_t.savefig(self.data_dir+"tree_t.pdf")
        figure_d.savefig(self.data_dir+"tree_d.pdf")

        assert os.path.getsize(self.data_dir+"tree_l.pdf")
        assert os.path.getsize(self.data_dir+"tree_r.pdf")
        assert os.path.getsize(self.data_dir+"tree_t.pdf")
        assert os.path.getsize(self.data_dir+"tree_d.pdf")

    def test_view_dag(self):
        # assign_class_color_to_tree(self.t)
        d = tree_to_dag(self.t,tree_type='unordered')
        assign_color_to_dag(d)

        figure = view_dag(d,direction = 'up',node_scale=50)
        figure_l = view_dag(d,direction = 'left',node_scale=50)
        figure_r = view_dag(d,direction = 'right',node_scale=50)
        figure_d = view_dag(d,direction = 'down',node_scale=50)

        figure.savefig(self.data_dir+"tree.pdf")
        figure_l.savefig(self.data_dir+"tree_l.pdf")
        figure_r.savefig(self.data_dir+"tree_r.pdf")
        figure_d.savefig(self.data_dir+"tree_d.pdf")

        assert os.path.getsize(self.data_dir+"tree.pdf")
        assert os.path.getsize(self.data_dir+"tree_l.pdf")
        assert os.path.getsize(self.data_dir+"tree_r.pdf")
        assert os.path.getsize(self.data_dir+"tree_d.pdf")


    def test_view_tree_spline_2d(self):
        # assign_class_color_to_tree(self.t)
        assign_random_color_to_tree(self.t)
        assign_random_nodescale_to_tree(self.t,[2.4,5,2.1])

        figure = view_tree_spline_2d(self.t, figure=None, nodesize_property='nodescale_plotgraph', color_property=None, text_property=None, width_property='nodescale_plotgraph', root_point=[0,0], child_distance=1., leaf_alignment=-1, color='k', node_scale=40, linewidth=1, line_alpha=0.2, textsize=12)
        figure_2 = view_tree_spline_2d(self.t, figure=None, nodesize_property=None, color_property='color_plotgraph', text_property=None, width_property=None, root_point=[0,0], child_distance=1., leaf_alignment=None, color='k', node_scale=40, linewidth=1, line_alpha=0.2, textsize=12)
        figure_3 = view_tree_spline_2d(self.t, figure=None, nodesize_property=None, color_property='color_plotgraph',
                                       text_property='color_plotgraph', width_property=None, root_point=[0, 0], child_distance=1.,
                                       leaf_alignment=None, color='k', node_scale=40, linewidth=1, line_alpha=0.2,
                                       textsize=12)


        figure.savefig(self.data_dir+"tree.pdf")
        figure_2.savefig(self.data_dir+"tree_2.pdf")
        assert os.path.getsize(self.data_dir+"tree.pdf")
        assert os.path.getsize(self.data_dir+"tree_2.pdf")
