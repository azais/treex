# -*- python -*-
# -*- coding: utf-8 -*-
#
#		test/test_visualization/test_visu_options
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.lossless_compression.dag import *
from treex.visualization.options import *

def test_assign_random_color_to_tree():
	t = gen_random_tree(30,5)
	assign_random_color_to_tree(t)
	assert 'color_plotgraph' in t.get_attribute()

def test_assign_random_color_to_tree_2():
	t = gen_random_tree(30,5)
	assign_random_color_to_tree(t,'blue')
	assert 'color_plotgraph' in t.get_attribute()

def test_assign_class_color_to_tree_random():
	t = gen_random_tree(30,5)
	d = tree_to_dag(t,tree_type='unordered')
	assign_class_color_to_tree(t,tree_type=' ',colors=['yellow','blue'])
	assert 'color_plotgraph' in t.get_attribute()

def test_assign_class_color_to_tree_uno():
	t = gen_random_tree(30,5)
	d = tree_to_dag(t,tree_type='unordered')
	assign_class_color_to_tree(t,colors=['yellow','blue'])
	assert 'color_plotgraph' in t.get_attribute()

def test_assign_class_color_to_tree_uno_2():
	t = gen_random_tree(30,5)
	d = tree_to_dag(t,tree_type='unordered')
	assign_class_color_to_tree(t,colors='yellow')
	assert 'color_plotgraph' in t.get_attribute()

def test_assign_class_color_to_tree_o():
	t = gen_random_tree(30,5)
	d = tree_to_dag(t,tree_type='ordered')
	assign_class_color_to_tree(t,tree_type='ordered')
	assert 'color_plotgraph' in t.get_attribute()

def test_assign_class_color_to_tree_o_2():
	t = gen_random_tree(30,5)
	d = tree_to_dag(t,tree_type='ordered')
	assign_class_color_to_tree(t,tree_type='ordered',colors='yellow')
	assert 'color_plotgraph' in t.get_attribute()

def test_assign_random_nodescale_to_tree():
	t = gen_random_tree(30,5)
	assign_random_nodescale_to_tree(t)
	assert 'nodescale_plotgraph' in t.get_attribute()

def test_assign_random_nodescale_to_tree_2():
	t = gen_random_tree(30,5)
	assign_random_nodescale_to_tree(t,3)
	assert 'nodescale_plotgraph' in t.get_attribute()

def test_assign_color_to_dag():
	t = gen_random_tree(30,5)
	d = tree_to_dag(t,tree_type='unordered')
	assign_color_to_dag(d)
	assert 'color_plotgraph' in d.get_attribute()

def test_assign_color_to_dag_2():
	t = gen_random_tree(30,5)
	d = tree_to_dag(t,tree_type='unordered')
	assign_color_to_dag(d,'red')
	assert 'color_plotgraph' in d.get_attribute()

def test_assign_nodescale_to_dag():
	t = gen_random_tree(30,5)
	d = tree_to_dag(t,tree_type='ordered')
	assign_nodescale_to_dag(d)
	assert 'nodescale_plotgraph' in d.get_attribute()

def test_assign_nodescale_to_dag_2():
	t = gen_random_tree(30,5)
	d = tree_to_dag(t,tree_type='ordered')
	assign_nodescale_to_dag(d,5)
	assert 'nodescale_plotgraph' in d.get_attribute()

def test_assign_origin_color():
	forest=[gen_random_tree(20) for i in range(2)]
	d=Dag()
	d.concatenate(forest)
	d.compress()
	d.compress()
	assign_origin_color(d)
	assert d.get_attribute(d.my_leaves[0],'color_plotgraph')=='red'