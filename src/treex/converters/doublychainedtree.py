# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.converters.doublychainedtree
#
#       File author(s):
#           Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>
#
#       File contributor(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
    Called by this module: copy.deepcopy()

    This module allows the manipulation of doubly chained trees
    a structure used in the treex library

    - File author(s): Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>
    - File contributor(s): Romain Azais <romain.azais@inria.fr>
    
    References
    ----------

    See https://xlinux.nist.gov/dads/HTML/binaryTreeRepofTree.html (last consulted on November 22 2018)

"""

from treex.tree import *

# --------------------------------------------------------------

class DoublyChainedTree(Tree):
    """
    The structure of doubly chained trees in the treex library
    This class inherits from the Tree class

    This class implements the usual doubly chained tree structure

    Attributes
    ----------
    my_id: int
        A unique id attributed to the tree

    my_children: list
        my_children is a list of Tree class objects
        The Tree class is a recursive structure, children of a node as given
        as Tree class objects. Default is an empty list

    my_parent: Tree
        Default is None. If the tree is a substructure of another tree,
        my_parent is the Tree class object such that the original tree belongs
        to the larger tree's children
    """

    direction=None # 'l' for left, 'r' for right

    def __init__(self):
        super().__init__()

    def to_tree(self):
        return doublychainedtree_to_tree(self)

    @classmethod
    def from_tree(cls,tree):
        return tree_to_doublychainedtree(tree)

# --------------------------------------------------------------

def tree_to_doublychainedtree(tree,doublychainedtree=None):
    """
    Convert a Tree class object to a DoublyChainedTree class object
    The tree structure is completely preserved

    Parameters
    ----------
    tree: Tree
        A tree

    Returns
    -------
    DoublyChainedTree
        Doubly chained tree corresponding to the tree
        
    """
 
    if doublychainedtree==None: 
        doublychainedtree=DoublyChainedTree()
        doublychainedtree._Tree__my_attributes=deepcopy(tree.get_attribute(None))
        # doublychainedtree._Tree__my_properties=deepcopy(tree.get_property(compute_level=1))
        doublychainedtree.direction=""
    child_numero=0
    for elt in tree.my_children:
        child_numero+=1
        newdoublychainedtree=DoublyChainedTree()
        newdoublychainedtree._Tree__my_attributes=deepcopy(elt.get_attribute(None))
        # newdoublychainedtree._Tree__my_properties=deepcopy(elt.get_property(compute_level=1))
        if child_numero==1:  
            newdoublychainedtree.direction="l"
            doublychainedtree.add_subtree(newdoublychainedtree)
            btree_current=newdoublychainedtree
            tree_to_doublychainedtree(elt,btree_current)
        if child_numero>=2: 
            newdoublychainedtree.direction="r"
            btree_current.add_subtree(newdoublychainedtree)
            btree_current=newdoublychainedtree
            tree_to_doublychainedtree(elt,newdoublychainedtree)
    return doublychainedtree

# --------------------------------------------------------------

def doublychainedtree_to_tree(doublychainedtree,tree=None,parent=None):
    """
    Convert a DoublyChainedTree class object to a Tree class object

    Parameters
    ----------
    doublychainedtree: DoublyChainedTree
        A doubly chained tree

    Returns
    -------
    Tree
        Tree corresponding to the doubly chained tree

    Notes
    -----
    To compute the tree we need to use the additionnal attribute
    binary_coding_parent to point to parent of node different of parent

    """

    if tree==None:
        tree=Tree()
        tree._Tree__my_attributes = deepcopy(doublychainedtree.get_attribute(None))
        # tree._Tree__my_properties = deepcopy(doublychainedtree.get_property(compute_level=1))
        tree.add_attribute_to_id('binary_coding_parent',tree)
        tree.my_children=None
        doublychainedtree_to_tree(doublychainedtree,tree,tree)
    else:
        parent_local=tree
        if len(doublychainedtree.my_children) == 0:
            return(tree)
        else:
            for belt in doublychainedtree.my_children:
                newtree=Tree()
                newtree._Tree__my_attributes = deepcopy(belt.get_attribute(None))
                # newtree._Tree__my_properties = deepcopy(belt.get_property(compute_level=1))
                if belt.direction == 'l':
                    # we can not use parent_local.add_subtree(newtree)
                    # because parent_local is not always the parent of newtree
                    newtree.add_attribute_to_id('binary_coding_parent',parent_local)
                    parent_local.my_children=[newtree]
                    doublychainedtree_to_tree(belt,newtree,parent_local)
                else:
                    if belt.direction == 'r':
                        # Same reason as before 
                        # we can not use parent_local.add_subtree(newtree)
                        # because parent_local is not always the parent of newtree
                        newtree.add_attribute_to_id('binary_coding_parent',parent_local)
                        parent.my_children.append(newtree)
                        doublychainedtree_to_tree(belt,newtree,parent)
     #               else: ### For debugging only ###
     #                   print("Problem: the node is neither right or left")
    return tree