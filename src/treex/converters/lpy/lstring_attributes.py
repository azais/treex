# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.converters.lpy.lstring_attributes
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

from treex.tree import *


def move_lstring_attribute(t , lstring_attr_name):
	if lstring_attr_name in t.get_attribute():
		for var in t.get_attribute(lstring_attr_name):
			t.add_attribute_to_id(var , t.get_attribute(lstring_attr_name)[var])

		t.erase_attribute_at_id(lstring_attr_name , t.my_id)

	for child in t.my_children:
		move_lstring_attribute(child , lstring_attr_name)


def create_lstring_attribute(t , lstring_attr_name , attributes_list):

	dict = {}
	for var in attributes_list:
		if var in t.get_attribute():
			dict[var] = t.get_attribute(var)

	t.add_attribute_to_id(lstring_attr_name , dict)

	for child in t.my_children:
		create_lstring_attribute(child , lstring_attr_name , attributes_list)