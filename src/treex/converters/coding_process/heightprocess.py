# -*- python -*-
# -*- coding: utf-8 -*-
#
#		treex.converters.coding_process.heightprocess
#
#       File author(s):
#           Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

"""
	Called by this module: treex.tree

	This module allows to
	
	- calculate the height process from a Tree class object
	
	- calculate a Tree class object from a height process

	A height process is represented as a dictionary with keys 'x' and 'y'.
	The value associated to 'x' (to 'y', resp) is the list of absissae
	(of ordinates, resp) of the process


	- File author(s): Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>


	References
	----------
	Numerous references in the literature define the concepts of Lukasiewicz walk, 
	height process, and Harris or Dyck walk. We refer to
	
	MARCKERT and MOKKADEM "The depth first processes of Galton-Watson trees converge
	to the same Brownian excursion", Annals of Probability, 2003
	
	and the references therein. See also the Wikipedia article (only in French):
	https://fr.wikipedia.org/wiki/Arbre_de_Galton-Watson (last consulted on November 22 2018)

"""

from treex.tree import *

# --------------------------------------------------------------

def tree_to_height_process(tree):
	"""
	Computes the height process of a tree
	
	Parameters
	----------
	tree: Tree

	Returns
	-------
	dict
		a dictionary with two keys 'x' and 'y':
		dict['x'] is the list of absissae of the process,
		dict['y'] is the list of ordinates of the process

	"""
	function_Height_values=__function_HGT(tree)
	dic_Heigth={}
	dic_Heigth['x']=[]
	dic_Heigth['y']=[]
	aCounter=0
	for elt in function_Height_values:
		dic_Heigth['x'].append(aCounter)
		dic_Heigth['y'].append(elt)
		aCounter+=1
	return(dic_Heigth)

def __function_HGT(tree,list_Height=None,yHeight=0): # Private
	# Auxiliary function of tree_to_height_process
	# usage: List = __function_HGT(Tree)
	# return the list of [0,y_1,y_2,...,y_i] of points of tree_to_height_process 
	# without x-axis calculation
	if list_Height == None:
		list_Height = []
		yHeight=0
	if len(tree.my_children) == 0:
		list_Height.append(yHeight)
	else:
		list_Height.append(yHeight)
		for elt in tree.my_children:
			__function_HGT(elt,list_Height,yHeight+1)
	return(list_Height)

# --------------------------------------------------------------

def height_process_to_tree(a_dictionary_heightprocess):
	"""
	Computes a tree from its height process
	
	Parameters
	----------
	dict: a dictionary with two keys 'x' and 'y':
		dict['x'] is the list of absissae of the walk,
		dict['y'] is the list of ordinates of the walk

	Returns
	-------
	Tree
		The tree associated with the height process

	"""
	if (len(a_dictionary_heightprocess['y']) == 0):
		return None
	else:
		dictionary=deepcopy(a_dictionary_heightprocess)
		tree=Tree()
		tree.my_children=[]
		tree.my_parent=None
		del dictionary['y'][0]
		height_tree_level=0
		__function_HP2T(dictionary,tree,height_tree_level)
		return (tree)

def __function_HP2T(dictionary,tree,height_tree_level): # Private
	# Auxiliary function of height_process_to_tree
	if (len(dictionary['y']) == 0):
		return tree
	else:
		yfirstPoint=dictionary['y'][0]
		del dictionary['y'][0]
		childTree=Tree()
		if (yfirstPoint == height_tree_level+1):
			childTree.my_parent=tree
			tree.my_children.append(childTree)
			__function_HP2T(dictionary,childTree,height_tree_level+1)
		else:
			index=height_tree_level+1
			current_tree=tree
			while (index > yfirstPoint):
				index-=1
				current_tree=current_tree.my_parent
			childTree.my_parent=current_tree
			current_tree.my_children.append(childTree)
			__function_HP2T(dictionary,childTree,yfirstPoint)