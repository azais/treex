# -*- python -*-
# -*- coding: utf-8 -*-
#
#		treex.converters.coding_process.lukasiewicz
#
#       File author(s):
#           Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

"""
	Called by this module: treex.tree

	This module allows to
	
	- calculate the Lukasiewicz walk from a Tree class object
	
	- calculate a Tree class object from a Lukasiewicz walk

	A Lukasiewicz walk is represented as a dictionary with keys 'x' and 'y'.
	The value associated to 'x' (to 'y', resp) is the list of absissae
	(of ordinates, resp) of the walk


	- File author(s): Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>


	References
	----------
	Numerous references in the literature define the concepts of Lukasiewicz walk, 
	height process, and Harris or Dyck walk. We refer to
	
	MARCKERT and MOKKADEM "The depth first processes of Galton-Watson trees converge
	to the same Brownian excursion", Annals of Probability, 2003
	
	and the references therein. See also the Wikipedia article (only in French):
	https://fr.wikipedia.org/wiki/Arbre_de_Galton-Watson (last consulted on November 22 2018)

"""

from treex.tree import *

# --------------------------------------------------------------

def tree_to_lukasiewicz(tree):
	"""
	Computes the Lukasiewicz walk of a tree
	
	Parameters
	----------
	tree: Tree

	Returns
	-------
	dict
		a dictionary with two keys 'x' and 'y':
		dict['x'] is the list of absissae of the walk,
		dict['y'] is the list of ordinates of the walk

	"""
	global running_value_function_luka
	running_value_function_luka=0

	function_Luka_values=__function_LK(tree)
	dic_Luka={}
	dic_Luka['x']=[]
	dic_Luka['y']=[]
	aCounter=0
	for elt in function_Luka_values:
		dic_Luka['x'].append(aCounter)
		dic_Luka['y'].append(elt)
		aCounter+=1
	return(dic_Luka)

running_value_function_luka=0 # For performance, global variable is better than using a [] parameter

def __function_LK(tree,list_Lu=[0]): # Private
	# Auxiliary function of tree_to_lukasiewicz
	# usage: List = __function_LK(Tree)
	# return the list of[0,y_1,y_2,...,y_i] of points of Lukasiewicz 
	# without x-axis calculation
	global running_value_function_luka

	nb_children=len(tree.my_children)
	running_value_function_luka=running_value_function_luka+nb_children-1
	list_Lu.append(running_value_function_luka)

	if  (nb_children != 0):
		for elt in tree.my_children:
			__function_LK(elt,list_Lu)
	return(list_Lu)

# --------------------------------------------------------------

def lukasiewicz_to_tree(dictionnary_lukasiewicz):
	"""
	Computes a tree from its Lukasiewicz walk
	
	Parameters
	----------
	dict: a dictionary with two keys 'x' and 'y':
		dict['x'] is the list of absissae of the walk,
		dict['y'] is the list of ordinates of the walk

	Returns
	-------
	Tree
		The tree associated with the Lukasiewicz walk

	"""
	yList=deepcopy(dictionnary_lukasiewicz['y'])
	root_tree=tree=Tree()
	root_tree.my_parent=None
	root_tree.add_attribute_to_id('yLuka',0)
	del yList[0]
	theTree = __L2T(yList,tree)
	return root_tree

def __L2T(yList,tree): # Private
	# Auxiliary function of lukasiewicz_to_tree
	# usage: Tree =  __L2T(dic)
	# return a Tree from a Lukasiewicz walk (dictionary)
	if (yList[0]==-1):
		return tree
	else:
		y_current=yList[0]
		y_current_tree=tree.get_attribute('yLuka')

		if (y_current >= y_current_tree): # It is a son
			del yList[0]
			child=Tree()
			tree.my_children.append(child)
			child.add_attribute_to_id('yLuka',y_current)
			child.my_parent=tree
			__L2T(yList,child)
		else:   # uncle
			__L2T(yList,tree.my_parent)
			return tree