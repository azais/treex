# -*- python -*-
# -*- coding: utf-8 -*-
#
#		treex.converters.coding_process.harris
#
#       File author(s):
#           Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

"""
	Called by this module: treex.tree

	This module allows to
	
	- calculate the Harris walk (or Dyck walk) from a Tree class object
	
	- calculate a Tree class object from a Harris walk

	A Harris walk is represented as a dictionary with keys 'x' and 'y'.
	The value associated to 'x' (to 'y', resp) is the list of absissae
	(of ordinates, resp) of the walk


	- File author(s): Didier Gemmerle <didier.gemmerle@univ-lorraine.fr>


	References
	----------
	Numerous references in the literature define the concepts of Lukasiewicz walk, 
	height process, and Harris or Dyck walk. We refer to
	
	MARCKERT and MOKKADEM "The depth first processes of Galton-Watson trees converge
	to the same Brownian excursion", Annals of Probability, 2003
	
	and the references therein. See also the Wikipedia article (only in French):
	https://fr.wikipedia.org/wiki/Arbre_de_Galton-Watson (last consulted on November 22 2018)

"""

from treex.tree import *

# --------------------------------------------------------------

def tree_to_harris_walk(tree):
	"""
	Computes the Harris walk of a tree
	
	Parameters
	----------
	tree: Tree

	Returns
	-------
	dict
		a dictionary with two keys 'x' and 'y':
		dict['x'] is the list of absissae of the walk,
		dict['y'] is the list of ordinates of the walk

	"""
	function_HW_values=__function_HW(tree)
	dic_Harris={}
	dic_Harris['x']=[]
	dic_Harris['y']=[]
	aCounter=0
	for elt in function_HW_values:
		dic_Harris['x'].append(aCounter)
		dic_Harris['y'].append(elt)
		aCounter+=1
	return(dic_Harris)

def __function_HW(tree,list_H_w=None,yHarris=0): # Private
	# Auxiliary function of tree_to_harris_walk
	# usage: List = __function_HW(Tree)
	# return the list of [0,y_1,y_2,...,y_i] of points of Harris walk 
	# without x-axis calculation
 	if list_H_w == None:
 		list_H_w = []
 		yHarris=0
 	if len(tree.my_children) == 0:
 		list_H_w.append(yHarris)
 	else:
 		if len(tree.my_children) == 1:
 			elt=tree.my_children[0]
 			list_H_w.append(yHarris)	
 			__function_HW(elt,list_H_w,yHarris+1)
 			list_H_w.append(yHarris)
 		else:
 			list_H_w.append(yHarris)
 			for elt in tree.my_children:	
 				__function_HW(elt,list_H_w,yHarris+1)
 				list_H_w.append(yHarris)
 	return(list_H_w)

# --------------------------------------------------------------

def harris_walk_to_tree(a_dictionary):
	"""
	Computes a tree from its Harris walk
	
	Parameters
	----------
	dict: a dictionary with two keys 'x' and 'y':
		dict['x'] is the list of absissae of the walk,
		dict['y'] is the list of ordinates of the walk

	Returns
	-------
	Tree
		The tree associated with the Harris walk

	"""
	if (len(a_dictionary['y']) == 0):
		return None
	else:
		dictionary=deepcopy(a_dictionary)
		tree=Tree()
		ylastPoint=dictionary['y'][0]
		del dictionary['y'][0]
		__function_T2H(dictionary,tree,ylastPoint)
		return (tree)

def __function_T2H(dictionary,tree,ylastPoint): # Private
	# Auxiliary function of harris_walk_to_tree
	if (len(dictionary['y']) == 0):
		return tree
	else:
		yfirstPoint=dictionary['y'][0]
		del dictionary['y'][0]
		gradient=yfirstPoint-ylastPoint
		ylastPoint=yfirstPoint
		if (gradient > 0):
			childTree=Tree()
			childTree.my_parent=tree
			tree.my_children.append(childTree)
			__function_T2H(dictionary,childTree,ylastPoint)
		else:
			__function_T2H(dictionary,tree.my_parent,ylastPoint)
