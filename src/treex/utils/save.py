# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.utils.save
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

""" 
Called by this module: pickle, os 

This module implements generic functions to save and manipulate saved python
objects

- File author(s): Romain Azais <romain.azais@inria.fr>

"""

import pickle
import os

from treex.tree import *

# --------------------------------------------------------------
# Save functions

def save_object( obj , path ):
    """
    Save python object to the path specified

    Parameters
    ----------
    obj: 
        a python object

    path: str
        the path where to save the object. Path must include the name of the
        file that will be created, and it must ends with .p extension

    Returns
    -------
    None

    See Also
    --------
    read_object(...)

    """
    with open(path , 'wb') as f:
        pickle.dump(obj,f)

def mat_to_txt(mat,path):
    """
    Save a two-dimensional matrix to a .txt file

    Parameters
    ----------
    mat: list
        a two-dimensional matrix, as a list of lists. The latter must have
        same length

    path: str
        the path where to save the object. Path must include the name of the
        file that will be created, and it must ends .txt extension

    Returns
    -------
    None

    See Also
    --------
    save_object(...)

    """
    fichier = open(path,"w")
    for i in range(len(mat)):
        for j in range(len(mat[0])):
            if isinstance(mat[i][j],str):
                fichier.write(mat[i][j])
            else:
                fichier.write(str(mat[i][j]))
            fichier.write('\t')
        fichier.write('\n')
    fichier.close()

def str_to_txt(string,path):
    """
    Save a string to a .txt file

    Parameters
    ----------
    string: str

    path: str
        the path where to save the object. Path must include the name of the
        file that will be created, and it must end .txt extension

    Returns
    -------
    None

    See Also
    --------
    save_object(...)

    """
    fichier = open(path,"w")
    fichier.write(string)
    fichier.close()

def save_tree_properties_and_attributes(t,path,excluded_attributes=[]):
    """
    Save a tree, its properties, and its attributes as a matrix written in a txt file

    Parameters
    ----------
    t: Tree

    path: str
        the path where to save the object. Path must include the name of the
        file that will be created, and it must end .txt extension

    excluded_attributes: list
        a list of attribute names that should not be included in the txt file

    Returns
    -------
    None

    See Also
    --------
    save_object(...)

    """
    d = t.dict_of_ids()
    lst_props = []
    lst_attrs = []
    for i in d.keys():
        for u in d[i]['properties'].keys():
            if u not in lst_props:
                lst_props.append(u)
        for u in d[i]['attributes'].keys():
            if u not in lst_attrs and u not in excluded_attributes:
                lst_attrs.append(u)

    mat = ['parent_id']
    mat.extend(lst_props)
    mat.extend(lst_attrs)
    mat = [ mat ]

    for i in d.keys():
        l = [ d[i]['parent_id'] ]
        for u in lst_props:
            if u in d[i]['properties'].keys():
                l.append(d[i]['properties'][u])
            else:
                l.append(None)

        for u in lst_attrs:
            if u in d[i]['attributes'].keys():
                l.append(d[i]['attributes'][u])
            else:
                l.append(None)

        mat.append(l)

    mat_to_txt(mat,path)
    
# --------------------------------------------------------------
# Read functions

def reassign_ids_to_trees(obj , mapping_list = []):
    """
    Rebuild obj such that trees contained in obj have new ids

    Parameters
    ----------
    obj: 
        a python object

    mapping_list: list
        list of mappings between trees old ids and trees new ids

    Returns
    -------
    A dictionary containing: an object of the same type as obj
    but all the trees contained in it have new ids, and the list
    of mappings between trees old ids and trees new ids

    See Also
    --------
    read_object(...)

    """

    if isinstance(obj,Tree):
        rb = obj.rebuild()
        out = rb['tree']
        mapping = rb['mapping']
        mapping_list.append(mapping)

    elif isinstance(obj,list):
        out = []
        for i in obj:
            rb = reassign_ids_to_trees(i , mapping_list)
            out.append( rb['object'] )
            mapping_list.extend(rb['mapping'])

    elif isinstance(obj,dict):
        out = {}
        for i in obj.keys():
            rb1 = reassign_ids_to_trees(i , mapping_list)
            key = rb1['object']
            mapping_list.extend(rb1['mapping'])

            rb2 = reassign_ids_to_trees( obj[i] , mapping_list)
            val = rb2['object']
            mapping_list.extend(rb2['mapping'])

            out[ key ] = val

    else:
        out = obj

    return {'object':out,'mapping':mapping_list}

def read_object( path , return_mapping = False):
    """
    Read python object saved to the path specified

    Parameters  
    ----------  
    path: str
        the path (including name of the file) where to read the object
        Filename must ends with .p extension
    
    return_mapping: str
        if yes, and if the file contains some trees, returns the mapping
        or the list of mappings between trees old ids and trees new ids

    Warning
    -------
    If the object is a Tree or a list containing a Tree or a dictionary
    containing a Tree (as key or as value) (and recursively), the Tree
    is rebuilt and the ids reassigned.

    See Also
    --------
    save_object(...)
    reassign_ids_to_trees(...)

    """
    obj = pickle.load( open( path , "rb" ) )
    obj_new_ids = reassign_ids_to_trees(obj , mapping_list = [])
    if return_mapping and obj_new_ids['mapping'] != [] :
        if len(obj_new_ids['mapping']) == 1:
            return {'object':obj_new_ids['object'] , 'mapping':obj_new_ids['mapping'][0]}
        else:
            return {'object':obj_new_ids['object'] , 'mapping':obj_new_ids['mapping']}
    else:
        return obj_new_ids['object']


def read_txt( path ):
    """
    Read txt file

    Parameters  
    ----------  
    path: str
        the path (including name of the file) where to read the object
        Filename must ends with .p extension
    
    Returns
    -------
    str

    """
    fichier=open(path,'r')
    string=fichier.read()
    fichier.close()
    return string

def read_disk_size( path ):
    """
    Returns the size, in kB, of the directory at 'path'

    Parameters
    ----------
    path: str
        the path to read

    Returns
    -------
    int

    """
    return os.path.getsize(path)

# --------------------------------------------------------------