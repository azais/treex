# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.app.probability_tree
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       File contributor(s):
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------


def valid_local_probas_sum_to_one(t , local_proba_name , attribute_valid_name , exact_flag = True , epsilon = 0.0001):
	if t.my_parent is None and t.get_attribute(local_proba_name) == 1.0:
		t.add_attribute_to_id(attribute_valid_name, "True")
	else:
		t.add_attribute_to_id(attribute_valid_name, "False")

	sum_proba = 0
	for child in t.my_children:
		valid_local_probas_sum_to_one(child, local_proba_name, attribute_valid_name, exact_flag, epsilon)
		sum_proba += child.get_attribute(local_proba_name)

	if (exact_flag and sum_proba == 1) or (exact_flag == False and abs(sum_proba - 1)<epsilon):
		for child in t.my_children:
			child.add_attribute_to_id(attribute_valid_name, "True")
	else:
		for child in t.my_children:
			child.add_attribute_to_id(attribute_valid_name, "False")


def compute_global_probas(t , local_proba_name):
	global_proba_name = 'global_'+local_proba_name

	if t.my_parent == None:
		t.add_attribute_to_id(global_proba_name , t.get_attribute(local_proba_name))
	else:
		t.add_attribute_to_id(global_proba_name , t.get_attribute(local_proba_name)*t.my_parent.get_attribute(global_proba_name))
	for child in t.my_children:
		compute_global_probas(child , local_proba_name)