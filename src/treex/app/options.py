import numpy as np

from treex.visualization.options import list_of_colors


def _scalar_variable_names(tree, variable_names=None, variable_type='attribute', all_nodes=True):
    if variable_names is None:
        variable_names = getattr(tree,'get_'+variable_type)().keys()
    scalar_names = []
    for name in variable_names:
        if all_nodes:
            variable_values = np.array([getattr(t,'get_'+variable_type)()[name] for t in tree.list_of_subtrees()])
        else:
            variable_values = np.array([getattr(tree,'get_'+variable_type)()[name]])
        try:
            variable_values.astype(float)
        except ValueError:
            pass
        except TypeError:
            pass
        else:
            if variable_values.dtype != bool:
                scalar_names.append(name)
    return scalar_names


def _category_variable_names(tree, variable_names=None, variable_type='attribute', all_nodes=True):
    if variable_names is None:
        variable_names = getattr(tree,'get_'+variable_type)().keys()
    category_names = []
    for name in variable_names:
        if all_nodes:
            variable_values = np.array([getattr(t,'get_'+variable_type)()[name] for t in tree.list_of_subtrees()])
        else:
            variable_values = np.array([getattr(tree,'get_'+variable_type)()[name]])
        if np.all([isinstance(v,str) for v in variable_values]):
            category_names.append(name)
        elif variable_values.dtype in [bool, int, np.uint]:
            category_names.append(name)
    return category_names


def all_node_variable_names(tree, type=None, variable_type='attribute'):
    subtrees = tree.list_of_subtrees()
    all_variable_names = np.unique([v for t in subtrees for v in list(getattr(t,'get_'+variable_type)().keys())])
    all_node_names = [name for name in all_variable_names if np.all([name in getattr(t,'get_'+variable_type)().keys() for t in subtrees])]
    if type == 'scalar':
        return _scalar_variable_names(tree,all_node_names,variable_type=variable_type)
    elif type == 'category':
        return _category_variable_names(tree,all_node_names,variable_type=variable_type)
    else:
        return all_node_names


def _assign_scalar_variable_plot_parameter_to_tree(tree, parameter_name, variable_name, variable_type, value_range, parameter_range, mode):
    variable_value = getattr(tree,'get_'+variable_type)()[variable_name]
    truncated_variable_value = np.minimum(np.maximum(value_range[0],variable_value),value_range[1])

    parameter_value = (truncated_variable_value-value_range[0])/(value_range[1]-value_range[0])
    if mode == 'linear':
        parameter = parameter_range[0] + parameter_value*parameter_range[1]
    else:
        parameter = parameter_range[0]
    tree.add_attribute_to_id(parameter_name,parameter)
    for child in tree.my_children:
        _assign_scalar_variable_plot_parameter_to_tree(child, parameter_name, variable_name, variable_type, value_range, parameter_range, mode)


def _assign_category_variable_plot_parameter_to_tree(tree, parameter_name, variable_name, variable_type, parameter_dict):
    variable_value = getattr(tree,'get_'+variable_type)()[variable_name]
    parameter = parameter_dict[variable_value]
    tree.add_attribute_to_id(parameter_name,parameter)
    for child in tree.my_children:
        _assign_category_variable_plot_parameter_to_tree(child, parameter_name, variable_name, variable_type, parameter_dict)


def assign_scalar_variable_nodescale_to_tree(tree, variable_name, variable_type='attribute', value_range=None, nodescale_range=(1,5), mode='linear'):
    if value_range is None:
        variable_values = [getattr(t,'get_'+variable_type)()[variable_name] for t in tree.list_of_subtrees()]
        if np.min(variable_values)<np.max(variable_values):
            value_range = (np.min(variable_values),np.max(variable_values))
        else:
            value_range = (np.min(variable_values)-1,np.max(variable_values)+1)
    _assign_scalar_variable_plot_parameter_to_tree(tree,'nodescale_plotgraph',variable_name,variable_type,value_range=value_range,parameter_range=nodescale_range,mode=mode)


def assign_scalar_variable_linewidth_to_tree(tree, variable_name, variable_type='attribute', value_range=None, linewidth_range=(1,10), mode='linear'):
    if value_range is None:
        variable_values = [getattr(t,'get_'+variable_type)()[variable_name] for t in tree.list_of_subtrees()]
        if np.min(variable_values)<np.max(variable_values):
            value_range = (np.min(variable_values),np.max(variable_values))
        else:
            value_range = (np.min(variable_values)-1,np.max(variable_values)+1)
    _assign_scalar_variable_plot_parameter_to_tree(tree,'linewidth_plotgraph',variable_name,variable_type,value_range=value_range,parameter_range=linewidth_range,mode=mode)


def assign_category_variable_color_to_tree(tree, variable_name, variable_type='attribute', color_list=list_of_colors):
    variable_values = [getattr(t,'get_'+variable_type)()[variable_name] for t in tree.list_of_subtrees()]
    color_dict = {}
    for i_p, value in enumerate(np.unique(variable_values)):
        color_dict[value] = color_list[(i_p + (len(np.unique(variable_values))==1))%len(color_list)]
    _assign_category_variable_plot_parameter_to_tree(tree, 'color_plotgraph', variable_name, variable_type, parameter_dict=color_dict)

