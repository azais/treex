# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.simulation.random_recursive_trees
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
Called by this module: treex.tree, random

This module implements functions to create random recursive trees

- File author(s): Romain Azais <romain.azais@inria.fr>
"""

from treex.tree import *

import random, math

# --------------------------------------------------------------

def __gen_random_list(size,degree): # Private
# Intermediate function
    liste=[[] for i in range(size)]
    compteur=0
    while compteur<size-1:
        u = random.randint(0,compteur)
        if len(liste[u])<degree:
            compteur+=1
            liste[u].append(compteur)
    return liste

def __gen_random_list_with_height(size,degree,height): # Private
# Intermediate function
    liste = [[] for i in range(size)]
    profs = [0 for i in range(size)]

    compteur=0
    
    while compteur<size-1:
        u = random.randint(0,compteur)
        if len(liste[u])<degree and profs[u]<height:
            compteur+=1
            liste[u].append(compteur)
            profs[compteur]=profs[u]+1
    return liste

def __gen_list2tree(liste,t): # Private
# Intermediate function
    for i in range(len(liste[t._Tree__my_attributes['label_for_simulation']])):
        subtree=Tree()
        subtree.add_attribute_to_id('label_for_simulation',liste[t._Tree__my_attributes['label_for_simulation']][i])
        t.add_subtree(subtree)
        __gen_list2tree(liste,subtree)

def gen_random_tree(size,degree=None,height=None):
    """
    This function creates a random Tree class object with some fixed
    properties

    Parameters
    ----------
    size: int
        The number of nodes of the created tree

    degree: int, optional
        The maximum degree for a node in the tree. Default is set to None. In
        this case, the tree can possibly have any degree, limited of course by
        the size of the tree.

    height: int, optional
        The maximum height of the tree. Default is set to None.

    Returns
    -------
    Tree

    References
    ----------
    This simulation algorithm (without the condition on the height) is presented in
    AZAIS, DURAND, and GODIN "Approximation of trees by self-nested trees"
    ALENEX 2019, San Diego (Ca)
    """
    if degree is None:
        degree=size
    t=Tree()
    t.add_attribute_to_id('label_for_simulation',0)
    if height is None:
        __gen_list2tree(__gen_random_list(size,degree),t)
    else:
        __gen_list2tree(__gen_random_list_with_height(size,degree,height),t)
    # t.erase_attribute('label_for_simulation') # Erase labels: 'label for simulation'
    return t