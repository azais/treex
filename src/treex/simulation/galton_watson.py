# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.simulation.galton_watson
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
Called by this module: treex.tree, random

This module implements functions to generate Galton-Watson trees

- File author(s): Romain Azais <romain.azais@inria.fr>
"""

from treex.tree import *

import random, math

# --------------------------------------------------------------

def __discrete_distribution(name,parameter): # Private
    u=random.random()
    if name=='geometric':
        exp=math.log(u)
        return int(1.0/math.log(1-parameter)*exp)

    elif name=='geometric_with_shift':
        exp=math.log(u) 
        return int(1.0/math.log(1-parameter)*exp)+1

    elif name=='geometric_with_dirac':
        alpha = parameter[0]
        p = parameter[1]

        if u<alpha:
            return 0
        else:
            u=random.random()
            exp = math.log(u)
            return int(1.0/math.log(1-p)*exp)+1

    elif name=='poisson':
        exp = math.exp(-parameter)
        k=0
        p=1
        while p>exp:
            k+=1
            p*=random.random()
        return k-1

    elif name=='binomial':
        n=parameter[0]
        p=parameter[1]
        out=0
        for i in range(n):
            u=random.random()
            if u<p:
                out+=1
        return out

    elif name=='':
        cumfreq=[0]
        for i in range(len(parameter)):
            cumfreq.append(cumfreq[i]+parameter[i])
            if u>cumfreq[i] and u<=cumfreq[i+1]:
                out=i
        return out

# --------------------------------------------------------------

def __growth_gw(generation,name_distribution,parameter,t,max_generation=math.inf): # Private
    if generation<max_generation:
        nb_children=__discrete_distribution(name_distribution,parameter)
        for i in range(nb_children):
            subtree = Tree()
            subtree.add_attribute_to_id('generation',generation+1)
            t.add_subtree_to_id(subtree)
        for child in t.my_children:
            __growth_gw(generation+1,name_distribution,parameter,child,max_generation)

def gen_galton_watson(name_distribution,parameter,max_generation=math.inf):
    """
    This function creates a random Tree class object according to Galton
    Watson processes

    Parameters 
    ---------- 

    name_distribution: str
        - 'geometric': the usual geometric law, with values starting from 0. Parameter is p.
        - 'geometric with shift': same as above, but starts from 1. Parameter is p.
        - 'geometric with dirac': a classic branchment process law. Parameters are alpha and p. With probability alpha returns 0; otherwise, returns the result of a geometric law with shift
        - 'poisson': the usual Poisson law. Parameter is lambda.
        - 'binomial': the usual binomial law. Parameters are n and p.
        - '': a personnalized distribution. Give as parameters the (finite) distribution law, where P(X=k) = parameters[k] 

    parameter: int or list
        Either an integer or an integer list, wether the distribution requires 
        one or more parameters (see above)

    max_generation: int, optional
        Default is set to infinity. If given, the tree construction will stop when
        the maximum generation (corresponding to the height of the tree) is reached


    Notes
    -----
    If the expected value of X is lesser or equal to 1, the tree is almost surely 
    finite. Otherwise, if E[X]>1, the probability that the tree is finite is <1.
    In order to avoid infinite generation tree, use the max_generation parameter.

    Returns
    -------
    Tree
    """
    t = Tree()
    t.add_attribute_to_id('generation',0)
    __growth_gw(0,name_distribution,parameter,t,max_generation)
    return t

# --------------------------------------------------------------
# Estimation of extinction probabiity of a Galton-Watson tree

def __estim_extinct_prob(x,p):
    return sum( [ p[k]*(x**k) for k in range(len(p)) ] )

def __iter_estim_extinct_prob(x,p,err):
    out = x
    test = False
    while test == False:
        new_out = __estim_extinct_prob(out,p)
        if abs(new_out - out)<err:
            test = True
        out = new_out
    return out

def extinction_probability(distribution , err = 10e-9):
    return __iter_estim_extinct_prob(distribution[0] , distribution , err)

# --------------------------------------------------------------

def gen_conditioned_galton_watson(name_distribution , parameter , height , iter_max = 100):
    h = 0
    compteur = 0
    while h<height and compteur < iter_max:
        t = gen_galton_watson(name_distribution,parameter,max_generation=height+1)
        h = t.get_property('height',compute_level=1)
        compteur += 1
    return t

# --------------------------------------------------------------
# Biased distribution in Galton-Watson trees conditioned to survive

def biased_distribution(distribution): # , err = 10e-9 , option = 1):

    # Mean value of the distribution
    m = sum( [k*distribution[k] for k in range(len(distribution))] )
    bp = [ k*distribution[k]/m for k in range(len(distribution)) ]

    # if m<=1: # Subcritical and critical cases
    #     bp = [ k*distribution[k]/m for k in range(len(distribution)) ]
    
    # else: # Supercritical case

    #     ### Option 1 ###
    #     if option == 1:
    #         ep = extinction_probability(distribution , err)
    #         bp = [ distribution[k]*(1-ep**k)/(1-ep) for k in range(len(distribution)) ]

    #     ### Option 2 ###
    #     elif option == 2:
    #         bp = [ k*distribution[k]/m for k in range(len(distribution)) ]

    #     ### Option 3 ###
    #     elif option == 3:
    #         ep = extinction_probability(distribution , err)
    #         bp = [ k*distribution[k]*(1-ep**k)/(1-ep) for k in range(len(distribution)) ]
    #         bp = [ bp[k] / sum(bp) for k in range(len(bp)) ]

    return bp
