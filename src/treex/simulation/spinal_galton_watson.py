# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.simulation.spinal_galton_watson
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
Called by this module: treex.tree, random

This module implements functions to generate spinal Galton-Watson trees

- File author(s): Romain Azais <romain.azais@inria.fr>
"""

from treex.tree import *
from treex.simulation.galton_watson import __discrete_distribution

import random, math

def __growth_spinal_gw(generation,name_distribution1,parameter1,name_distribution2,parameter2,t,max_generation): # Private
    # Requires that prob(X=0) is 0 if X follows the second distribution
    if generation<max_generation:
        if t.get_attribute('vertex_type')=='normal':
            nb_children = __discrete_distribution(name_distribution1,parameter1)
            v = -1
        else: # ie if t.get_attribute('vertex type')=='special'
            nb_children = __discrete_distribution(name_distribution2,parameter2)
            v = random.randint(0,nb_children-1) # a node randomly picked up is special

        for i in range(nb_children):
            subtree = Tree()
            subtree.add_attribute_to_id('generation',generation+1)
            subtree.add_attribute_to_id('vertex_type','normal')
            t.add_subtree_to_id(subtree)

        if v>-1:
            t.my_children[v].add_attribute_to_id('vertex_type','special')

        for child in t.my_children:
            __growth_spinal_gw(generation+1,name_distribution1,parameter1,name_distribution2,parameter2,child,max_generation)

def gen_spinal_galton_watson(name_distribution1,parameter1,name_distribution2,parameter2,max_generation):
    """
    This function creates a random Tree class object generated according to 
    a spinal Galton Watson process
    """
    ### Warning: requires that prob(X=0) is 0 if X follows the second distribution ###
    t = Tree()
    t.add_attribute_to_id('generation',0)
    t.add_attribute_to_id('vertex_type','special')
    __growth_spinal_gw(0,name_distribution1,parameter1,name_distribution2,parameter2,t,max_generation)
    return t

def __growth_sgw_spine(t , sp = []): # Private
    sp.append(t.my_id)
    for child in t.my_children:
        if child.get_attribute('vertex_type') == 'special':
            __growth_sgw_spine(child,sp)

def sgw_spine(t):
    sp = []
    __growth_sgw_spine(t,sp)
    return sp

# --------------------------------------------------------------

def sgw_special_distribution(distribution,transform_distribution):
    out = [0 for i in range(len(distribution))]
    for i in range(len(out)):
        out[i] = transform_distribution[i]*distribution[i]
    s = sum(out)
    if s>0:
        out = [out[i]/s for i in range(len(out))]
    return out