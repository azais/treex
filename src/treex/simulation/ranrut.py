# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.simulation.ranrut
#
#       File author(s):
#           Florian Ingels <florian.ingels@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
Called by this module: treex.tree, random

This module implements ranrut algorithm

- File author(s): Florian Ingels <florian.ingels@inria.fr>
"""

from treex.tree import *

import random, math

def __recursive_number_of_unordered_trees(n,dico):
    sum=0
    for d in range(1,n):
        for j in range(1,math.floor((n-1)/d)+1):
            if n-j*d not in dico:
                __recursive_number_of_unordered_trees(n-j*d,dico)
            if d not in dico:
                __recursive_number_of_unordered_trees(d,dico)
            sum+=d*dico[n-j*d]*dico[d]
    dico[n]=int(sum/(n-1))

def __random_pair(n,dico):
    p = random.random()
    cumsum=0
    for d in range(n-1,0,-1):
        for j in range(1,math.floor((n-1)/d)+1):
            cumsum+=d*dico[n-j*d]*dico[d]/(dico[n]*(n-1))
            if p<cumsum:
                return (j,d)

def __recursive_ranrut_tree(n,dico):
    if n==1:
        return Tree()
    elif n==2:
        t1=Tree()
        t1.add_subtree(Tree())
        return t1
    else:
        j,d = __random_pair(n,dico)

        t1 = __recursive_ranrut_tree(n-j*d,dico)
        t2 = __recursive_ranrut_tree(d,dico)

        t_list = [t2.copy() for i in range(j)]
        for t in t_list:
            t1.add_subtree(t)
    return t1

def gen_ranrut_tree(size):
    """
    Sample a tree from the uniform distribution of unordered trees of the given size, with the RANRUT algorithm [1].

    :param size: int

    :returns:  Tree

    Warning
    -------
    The complexity of the algorithm [2] is in average of the order :math:`O(n^2\\log n)` for the pre-processing part, and :math:`O(n\\log n)` for the actual simulation.
    Therefore, depending on your machine, large values of ``size`` might be intractable.

    References
    ----------
    [1] Nijenhuis, Albert, and H. S. Wilf. "Combinatorial Algorithms Academic Press." New York (1978).

    [2] Alonso, L., R. Schott, and INRIA-Lorraine CRIN. "Random Unlabelled Rooted Trees Revisited." Proc. ICCI. Vol. 94. 1994.


    """
    if size==1:
        return Tree()
    elif size==2:
        t1=Tree()
        t1.add_subtree(Tree())
        return t1
    else:
        dico = {1: 1}
        __recursive_number_of_unordered_trees(size, dico)
        return __recursive_ranrut_tree(size,dico)