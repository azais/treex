# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.simulation.hidden_markov
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
Called by this module: treex.tree, random

This module implements functions to generate hidden Markov chains
along branches of a tree

- File author(s): Romain Azais <romain.azais@inria.fr>
"""

from treex.tree import *
import random
from treex.simulation.random_recursive_trees import gen_random_tree
from treex.simulation.galton_watson import __discrete_distribution

### Generate initial value from a distribution
def __gen_initial_distribution(initial_distribution):
    return __discrete_distribution('',initial_distribution)

### Generate random transition from a Markov matrix
def __gen_transition(k , transition_matrix):
    return __discrete_distribution('', transition_matrix[k] )

### Assign types to the nodes of a tree
### These types are distributed according to a Markov chain
def assign_markov_types_to_tree(t , initial_distribution , transition_matrix , type_name = 'hidden_type'):
    root_type = __gen_initial_distribution(initial_distribution)
    t.add_attribute_to_id(type_name,root_type)

    if 'label_for_simulation' in t.get_attribute():
        t.erase_attribute('label_for_simulation')

    for child in t.my_children:
        child_type = __gen_transition(root_type , transition_matrix)
        assign_markov_types_to_tree(child , transition_matrix[root_type] , transition_matrix , type_name)

### Assign observed variables to the nodes of a tree
def assign_observed_variables_to_tree(t , gen_emission , parameters , type_name = 'hidden_type' , obs_name = 'observation'):
    root_type = t.get_attribute(type_name)
    obs = gen_emission(root_type , parameters)
    t.add_attribute_to_id(obs_name,obs)

    for child in t.my_children:
        assign_observed_variables_to_tree(child,gen_emission,parameters,type_name,obs_name)

def gen_hidden_markov_tree(size , initial_distribution , transition_matrix , gen_emission , parameters , type_name = 'hidden_type' , obs_name = 'observation'):
    t = gen_random_tree(size)
    assign_markov_types_to_tree(t , initial_distribution , transition_matrix , type_name)
    assign_observed_variables_to_tree(t , gen_emission , parameters , type_name , obs_name)
    return t