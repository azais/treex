# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.visualization.options
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       File contributor(s):
#           Florian Ingels <florian.ingels@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

""" 
    Called by this module : treex.tree, treex.dag, random

    This module implements functions to compute some optional graphic parameters, such as coloring the nodes or rescale them

    - File author(s): Romain Azais <romain.azais@inria.fr>
    - File contributor(s): Florian Ingels <florian.ingels@inria.fr>

"""

from treex.tree import *
from treex.lossless_compression.dag import *

import random

global list_of_colors, list_of_scales, list_of_widths

list_of_colors = ['blue','cyan','magenta','yellow','orange','brown','pink',
'olive','white','darkgray','red','violet','gray','green','purple','lightgray']

list_of_scales = [0.5,1.2,1.5]

list_of_widths = [0.5,1.5]

# ---------------------------------------------------------------------------------

def assign_random_color_to_tree(tree, colors=list_of_colors):
    """
    Give to each node in the tree a new attribute, 'color_plotgraph'. If one
    color is provided, every nodes will be colored with it. Eitherway, if a
    list of colors is provided, each node will receive a random color from the
    list.

    Parameters
    ----------
    tree: Tree
        a tree to be colored

    colors: list or str, optional
        the color(s) that will be assigned to the tree. Default is set to
        list_of_colors

    See Also
    --------
    assign_class_color_to_tree(...)
    """
    if isinstance(colors,list)==True:
        tree.add_attribute_to_id('color_plotgraph',colors[random.randint(0,len(colors)-1)])
    else:
        tree.add_attribute_to_id('color_plotgraph',colors)
    for child in tree.my_children:
        assign_random_color_to_tree(child,colors)

def assign_class_color_to_tree(tree,tree_type='unordered',colors=list_of_colors):
    """
    Give to each node in the tree a new attribute, 'color_plotgraph'. If one
    color is provided, every nodes will be colored with it. Eitherway, if a
    list of colors is provided, the nodes will be colored depending on their
    class of equivalence, either ordered or unordered.

    Parameters
    ----------
    tree: Tree
        a tree to be colored

    tree_type: str, optional
        either 'ordered' or 'unordered'. If an other string is given, compute
        assign_random_color_to_tree instead. Default is 'unordered'

    colors: list or str, optional
        the color(s) that will be assigned to the tree. Default is set to
        list_of_colors

    Warnings
    --------
    this function can work only if the equivalence class attribute (either
    ordered or unordered) is already computed. This can be achieved by using
    the tree_to_optimaldag function in treex.tree.

    See Also
    --------
    assign_random_color_to_tree(...), assign_color_to_dag(...)

    Notes
    -----
    ordered and unordered_equivalence_class are debated in the documentation
    of the function is_isomorphic_to(...) in module treex.tree(...)

    """
    if tree_type == 'unordered':
        if isinstance(colors,list)==True:
            tree.add_attribute_to_id('color_plotgraph',colors[ tree.get_attribute('unordered_equivalence_class')%len(colors) ])

        else:
            tree.add_attribute_to_id( 'color_plotgraph' , colors )

        for child in tree.my_children:
            assign_class_color_to_tree(child,tree_type,colors)
          
    elif tree_type == 'ordered':
        if isinstance(colors,list)==True:
            tree.add_attribute_to_id('color_plotgraph',colors[ tree.get_attribute('ordered_equivalence_class')%len(colors) ])
        else:
            tree.add_attribute_to_id( 'color_plotgraph' , colors )

        for child in tree.my_children:
            assign_class_color_to_tree(child,tree_type,colors)

    else:
        assign_random_color_to_tree(tree,colors)

def assign_random_nodescale_to_tree(tree,nodescale=list_of_scales):
    """
    Give to each node in the tree a new attribute, 'nodescale_plotgraph'. If
    one number is provided, every nodes will be rescale with it. Eitherway, if
    a list of scales is provided, each node will receive a random scale from
    the list.

    Parameters
    ----------
    tree: Tree
        a tree to be rescaled

    nodescale: list or int, optional
        the scale(s) that will be assigned to the tree. Default is set to
        list_of_scale
    """
    if isinstance(nodescale,list)==True:
        tree.add_attribute_to_id('nodescale_plotgraph',nodescale[random.randint(0,len(nodescale)-1)])
    else:
        tree.add_attribute_to_id('nodescale_plotgraph',nodescale)
    for child in tree.my_children:
        assign_random_nodescale_to_tree(child,nodescale)

def assign_random_linewidth_to_tree(tree , linewidth=list_of_widths):
    """
    Give to each node in the tree a new attribute, 'linewidth_plotgraph'. If
    one number is provided, every nodes will be rescale with it. Eitherway, if
    a list of scales is provided, each node will receive a random scale from
    the list.

    Parameters
    ----------
    tree: Tree
        a tree to be rescaled

    nodescale: list or int, optional
        the scale(s) that will be assigned to the tree. Default is set to
        list_of_scale
    """
    if isinstance(linewidth,list)==True:
        tree.add_attribute_to_id('linewidth_plotgraph',linewidth[random.randint(0,len(linewidth)-1)])
    else:
        tree.add_attribute_to_id('linewidth_plotgraph',linewidth)
    for child in tree.my_children:
        assign_random_linewidth_to_tree(child,linewidth)

# ---------------------------------------------------------------------------------

def assign_color_to_dag(dag,colors=list_of_colors):
    """
    Give to each node in the dag a new attribute, 'color_plotgraph'. If one
    color is provided, every nodes will be colored with it. Eitherway, if a
    list of colors is provided, each node will receive a color from the list,
    depending on the rest of euclidian division of the node number by the
    length of the list of colors.

    Parameters
    ----------
    dag: Dag
        a dag to be colored

    colors: list or str, optional
        the color(s) that will be assigned to the dag. Default is set to
        list_of_colors

    See Also
    --------
    assign_class_color_to_tree(...)

    Notes
    -----
    if the dag is obtained by using the tree_to_optimaldag function of
    treex.dag, the numbering of the dag will exactly correspond to the classes
    of equivalence of the tree. Therefore, if the tree is also colored, both
    the tree and the dag will have matching colors. It can be helpful to
    visualize how the dag is constructed.

    """
    if isinstance(colors,list)==False:
        l_colors=[colors]
    else:
        l_colors=colors
    for node in dag.nodes():
        dag.add_attribute_to_node('color_plotgraph',l_colors[ node%len(l_colors)],node)

def assign_nodescale_to_dag(dag,nodescale=list_of_scales):
    """
    Give to each node in the dag a new attribute, 'nodescale_plotgraph'. If
    one number is provided, every nodes will be rescaled with it. Eitherway,
    if a list of number is provided, each node will receive a scale from the
    list, depending on the rest of euclidian division of the node number by
    the length of the list of scales.

    Parameters
    ----------
    dag: Dag
        a dag to be rescaled

    nodescale: list or int, optional
        the scale(s) that will be assigned to the dag. Default is set to
        list_of_scales
    """
    if isinstance(nodescale,list)==False:
        l_scales=[nodescale]
    else:
        l_scales=nodescale
    for node in dag.nodes():
        dag.add_attribute_to_node('nodescale_plotgraph',l_scales[ node%len(l_scales)],node)

# ---------------------------------------------------------------------------------

def assign_origin_color(dag):
    """
    Graphic option for plotting.

    Nodes of the same color come from the same dag in the original forest. If a node has several origins, it is colored in red.

    The attribute 'origin' must have been computed on the nodes of the DAG.

    See Also
    --------
    Dag.propagate_origins(...)
    """

    list_of_colors=['blue','cyan','magenta','yellow','orange','brown','pink',
    'olive','white','darkgray','violet','gray','green','purple','lightgray']
        
    origin_list = list(dag.my_origins.keys())

    for node in dag.nodes():
        origin = list(dag.get_attribute(node,'origin').keys())

        if len(origin)>1:
            dag.add_attribute_to_node('color_plotgraph','red',node)
        else:
            dag.add_attribute_to_node('color_plotgraph',list_of_colors[origin_list.index(origin[0])%len(list_of_colors)],node)
# --------------------------------------------------------------------------------------------------------