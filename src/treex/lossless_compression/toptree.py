# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.lossless_compression.toptree
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

"""
This module allows the construction of the toptree of any ordered labeled tree
See Tree compression with top trees by P.Bille, I.L.Gortz, G.M.Landau, and O.Weimann

- File author(s): Romain Azais <romain.azais@inria.fr>

"""

from treex.tree import *
import random

# --------------------------------------------------------------
#
# Find horizontal merges
#
# --------------------------------------------------------------

def __find_horizontal_merges(t,liste = []): # Hidden recursive function
    if len(t.my_children)>1:
        for i in range(int(len(t.my_children)/2)):
            if t.my_children[2*i].my_children == [] or t.my_children[2*i+1].my_children == []:
                liste.append(t.my_children[2*i].my_id)

        if len(t.my_children)%2 == 1: # if the number of children is odd

            if t.my_children[ len(t.my_children)-1 ].my_children == []: # if the last child is a leaf
                if t.my_children[ len(t.my_children)-2 ].my_children != []: # if the previous one is not a leaf
                    if t.my_children[ len(t.my_children)-3 ].my_children != []: # if the previous previous one is not a leaf
                        liste.append(t.my_children[len(t.my_children)-2].my_id)

    for child in t.my_children:
        __find_horizontal_merges(child , liste)

def horizontal_merges(t): # Main function
    # Returns the list of nodes that must be merged with their right brother
    l = []
    __find_horizontal_merges(t,l)
    l2 = []
    for i in range(len(l)):
        l2.append(l[len(l)-i-1])
    return l2

# --------------------------------------------------------------
#
# Find maximal paths and vertical merges
#
# --------------------------------------------------------------

def __find_maximal_paths(t , cont = True , key = None , dico = {} , depth = 0): # Hidden recursive function
    # if cont is True, the current path is continued
    # if key is not none, it is the root of the current path
    # dico is a dictionary that contains the maximal paths, the key being the root of the path
    # depth is the depth of nodes

    for child in t.my_children:
        if len(child.my_children)==1:
            if cont: # path is augmented
                dico[key].append((child.my_id,depth+1))
                __find_maximal_paths(child,True,key,dico,depth+1)

            else: # creation of a new path
                new_key = (t.my_id , child.my_id)
                dico[new_key] = [ (t.my_id,depth) , (child.my_id,depth+1) ]
                __find_maximal_paths( child , True , new_key , dico , depth+1 )

        elif len(child.my_children)>1: # the node has several children

            if cont: # the node is the end of a path
                dico[key].append( (child.my_id , depth+1) )

            __find_maximal_paths(child , False , None , dico , depth+1)

        else: # the node is a leaf
            if cont: # the node is the end of a path
                dico[key].append( (child.my_id ,depth+1) )

def maximal_paths(t): # Only maximal paths with at least 3 nodes
    dico = {}
    __find_maximal_paths(t,False,None,dico,0)

    list_mp = []
    for mp in dico.values():
        if len(mp)>2:
            list_mp.append(mp)
    return list_mp


def vertical_merges(t , liste_horizontal_merges): # Main function
    # Returns the list of nodes that must be merged with their parent
    max_paths = maximal_paths(t)

    dico = {}
    for mp in max_paths:
        if len(mp)%2 == 0: # length of path is even
            for k in range( int((len(mp)-2)/2) ):
                a = mp[len(mp)-2*k-1]

                if a[1] not in dico.keys():
                    dico[a[1]] = [ a[0] ]
                else:
                    dico[a[1]].append(a[0])

        else: # length of path is odd
            for k in range( int((len(mp)-3)/2) ): # empty if mp is of length 3
                a = mp[len(mp)-2*k-1]
                
                if a[1] not in dico.keys():
                    dico[a[1]] = [ a[0] ]
                else:
                    dico[a[1]].append(a[0])

            ####################################################
            # Only exception in vertical merges
            if mp[1][0] not in liste_horizontal_merges: # the parent of mp[2] has not been horizontally merged before
                if mp[2][1] not in dico.keys():
                    dico[mp[2][1]] = [ mp[2][0] ]
                else:
                    dico[mp[2][1]].append(mp[2][0])
            ####################################################

    depths = list(dico.keys())
    depths.sort(reverse=True)
    vms = []
    for d in depths:
        vms.extend(dico[d])

    return vms

# --------------------------------------------------------------
#
# Merge functions
#
# --------------------------------------------------------------

def merge_with_consecutive_brother(t , node_id , dico_edges , toptree_attribute , dict_t_ids ): # horizontal merge
    # node_id must have a consecutive brother
    # node_id or its brother is a leaf

    node_id_index = dict_t_ids[node_id]['properties']['index_among_siblings'] # t.index_among_siblings(node_id) # index of node_id among the siblings
    bro_index = node_id_index + 1 # index of the consecutive brother among the siblings
    parent_id = dict_t_ids[node_id]['parent_id'] # t.subtree(node_id).my_parent.my_id # id of the parent
    bro_id = dict_t_ids[parent_id]['children_id'][bro_index] # t.subtree(parent_id).my_children[bro_index].my_id # id of the brother


    # Type of merge
    if dict_t_ids[node_id]['children_id'] == []: # if t.subtree(node_id).my_children == []:
        if dict_t_ids[bro_id]['children_id'] == []: # if t.subtree(bro_id).my_children == []:
            merge_type = 'e'
        else:
            merge_type = 'd'
    else: # if node_id has children then bro_id should have no child
        merge_type = 'c'

    # Edition and new edge
    if dict_t_ids[node_id]['children_id'] != []: # if t.subtree(node_id).my_children != []: # node_id is not a leaf then bro_id is a leaf
        t.delete_subtree_at_id(bro_id)
        ne = (node_id , parent_id)
        node_to_be_del = bro_id
    else: # node_id is a leaf
        t.delete_subtree_at_id(node_id)
        ne = (bro_id , parent_id)
        node_to_be_del = node_id

    ed1 = (node_id , parent_id)
    ed2 = (bro_id , parent_id)

    # Construction of top tree
    tt = Tree()
    tt.add_attribute_to_id(toptree_attribute , merge_type)

    tt.add_subtree_to_id(dico_edges[ed1])
    tt.add_subtree_to_id(dico_edges[ed2])

    # Update dico_edges
    dico_edges[ne] = tt
    dico_edges.pop( (node_to_be_del,parent_id) )

    ####################################################    
    # Update dict_t_ids
    dict_t_ids.pop(node_to_be_del) # Remove node_to_be_del (node_id or bro_id) from dict_t_ids
    dict_t_ids[parent_id]['children_id'].remove(node_to_be_del) # Remove node_to_be_del (node_id or bro_id) from children of parent_id

    # Update indices among siblings
    ids_c = dict_t_ids[parent_id]['children_id']

    compteur = 0
    for i in ids_c:
        dict_t_ids[i]['properties']['index_among_siblings'] = compteur
        compteur+=1

    ### Warning: index_among_siblings attribute is not updated in the tree ###
    ####################################################

    return [node_id,bro_id] # node_id and bro_id are returned to list nodes that have been horizontally merged


def merge_with_parent(t , node_id , dico_edges , toptree_attribute , dict_t_ids): # vertical merge
    # node_id must be the unique child of its parent

    parent_id = dict_t_ids[node_id]['parent_id'] # t.subtree(node_id).my_parent.my_id # id of parent of node_id in t
    parent_index = dict_t_ids[parent_id]['properties']['index_among_siblings'] # t.index_among_siblings(parent_id) # index of the parent among the siblings
    parent_parent_id = dict_t_ids[parent_id]['parent_id'] # t.subtree(parent_id).my_parent.my_id # id of parent of parent of node_id in t

    # Type of merge
    if dict_t_ids[node_id]['children_id'] == []: # if t.subtree(node_id).my_children == []:
        merge_type = 'b'
    else:
        merge_type = 'a'

    # Edition
    t.subtree(parent_parent_id).add_subtree(t.subtree(node_id) , parent_index)
    des_parent_id = t.subtree(parent_id).list_of_ids() # Descendants of parent_id ( -> to be deleted in dict_t_ids)
    t.delete_subtree_at_id(parent_id) # del the subtree rooted at parent_id

    # New edge
    ne = (t.subtree(parent_parent_id).my_children[parent_index].my_id , parent_parent_id)
    ed1 = (parent_id , parent_parent_id)
    ed2 = (node_id , parent_id)

    # Construction of top tree
    tt = Tree()
    tt.add_attribute_to_id(toptree_attribute , merge_type)

    tt.add_subtree_to_id(dico_edges[ed1])
    tt.add_subtree_to_id(dico_edges[ed2])

    # Update dico_edges
    dico_edges[ne] = tt
    dico_edges.pop(ed1)
    dico_edges.pop(ed2)

    ####################################################
    # Update dict_t_ids
    for n in des_parent_id:
        dict_t_ids.pop(n) # Remove nodes rooted at parent_id from dict_t_ids

    dict_t_ids[parent_parent_id]['children_id'].remove(parent_id) # Remove parent_id from children of parent_parent_id
    dict_t_ids[parent_parent_id]['children_id'].insert(parent_index , ne[0]) # Add the new subtree as a child of parent_parent_id at the right place

    dico_new_subtree = t.subtree(ne[0]).dict_of_ids()
    dict_t_ids.update( dico_new_subtree )
    dict_t_ids[ne[0]]['properties']['index_among_siblings'] = parent_index # index_among_siblings of new node is parent_index

    ### Warning: index_among_siblings property is not updated in the tree, in particular for the new node ne[0] ###
    ####################################################

# --------------------------------------------------------------
#
# Construction of toptree
#
# --------------------------------------------------------------

def toptree(t , toptree_attribute = 'toptree' , attribute_name = None):
    s = t.copy()

    # First traversal of s
    s.get_property('index_among_siblings',compute_level=1)
    dict_s_ids = s.dict_of_ids()

    # Initial edges
    dico_edges = {}
    for i in dict_s_ids:
        if i != s.my_id: # ie i is not the root of s

            a = Tree()

            if attribute_name == None:
                a.add_attribute_to_id(toptree_attribute , 'XX')
            else:
                a.add_attribute_to_id(toptree_attribute , ( dict_s_ids[i]['attributes'][attribute_name] , dict_s_ids[ dict_s_ids[i]['parent_id'] ]['attributes'][attribute_name] ) )

            dico_edges[ (i,dict_s_ids[i]['parent_id']) ] = a

    # Construction of merges and associated top tree
    merges = {}
    while s.get_property('size',compute_level=2)>2:

        # Horizontal merges
        hms = horizontal_merges(s)

        nodes_horizontally_merged = [] # list of nodes horizontally merged
        for hm in hms:
            nodes_horizontally_merged.extend( merge_with_consecutive_brother(s , hm , dico_edges , toptree_attribute , dict_s_ids) )

        # Update indices among siblings and dictionary of ids after horizontal merges # Not useful but can solve problems #
        # s.get_attribute('index_among_siblings',compute=True)
        # dict_s_ids = s.dict_of_ids()

        # Vertical merges
        vms = vertical_merges(s,nodes_horizontally_merged)
        for vm in vms:
            merge_with_parent(s,vm,dico_edges , toptree_attribute , dict_s_ids)

        # Update indices among siblings and dictionary of ids after horizontal merges # Not useful but can solve problems #
        # s.get_attribute('index_among_siblings',compute=True)
        # dict_s_ids = s.dict_of_ids()

    return list(dico_edges.values())[0]

# --------------------------------------------------------------
#
# Construction of a random toptree
#
# --------------------------------------------------------------

def random_toptree(t , toptree_attribute = 'toptree' , attribute_name = None):
    s = t.copy()

    # First traversal of s
    s.get_property('index_among_siblings',compute_level=1)
    dict_s_ids = s.dict_of_ids()

    # Initial edges
    dico_edges = {}
    for i in dict_s_ids:
        if i != s.my_id: # ie i is not the root of s

            a = Tree()

            if attribute_name == None:
                a.add_attribute_to_id(toptree_attribute , 'XX')
            else:
                a.add_attribute_to_id(toptree_attribute , ( dict_s_ids[i]['attributes'][attribute_name] , dict_s_ids[ dict_s_ids[i]['parent_id'] ]['attributes'][attribute_name] ) )

            dico_edges[ (i,dict_s_ids[i]['parent_id']) ] = a

    # Construction of merges and associated top tree

    merges = {}
    while s.get_property('size',compute_level=2)>2:

        u = random.random()

        if u<0.5: # Horizontal merge with probability 0.5

            hms = horizontal_merges(s)

            if hms != []:
                v = int(random.random() * len(hms))
                hm = hms[v]

                nodes_horizontally_merged = [] # list of nodes horizontally merged
                nodes_horizontally_merged.extend( merge_with_consecutive_brother(s , hm , dico_edges , toptree_attribute , dict_s_ids) )

        else: # Vertical merge with probability 0.5

            vms = vertical_merges(s,[]) # or nodes_horizontally_merged

            if vms != []:
                v = int(random.random() * len(vms))
                vm = vms[v]
                merge_with_parent(s,vm,dico_edges , toptree_attribute , dict_s_ids)


    return list(dico_edges.values())[0]