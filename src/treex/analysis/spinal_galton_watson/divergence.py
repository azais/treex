# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.analysis.spinal_galton_watson.divergence
#
#       File author(s):
#           Benoit Henry <benoit.henry@imt-lille-douai.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
Called by this module: numpy, scipy

- File author(s): Benoit Henry <benoit.henry@imt-lille-douai.fr>
"""

import numpy as np

from scipy.optimize import NonlinearConstraint
from scipy.optimize import BFGS
from scipy.optimize import minimize
from scipy.optimize import LinearConstraint

import sys 

def sgw_div(p,q):
	dim = len(p)
	sublinC = []
	for i in range(3):
		V = np.zeros(3*dim+1)
		for j in range(dim):
			V[i*dim+j] = 1
		sublinC.append(V)

	sublinC2 = []
	for i in range(3*dim+1):
		V = np.zeros(3*dim+1)
		V[i] = 1
		sublinC2.append(V)

	linC = LinearConstraint(sublinC, [1, 1, 1], [1, 1, 1])

	mmm = np.ones(3*dim+1)

	linPos = LinearConstraint(sublinC2, np.zeros(3*dim+1), mmm)

	def const4(x): 
		poid = x[dim]/(x[dim]+1)
		v = (1-poid)*x[0:dim]+poid*x[2*dim:3*dim]
		u = (1-poid)*x[dim:2*dim]+poid*x[2*dim:3*dim]
		return chi(v,p)-chi(u,p)

	n4 = NonlinearConstraint(const4,0,np.inf, jac='2-point',hess=BFGS())

	def mini(x):
		return (kull(x[0:dim],p)+kull(x[dim:2*dim],q))+x[dim]*kull(x[2*dim:3*dim],q)

	def comb(x,y):
		tot = 0
		fu = []
		for i in range(len(x)):
			tot = tot+np.sqrt(x[i]*y[i])
		for i in range(len(x)):
			fu.append(np.sqrt(x[i]*y[i])/tot)
		return np.array(fu)

	def kull(x,y):
		k = 0
		for i in range(len(x)):
			if y[i]==0 and x[i]>0:
				k = np.inf
			elif x[i]>0:
				k = k+x[i]*(np.log(x[i])-np.log(y[i]))
		return k

	def chi(x,y):
		k = 0
		for i in range(len(x)):
			k = max(k,abs((x[i]-y[i])))
		return kull(x,y)

	def baba(x,y):
		scal = sum(np.sqrt(np.array(x)*np.array(y)))
		if scal == 0:
			return np.inf
		else:
			return -2*np.log(scal)

	x0 = np.concatenate((comb(p,q), comb(p,q), q, [0.5]))
	res = minimize(mini, x0, method='trust-constr',jac='2-point',tol=1e-16, hess=BFGS(),constraints=[linC,linPos,n4],options={'verbose': 1, 'gtol':1e-20,'maxiter':10000})
	
	return res.fun
