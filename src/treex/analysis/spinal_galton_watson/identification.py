# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.analysis.spinal_galton_watson.identification
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
Called by this module: treex.tree, deepcopy

- File author(s): Romain Azais <romain.azais@inria.fr>
"""

from treex.tree import *
from copy import deepcopy
import math

### Code corrected on Feb 5 2020 by Romain ###
### Possible spines are only of maximal length: updated on Aug 4 2020 by Romain ###

def __sgw_assign_observed_height(t , max_depth):
    # This function assigns an observation status to the nodes of t as follows:
    # If depth of the node is less than max_depth, then the node is observed
    if t.get_property('depth',compute_level=1) < max_depth:
        t.add_attribute_to_id('observation_status','observed')

        if t.get_property('height',compute_level=1) + t.get_property('depth',compute_level=1) < max_depth:
            t.add_attribute_to_id('observed_height' , t.get_property('height',compute_level=1))
        else:
            t.add_attribute_to_id('observed_height' , max_depth - t.get_property('depth',compute_level=1))

    else:
        t.add_attribute_to_id('observation_status','unobserved')
        t.add_attribute_to_id('observed_height',None)

    for child in t.my_children:
        __sgw_assign_observed_height(child , max_depth)

def __sgw_growth_spinal_str(t , max_depth , identified_spine , possible_spines , nb_children , normal_nb_children):

    if t.get_attribute('observation_status') == 'observed':

        nb_children.append( t.get_property('number_of_children',compute_level=1) )

        if t.get_property('depth',compute_level=1) == 0:
            t.add_attribute_to_id('identified_vertex_type','special')

        else:

            if t.get_property('depth',compute_level=1) + t.get_attribute('observed_height') < max_depth:
                t.add_attribute_to_id('identified_vertex_type' , 'normal')

            elif t.my_parent.get_attribute('identified_vertex_type') == 'special':

                compteur = 0
                compteur2 = 0
                for c in t.my_parent.my_children:
                    compteur2+=1
                    if c.get_property('depth',compute_level=1) + c.get_attribute('observed_height') >= max_depth:
                        compteur +=1
                if compteur == 1:
                    t.add_attribute_to_id('identified_vertex_type' , 'special')
                else:
                    t.add_attribute_to_id('identified_vertex_type' , 'unknown')

            else:
                t.add_attribute_to_id('identified_vertex_type' , 'unknown')

        # -------------------------------------
        # Increment identified_spine and possible spines
        if t.get_attribute('identified_vertex_type') == 'special': # Increment identified spine
            identified_spine[0].append(t.my_id)
            identified_spine[1].append(len(t.my_children))
    
        elif t.get_attribute('identified_vertex_type') == 'unknown':
            if t.my_parent.get_attribute('identified_vertex_type') == 'special': # New possible spine
                s = deepcopy(identified_spine)
                s[0].append(t.my_id)
                s[1].append(len(t.my_children))
                possible_spines.append(s)

            else: # Increment an existing possible spine
                for ps in possible_spines:
                    if ps[0][-1] == t.my_parent.my_id:
                        s = deepcopy(ps)
                        s[0].append(t.my_id)
                        s[1].append(len(t.my_children))
                possible_spines.append(s)

        else: # Increment number of children list for normal nodes
            normal_nb_children.append(t.get_property('number_of_children',compute_level=1))
        # -------------------------------------

    else:
        t.add_attribute_to_id('identified_vertex_type','unobserved')

    for child in t.my_children:
        __sgw_growth_spinal_str(child , max_depth , identified_spine , possible_spines , nb_children , normal_nb_children)


def sgw_identify_spinal_structure(t , max_depth = None):
    """
    Identify the spinal structure of a tree
    """

    if max_depth == None:
        h = t.get_property('height',compute_level=1)
    else:
        h = max_depth

    __sgw_assign_observed_height(t , h)

    identified_spine = [[],[]]
    possible_spines = []
    nb_children = []
    normal_nb_children = []
    __sgw_growth_spinal_str(t , h , identified_spine , possible_spines , nb_children , normal_nb_children)

    # Note: all the possible spines should have the same length
    possible_spines_ok = []
    for pos_s in possible_spines:
        if len(pos_s[0]) == h:
                possible_spines_ok.append(pos_s)

    if possible_spines_ok == []:
        possible_spines_ok.append( identified_spine )

    return {'identified_spine':identified_spine , 'possible_spines':possible_spines_ok
        , 'numbers_of_children': {'observed_nodes':nb_children, 'observed_normal_nodes':normal_nb_children}}