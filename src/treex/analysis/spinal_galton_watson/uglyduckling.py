# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.analysis.spinal_galton_watson.uglyduckling
#
#       File author(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
""" 
Called by this module: treex.tree, treex.galton_watson.estimation_spinal_gw, deepcopy

- File author(s): Romain Azais <romain.azais@inria.fr>
"""

from treex.analysis.spinal_galton_watson.identification import sgw_identify_spinal_structure
from treex.analysis.spinal_galton_watson.divergence import sgw_div

from treex.simulation.spinal_galton_watson import sgw_special_distribution
from treex.simulation.galton_watson import biased_distribution

import math
from copy import deepcopy

# ------------------------------------------------------------------------------
# Distances

def sgw_kl(sample , distrib):
    s = 0
    for i in range(len(distrib)):
        if distrib[i]>0 and sample.count(i)>0:
            s += sample.count(i)*math.log(sample.count(i)/distrib[i])
    return s

# def bhattacharyya_dist( distrib1 , distrib2 ):
#     return -2*math.log( sum( [ math.sqrt(distrib1[k]*distrib2[k]) for k in range(min(len(distrib1),len(distrib2))) ] ) )

# ------------------------------------------------------------------------------

def sgw_normal_birth_distrib_estim(sample , max_degree=None):
    if max_degree == None:
        max_birth_distrib = max(sample)
    else:
        max_birth_distrib = max_degree

    if len(sample)>0:
        phat = []
        for i in range( max_birth_distrib+1):
            phat.append(1.0*sample.count(i)/len(sample))
    else:
        phat = [0 for k in range(max_birth_distrib+1)]
    return phat

def sgw_transform_fun_estim(normal_birth_distrib , possible_spine):
    tf = [possible_spine.count(k) / (len(possible_spine) * normal_birth_distrib[k]) if normal_birth_distrib[k]>0 else 0 for k in range(len(normal_birth_distrib))]
    if sum(tf)>0:
        tf = [tf[k] / sum(tf) for k in range(len(tf))]
    return tf

# ------------------------------------------------------------------------------

def sgw_uglyduckling(t , max_depth = None , max_degree = None):

    # 0 # Identify spinal structure
    d = sgw_identify_spinal_structure(t,max_depth)
    poss_spines = d['possible_spines']

    # 1 # Estimation of the normal birth distribution from all the observed nodes
    bd_hat = sgw_normal_birth_distrib_estim( d['numbers_of_children']['observed_nodes'] , max_degree = max_degree)

    # 2 # Compute the spine as the Ugly Duckling
    max_dist = 0
    index = -1
    for psp, i in zip(poss_spines,range(len(poss_spines))):
        di = sgw_kl( psp[1] , biased_distribution(bd_hat) )
        if di > max_dist:
            index = i
            max_dist = di
    sp_hat = poss_spines[index][1]

    # 3 # Correct the estimation of the normal birth distribution
    dat = deepcopy(d['numbers_of_children']['observed_nodes'])
    for i in sp_hat:
        dat.remove(i)
    bd_hat_c = sgw_normal_birth_distrib_estim(dat , max_degree = max_degree)

    # 4 # Estimation of the transform function
    tf_hat = sgw_transform_fun_estim(bd_hat_c , sp_hat)

    # Result
    d.update({'estimated_normal_birth_distribution': bd_hat
                , 'corrected_estimated_normal_birth_distribution': bd_hat_c
                , 'estimated_transform_function': tf_hat
                , 'estimated_special_birth_distribution': sgw_special_distribution(bd_hat_c, tf_hat)
                , 'estimated_spine': poss_spines[index]})

    return d

# ------------------------------------------------------------------------------
# Ugly-Duckling convergence criterion

def sgw_uglyduckling_criterion( distrib1 , distrib2 ): # , err=10e-9 , option = 1):
    m = sum( [ k*distrib1[k] for k in range(len(distrib1)) ] )
    b1 = biased_distribution(distrib1)
    return math.log(m) - sgw_div( b1[1:len(b1)] , distrib2[1:len(distrib2)] )

# ------------------------------------------------------------------------------
