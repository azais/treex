# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.analysis.edit_distance.incremental_zhang_ns
#
#       File author(s):
#           Farah Ben-Naoum <farah.bennaoum@univ-sba.dz>
#
#       File contributor(s):
#           Romain Azais <romain.azais@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

"""
    Called by this module: treex.tree, treex.dag, treex.zhang, networkx, copy

    This module allows to compute incremental Zhang edit distance
    between unordered trees through Network Simplex algorithm

    - File author(s): Farah Ben-Naoum <farah.bennaoum@univ-sba.dz>
    - File contributor(s): Romain Azais <romain.azais@inria.fr>
    
"""

from treex.tree import *
from treex.lossless_compression.dag import *
from treex.analysis.edit_distance.zhang_dag import *

from treex.analysis.edit_distance.zhang_dag import __dag_matching
from treex.analysis.edit_distance.zhang_dag import __size_tree
from treex.analysis.edit_distance.zhang_dag import __dist_VIDE_DAG
from treex.analysis.edit_distance.zhang_dag import __dist_VIDE_Forest
from treex.analysis.edit_distance.zhang_dag import __dist_forest_nodes
from treex.analysis.edit_distance.zhang_dag import __dist_nodes
from treex.analysis.edit_distance.zhang_dag import __network_simplex_incremental

# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

def __matching_nodes_graph(D1, D2, DV1, DV2, Top1, Top2, M, i1, i2, g, g_ind2): # Private

    G=nx.DiGraph()
    ind1={}
    ind2={}
    vide1=0
    vide2=0
    last_i=0
    last_j=0
    
    ch1=[]
    for n in D1.my_structure[i1]['children']:
        if n not in ch1:
            ch1.append(n)

    ch2=[]
    for n in D2.my_structure[i2]['children']:
        if n not in ch2:
            ch2.append(n)
    
    size = 2+len(ch1)+len(ch2)
           
    ni1 = len(D2.my_structure[i2]['children'])
    ni2 = len(D1.my_structure[i1]['children'])

    if ni1!=ni2:
        size=size+1

    ind2[size]=i2
      
    i=2
    for j in ch1:
        G.add_edge(1,i,capacity=D1.my_structure[i1]['children'].count(j), weight=0)
        ind1[i]=j
        last_i=i
        i=i+1
    
    if ni2<ni1:    
        G.add_edge(1,i,capacity=ni1-ni2, weight=0)
        vide1=i
        i=i+1

    for j in ch2:
        G.add_edge(i,size,capacity=D2.my_structure[i2]['children'].count(j), weight=0)
        ind2[i]=j
        last_j=i
        i=i+1
    
    if ni1<ni2:
        G.add_edge(i,size,capacity=ni2-ni1, weight=0)
        vide2=i
    
    i=2
    while i<=last_i:
        if vide1==0:
            j=last_i+1
        else:
            j=vide1+1
        while j<=last_j:
            a=G[1][i].get('capacity')
            b=G[j][size].get('capacity')
            if a<b:
                c=a
            else:
                c=b            
            G.add_edge(i,j,capacity=c, weight=M[Top1.index(ind1[i])][Top2.index(ind2[j])])
            j=j+1
        i=i+1

    if vide1!=0:
        i=vide1
        j=i+1
        while j<=last_j:
            a=G[j][size].get('capacity')
            b=G[1][i].get('capacity')
            if a<b:
                c=a
            else:
                c=b                                    
            G.add_edge(i,j,capacity=c, weight=DV2[Top2.index(ind2[j])])
            j=j+1

    if vide2!=0:
        i=2
        j=vide2
        while i<=last_i:
            a=G[1][i].get('capacity')
            b=G[j][size].get('capacity')
            if a<b:
                c=a
            else:
                c=b                        
            G.add_edge(i,j,capacity=c, weight=DV1[Top1.index(ind1[i])])
            i=i+1
    
    for (i,j) in G.edges:
        G.add_edge(i, j, flow=0)
        if i==1 and j in list(ind1.keys()):
            G[i][j]['flow']=G[i][j]['capacity']
        elif i==1 and j==vide1:
            G[i][j]['flow']=G[i][j]['capacity']
        elif i==vide1 and j in list(ind2.keys()):
            lab_j=ind2[j]
            if lab_j in list(g_ind2.values()):
                for k in g_ind2.keys():
                    if g_ind2[k]==lab_j:
                        gj=k
                        break
                r=0
                trouv=False
                while not trouv and r<len(g)-4:
                    if g[r][0]==vide1 and g[r][1]==gj:
                        trouv=True
                    else:
                        r=r+1
                if trouv:
                    G[i][j]['flow']=g[r][2]
        elif i in list(ind1.keys()) and j in list(ind2.keys()):
            lab_j=ind2[j]
            if lab_j in list(g_ind2.values()):
                for k in g_ind2.keys():
                    if g_ind2[k]==lab_j:
                        gj=k
                        break
                r=0
                while g[r][0]!=i or g[r][1]!=gj:
                    r=r+1
                G[i][j]['flow']=g[r][2]
        elif i in list(ind2.keys()) and j in list(ind2.keys()):
            G[i][j]['flow']=G[i][j]['capacity']
        elif i==vide2 and j in list(ind2.keys()):
            G[i][j]['flow']=G[i][j]['capacity']
        elif i in list(ind1.keys()) and j==vide2:
            r=0
            trouv=False
            while not trouv and r<len(g)-4:
                if g[r][0]==i and g[r][1] not in list(g_ind2.keys()):
                    trouv=True
                else:
                    r=r+1
            if trouv:
                G[i][j]['flow']=g[r][2]
    return G, ind2, vide1, vide2

def __class_G_edges(G, vide1, vide2, ind1, ind2, ml):

    S_dec=[[],[]]
    S_inc=[[],[]]
    S_crt=[]
    for (i,j) in G.edges():
        if (i in list(ind1.keys()) and (j in list(ind2.keys()) or j==ml)) or (i in list(ind1.keys()) and j==vide2) or (i==vide1 and (j in list(ind2.keys()) or j==ml)):
            if G[i][j]['flow']<G[i][j]['capacity']:
                S_inc[0].append(i)
                S_inc[1].append(j)
            if G[i][j]['flow']>G[i][j]['capacity']:
                S_crt.append((i,j))
            if G[i][j]['flow']>0:
                S_dec[0].append(i)
                S_dec[1].append(j)
    R=[]
    for i in range(len(S_dec[0])):
        for j in range(len(S_inc[0])):
            if (S_dec[0][i]==S_inc[0][j]) and (S_dec[1][i]!=S_inc[1][j]) and (S_dec[0][i] not in R):
                R.append(S_dec[0][i])
    if vide2!=0:
        R.append(vide2)
    elif vide1!=0:
        R.append(vide1)    

    L=[]
    for i in range(len(S_dec[1])):
        for j in range(len(S_inc[1])):
            if (S_dec[1][i]==S_inc[1][j]) and (S_dec[0][i]!=S_inc[0][j]) and (S_dec[1][i] not in L):
                L.append(S_dec[1][i])
    if vide2!=0:
        L.append(vide2)
    elif vide1!=0:
        L.append(vide1)
    L.append(ml)
    
    E_dec=[]
    for i in range(len(S_dec[0])):
        if (S_dec[0][i] in R) and (S_dec[1][i] in L):
            E_dec.append((S_dec[0][i],S_dec[1][i]))
    E_inc=[]
    for i in range(len(S_inc[0])):
        if (S_inc[0][i] in R) and (S_inc[1][i] in L):
            E_inc.append((S_inc[0][i],S_inc[1][i]))

    A=[]
    for (i,j) in E_inc:
        A.append((G[i][j]['weight'],(i,j)))
    A.sort()
    E_inc=[]
    for i in range(len(A)):
        E_inc.append(A[i][1])

    A=[]
    for (i,j) in E_dec:
        A.append((G[i][j]['weight'],(i,j)))
    A.sort()
    A.reverse()
    E_dec=[]
    for i in range(len(A)):
        E_dec.append(A[i][1])

    A=[]
    for (i,j) in S_crt:
        A.append((G[i][j]['weight'],(i,j)))
    A.sort()
    A.reverse()
    S_crt=[]
    for i in range(len(A)):
        S_crt.append(A[i][1])

    for (i,j) in S_crt:
        if (i,j) in E_dec:
            E_dec.remove((i,j))

    E_dec=S_crt+E_dec
    
    return E_inc, E_dec, S_crt

def __update_1(G, E_inc, E_dec, S_crt, a, b, q):
    
    if q>0:
        if (b,a) in E_inc:
            G[b][a]['flow']= G[b][a]['flow']+1
            return G
        else:         
            S1=E_inc[:]
            S2=E_dec[:]
            u=1
        
    elif q<0:
        if ((b,a) in S_crt) or ((b,a) in E_dec and S_crt==[]):
            G[b][a]['flow']= G[b][a]['flow']-1
            return G
        else:            
            S1=E_dec[:]
            S2=E_inc[:]
            u=-1
        
    for (i,j) in S1:
        if j==a:
            break
    
    G[i][j]['flow']= G[i][j]['flow']+u
    S1.remove((i,j))
    S2.append((i,j))
    if u<0 and (i,j) in S_crt:
        S_crt.remove((i,j))
    c=i
    if i==b:
        return G
    else:
        for (i,j) in S2:
            if i==c and j!=a:
                break
        G[i][j]['flow']= G[i][j]['flow']-u
        S2.remove((i,j))
        S1.append((i,j))
        if u>0 and (i,j) in S_crt:
            S_crt.remove((i,j))

        if q>0:
            return __update_1(G, S1, S2, S_crt, j, b, q)
        else:
            return __update_1(G, S2, S1, S_crt, j, b, q)

def __update_2(G, E_inc, E_dec, a, b):

    for (i,j) in E_dec:
        if j==b:
            break

    G[i][j]['flow']= G[i][j]['flow']-1
    E_dec.remove((i,j))
    E_inc.append((i,j))

    c=i
    trouv=False
    k=0
    while k<len(E_inc) and not trouv:
        (i,j)=E_inc[k]
        if (i,j)==(c,a):
            trouv=True
        else:
            k=k+1
    if not trouv:
        for (i,j) in E_inc:
            if i==c and j!=b:
                break
    G[i][j]['flow']= G[i][j]['flow']+1
    E_inc.remove((i,j))
    E_dec.append((i,j))
    
    if j!=a:
        return __update_2(G, E_inc, E_dec, a, j)
    else:
        return G

def __matching_nodes_graph_add_leaf(D1, D2, DV1, DV2, Top1, Top2, M, i1, i2, i3): # Private
    G=nx.DiGraph()
    ind1={}
    ind2={}
    vide1=0
    vide2=0
    last_i=0
    last_j=0

    ch1=[]
    for n in D1.my_structure[i1]['children']:
        if n not in ch1:
            ch1.append(n)

    ch2=[]
    for n in D2.my_structure[i2]['children']:
        if n not in ch2:
            ch2.append(n)
    
    size = 2+len(ch1)+len(ch2)
           
    ni1 = len(D2.my_structure[i2]['children'])
    ni2 = len(D1.my_structure[i1]['children'])

    if ni1!=ni2:
        size=size+1

    ind2[size]=i2
      
    i=2
    for j in ch1:
        G.add_edge(1,i,capacity=D1.my_structure[i1]['children'].count(j),flow=D1.my_structure[i1]['children'].count(j), weight=0)
        ind1[i]=j
        last_i=i
        i=i+1
    
    if ni2<ni1:    
        G.add_edge(1,i,capacity=ni1-ni2, flow=ni1-ni2, weight=0)
        vide1=i
        i=i+1

    for j in ch2:
        G.add_edge(i,size,capacity=D2.my_structure[i2]['children'].count(j), flow=D2.my_structure[i2]['children'].count(j), weight=0)
        ind2[i]=j
        last_j=i
        i=i+1
    
    if ni1<ni2:
        G.add_edge(i,size,capacity=ni2-ni1, flow=ni2-ni1, weight=0)
        vide2=i

    i=2
    while i<=last_i:
        if vide1==0:
            j=last_i+1
        else:
            j=vide1+1
        while j<=last_j:
            a=G[1][i].get('capacity')
            b=G[j][size].get('capacity')
            if a<b:
                c=a
            else:
                c=b            
            G.add_edge(i,j,capacity=c, flow=0,weight=M[Top1.index(ind1[i])][Top2.index(ind2[j])])
            j=j+1
        i=i+1

    if vide1!=0:
        i=vide1
        j=i+1
        while j<=last_j:
            a=G[j][size].get('capacity')
            b=G[1][i].get('capacity')
            if a<b:
                c=a
            else:
                c=b                        
            G.add_edge(i,j,capacity=c, flow=c, weight=DV2[Top2.index(ind2[j])])
            j=j+1
    
    if vide2!=0:
        i=2
        j=vide2
        while i<=last_i:
            a=G[1][i].get('capacity')
            G.add_edge(i,j,capacity=a, flow=a, weight=DV1[Top1.index(ind1[i])])
            i=i+1

    for (i,j) in ind2.items():
        if j==i3:
            ml=i
            break

    if vide2!=0:
        i=2
        i_min=2
        cost_min=G[i][ml]['weight']-G[i][vide2]['weight']
        while i<last_i:
            i=i+1
            nc=G[i][ml]['weight']-G[i][vide2]['weight']
            if nc<cost_min:
                i_min=i
                cost_min=nc
        G[i_min][ml]['flow']=1
        G[i_min][vide2]['flow']=G[i_min][vide2]['flow']-1
    elif vide1!=0:
        G[vide1][ml]['flow']=1
    else:
        G[2][ml]['flow']=1

    cost=0
    for (i,j) in list(G.edges()):
        cost=cost+G[i][j]['flow']*G[i][j]['weight']
              
    return G, ind2, vide1, vide2, cost

def __init_spanning_tree(EG): # Private
    S=[]
    V=[]
    T={}
    TG=nx.Graph()
    for (i,j) in EG.edges():
        if EG[i][j]['flow']==EG[i][j]['capacity']:
            S.append((i,j))
        elif EG[i][j]['flow']==0:
            V.append((i,j))
        else:
            T[(i,j)]=EG[i][j]['flow']
            TG.add_edge(i,j)
            Cy=[]
            try:
                Cy=nx.find_cycle(TG)
            except:
                pass
            if Cy!=[]:
                tmp=T.pop((i,j))
                TG.remove_edge(i,j)
                S.append((i,j))
                V.append((i,j))

    l=list(EG.nodes())
    l.sort()
    L=l[:]
    l.remove(l[0])
    l.remove(l[len(l)-1])
    gr=EG.subgraph(l)
    gr=gr.to_undirected()
    num=gr.number_of_nodes()
    P=[l[0]]
    prev=[]
    Tnodes={}
    cnT=[]
    for i in gr.nodes():
        Tnodes[i]=[]
    for (i,j) in list(T.keys()):
        Tnodes[i].append(j)
        Tnodes[j].append(i)
        if cnT==[]:
            cnT.append([i,j])
        else:
            trouv=False
            k=0
            while k<len(cnT) and not trouv:
                if i in cnT[k] and j not in cnT[k]:
                    cnT[k].append(j)
                    trouv=True
                elif i not in cnT[k] and j in cnT[k]:
                    cnT[k].append(i)
                    trouv=True
                elif i in cnT[k] and j in cnT[k]:
                    trouv=True
                k=k+1
            if not trouv:
                cnT.append([i,j])
    fin=False
    while not fin:
        excnT=cnT[:]
        i=0
        while i<len(cnT)-1:
            j=i+1
            while j<len(cnT):
                trouv=False
                k=0
                while k<len(cnT[i]) and not trouv:
                    if cnT[i][k] in cnT[j]:
                        trouv=True
                    else:
                        k=k+1
                if trouv:
                    cnT[i].extend(cnT[j])
                    cnT.remove(cnT[j])
                else:
                    j=j+1
            i=i+1
        if cnT==excnT:
            fin=True

    i=P[0]
    while len(P)<num:
        nei=list(gr.neighbors(i))
        k=0
        avr=[]
        while k<len(nei):
            trouv=False
            j=0
            while j<len(cnT) and not trouv:
                if i in cnT[j]:
                    avr=cnT[j][:]
                if i in cnT[j] and nei[k] in cnT[j] and nei[k] not in Tnodes[i]:
                    trouv=True
                else:
                    j=j+1
            if trouv or nei[k] in P:
                nei.remove(nei[k])
            else:
                k=k+1
        nxt=[]
        k=0
        while k<len(nei):
            if nei[k] in Tnodes[i]:
                nxt.append(nei[k])
            k=k+1
        if nxt!=[]:
            P.extend(nxt)
            if avr!=[]:
                for k in range(len(avr)):
                    if avr[k] not in P:
                        P.append(avr[k])
            prev.append(i)
            i=nxt[0]
        elif nei!=[]:
            P.append(nei[0])
            Tnodes[i].append(nei[0])
            Tnodes[nei[0]].append(i)
            if cnT==[]:
                cnT.append([i,nei[0]])
            else:
                j=nei[0]
                trouvi=False
                ki=0
                while ki<len(cnT) and not trouvi:
                    if i in cnT[ki]:
                        trouvi=True
                    else:
                        ki=ki+1
                kj=0
                trouvj=False
                while kj<len(cnT) and not trouvj:
                    if j in cnT[kj]:
                        trouvj=True
                    else:
                        kj=kj+1
                if trouvi and not trouvj:
                    cnT[ki].append(j)
                elif not trouvi and trouvj:
                    cnT[kj].append(i)
                elif trouvi and trouvj:
                    cnT[ki].extend(cnT[kj])
                    cnT.remove(cnT[kj])
                else:
                    cnT.append([i,j])
            
            if (i,nei[0]) in list(EG.edges()):
                T[(i,nei[0])]=EG[i][nei[0]]['flow']
                if (i,nei[0]) in S:
                    S.remove((i,nei[0]))
                if (i,nei[0]) in V:
                    V.remove((i,nei[0]))
            if (nei[0],i) in list(EG.edges()):
                T[(nei[0],i)]=EG[nei[0]][i]['flow']
                if (nei[0],i) in S:
                    S.remove((nei[0],i))
                if (nei[0],i) in V:
                    V.remove((nei[0],i))
            prev.append(i)
            i=nei[0]
        else:
            if prev!=[]:
                i=prev[len(prev)-1]
                prev.remove(prev[len(prev)-1])
    T[(1,2)]=EG[1][2]['flow']
    if (1,2) in V:
        V.remove((1,2))
    if (1,2) in S:
        S.remove((1,2))
    T[(L[len(L)-2],L[len(L)-1])]=EG[L[len(L)-2]][L[len(L)-1]]['flow']
    if (L[len(L)-2],L[len(L)-1]) in V:
        V.remove((L[len(L)-2],L[len(L)-1]))
    if (L[len(L)-2],L[len(L)-1]) in S:
        S.remove((L[len(L)-2],L[len(L)-1]))

    return T, S, V

def __mod_type_cap(D1, D2, DV1, DV2, Top1, Top2, M, i1, i2, i3, q, g, g_ind2, g_vide1, g_vide2):

    G,ind1,ind2,vide1,vide2,ml = __matching_nodes_graph_cap(D1, D2, DV1, DV2, Top1, Top2, M, i1, i2, i3, q, g, g_ind2, g_vide1, g_vide2)
    E_inc,E_dec,S_crt = __class_G_edges(G, vide1, vide2, ind1, ind2, ml)
    if (vide2!=0):
        if (q>0):
            G = __update_2(G, E_inc, E_dec, ml, vide2)
        elif (q<0):
            G = __update_2(G, E_inc, E_dec, vide2, ml)
    elif (vide1!=0):
        G = __update_1(G, E_inc, E_dec, S_crt, ml, vide1, q)

    EDG=list(G.edges())
    for (i,j) in EDG:
        if G[i][j]['capacity']==0:
            G.remove_edge(i,j)
    for i in list(G.nodes()):
        if G.in_degree[i]==0 and G.out_degree[i]==0:
            G.remove_node(i)
            if i==vide1:
                vide1=0
            if i==vide2:
                vide2=0
    
    return G, ind2, vide1, vide2

def __matching_nodes_graph_cap(D1, D2, DV1, DV2, Top1, Top2, M, i1, i2, i3, q, g, g_ind2, g_vide1, g_vide2): # Private

    G=nx.DiGraph()
    ind1={}
    ind2={}
    vide1=0
    vide2=0
    last_i=0
    last_j=0
    
    ch1=[]
    for n in D1.my_structure[i1]['children']:
        if n not in ch1:
            ch1.append(n)

    ch2=[]
    for n in D2.my_structure[i2]['children']:
        if n not in ch2:
            ch2.append(n)
    
    size = 2+len(ch1)+len(ch2)
           
    ni1 = len(D2.my_structure[i2]['children'])
    ni2 = len(D1.my_structure[i1]['children'])

    if ni1!=ni2:
        size=size+1

    ind2[size]=i2
      
    i=2
    for j in ch1:
        G.add_edge(1,i,capacity=D1.my_structure[i1]['children'].count(j), weight=0)
        ind1[i]=j
        last_i=i
        i=i+1
    
    if ni2<ni1:    
        G.add_edge(1,i,capacity=ni1-ni2, weight=0)
        vide1=i
        i=i+1

    for j in ch2:
        G.add_edge(i,size,capacity=D2.my_structure[i2]['children'].count(j), weight=0)
        ind2[i]=j
        last_j=i
        i=i+1
    
    if ni1<ni2:
        G.add_edge(i,size,capacity=ni2-ni1, weight=0)
        vide2=i
    
    i=2
    while i<=last_i:
        if vide1==0:
            j=last_i+1
        else:
            j=vide1+1
        while j<=last_j:
            a=G[1][i].get('capacity')
            b=G[j][size].get('capacity')
            if a<b:
                c=a
            else:
                c=b            
            G.add_edge(i,j,capacity=c, weight=M[Top1.index(ind1[i])][Top2.index(ind2[j])])
            j=j+1
        i=i+1
    
    if vide1!=0:
        i=vide1
        j=i+1
        while j<=last_j:
            a=G[j][size].get('capacity')
            b=G[1][i].get('capacity')
            if a<b:
                c=a
            else:
                c=b            
            G.add_edge(i,j,capacity=c, weight=DV2[Top2.index(ind2[j])])
            j=j+1

    if vide2!=0:
        i=2
        j=vide2
        while i<=last_i:
            a=G[1][i].get('capacity')
            b=G[j][size].get('capacity')
            if a<b:
                c=a
            else:
                c=b                        
            G.add_edge(i,j,capacity=c, weight=DV1[Top1.index(ind1[i])])
            i=i+1

    if vide1==0 and vide2==0:
        if q>0:
            vide2=size+1
            i=2
            j=vide2
            while i<=last_i:
                G.add_edge(i,j,capacity=0, weight=DV1[Top1.index(ind1[i])])
                i=i+1
        elif q<0:
            vide1=size+1
            i=vide1
            j=last_i+1
            while j<=last_j:
                G.add_edge(i,j,capacity=0, weight=DV2[Top2.index(ind2[j])])
                j=j+1

    if i3 not in list(ind2.values()):
        j=size+1
        ml=j
        i=2
        while i<=last_i:
            G.add_edge(i,j,capacity=0, weight=M[Top1.index(ind1[i])][Top2.index(i3)])
            i=i+1
        if vide1!=0:
            G.add_edge(vide1,j,capacity=0, weight=DV2[Top2.index(i3)])
    else:
        for (i,j) in ind2.items():
            if j==i3:
                ml=i
                break
    
    for (i,j) in G.edges():
        G.add_edge(i, j, flow=0)
        if i==1 and j in list(ind1.keys()):
            G[i][j]['flow']=G[i][j]['capacity']
        elif i==1 and j==vide1:
            G[i][j]['flow']=G[i][j]['capacity']
        elif i==vide1 and (j in list(ind2.keys()) or j==ml):
            if j==ml and ml not in list(ind2.keys()):
                lab_j=i3
            else:
                lab_j=ind2[j]
            if lab_j in list(g_ind2.values()):
                for k in g_ind2.keys():
                    if g_ind2[k]==lab_j:
                        gj=k
                        break
                r=0
                trouv=False
                while not trouv and r<len(g)-4:
                    if g[r][0]==g_vide1 and g[r][1]==gj:
                        trouv=True
                    else:
                        r=r+1
                if trouv:
                    G[i][j]['flow']=g[r][2]
        elif i in list(ind1.keys()) and (j in list(ind2.keys()) or j==ml):
            if j==ml and ml not in list(ind2.keys()):
                lab_j=i3
            else:
                lab_j=ind2[j]
            if lab_j in list(g_ind2.values()):
                for k in g_ind2.keys():
                    if g_ind2[k]==lab_j:
                        gj=k
                        break
                r=0
                while g[r][0]!=i or g[r][1]!=gj:
                    r=r+1
                G[i][j]['flow']=g[r][2]
        elif i in list(ind2.keys()) and j in list(ind2.keys()):
            G[i][j]['flow']=G[i][j]['capacity']
        elif i==vide2 and j in list(ind2.keys()):
            G[i][j]['flow']=G[i][j]['capacity']
        elif i in list(ind1.keys()) and j==vide2:
            r=0
            trouv=False
            while not trouv and r<len(g)-4:
                if g[r][0]==i and g[r][1]==g_vide2:
                    trouv=True
                else:
                    r=r+1
            if trouv:
                G[i][j]['flow']=g[r][2]                
      
    return G, ind1, ind2, vide1, vide2, ml

###############################################################################################

def __incremental_dag_matching_edge_inc(D1, D2, i1, i2, M, F, Flow, DV1, DFV1, Ord1, Ord2, cost_sup, cost_adj): # Private

    
    D2.my_structure[i1]['children'].append(i2)
    D2.assign_leaves()
    D2.get_property(compute_level=2)

    top1=Ord1[:]
    top1.reverse()

    top3=Ord2[:]
    top3.reverse()
      
    top2=Ord2[:]
    top2.reverse()
            
    T2 = __size_tree(D2, Ord2)
    DV2 = __dist_VIDE_DAG(T2, cost_adj)
    DFV2 = __dist_VIDE_Forest(T2, cost_adj)
        
    par=[]
    j=Ord2.index(i1)
    i=Ord2.index(i1)+1
    while i<len(Ord2):
        if i==len(Ord2)-1:
            par.append(Ord2[i])
            i+=1
        elif Ord2[j] in D2.my_structure[Ord2[i]]['children']:
            par.append(Ord2[i])
            j=i
        else:
            i+=1

    s_L=0    
    for n1 in Ord1:
        for n2 in Ord2[Ord2.index(i1):]:
            pivot=0    
            if n2==i1 or n2 in par:
                    g=Flow[top1.index(n1)][top2.index(n2)]
                    if n2==i1:                           
                        k=len(g)-1
                        ind2=g[k]
                        k=k-2
                        vide2=g[k]
                        k=k-1
                        vide1=g[k]
                        EG,EG_ind2,Evide1,Evide2 = __mod_type_cap(D1, D2, DV1, DV2, top1, top2, M, n1, n2, i2, 1, g, ind2, vide1, vide2)
                        T,S,V=__init_spanning_tree(EG)
                    elif n2 in par:
                        k=len(g)-1
                        ind2=g[k]
                        k=k-1
                        T,S,V=g[k]
                        EG,EG_ind2,Evide1,Evide2 = __matching_nodes_graph(D1, D2, DV1, DV2, top1, top2, M, n1, n2, g, ind2)
                    mincost,G,pivot,T,S,V = __network_simplex_incremental(EG,T,S,V)
                    s_L+=pivot
                    g=[]
                    for (i,j) in G.edges():
                        if i!=0 and j!=0:
                            g.append((i,j, G[i][j]['flow']))
                    g.append(Evide1)
                    g.append(Evide2)
                    g.append((T,S,V))
                    g.append(EG_ind2)
                    Flow[top1.index(n1)][top2.index(n2)]=g
                    F = __dist_forest_nodes(D1, D2, DFV1, DFV2, top1, top2, F, mincost, n1, n2)
                    M = __dist_nodes(D1, D2, DV1, DV2, top1, top2, M, F, n1,n2, cost_sup, cost_adj)

    # D2.get_property(compute_level=2)
    return M, F, Flow, DV1, DFV1, Ord1, Ord2, D2, s_L

def __incremental_dag_matching_edge_dec(D1, D2, i1, i2, M, F, Flow, DV1, DFV1, Ord1, Ord2, cost_sup, cost_adj): # Private
        
    D2.my_structure[i1]['children'].remove(i2)
       
    top1=Ord1[:]
    top1.reverse()

    top3=Ord2[:]
    top3.reverse()

    for i in list(D2.nodes()):
            if (i!=i1) and (len(D2.my_structure[i]['children'])==0):
                    l=top3.index(i)
                                                    
    T2=__size_tree(D2, Ord2)
    DV2=__dist_VIDE_DAG(T2, cost_adj)
    DFV2=__dist_VIDE_Forest(T2, cost_adj)
        
    top2=Ord2[:]
    top2.reverse()
        
    par=[]
    j=Ord2.index(i1)
    i=Ord2.index(i1)+1
    while i<len(Ord2):
        if i==len(Ord2)-1:
            par.append(Ord2[i])
            i+=1
        elif Ord2[j] in D2.my_structure[Ord2[i]]['children']:
            par.append(Ord2[i])
            j=i
        else:
            i+=1
    
    s_L=0
    for n1 in Ord1:
        for n2 in Ord2[Ord2.index(i1):]:
            pivot=0    
            if (n2==i1)  or (n2 in par):
                    if n2==i1:
                        g=Flow[top1.index(n1)][top2.index(n2)]
                        k=len(g)-1
                        ind2=g[k]
                        k=k-2
                        vide2=g[k]
                        k=k-1
                        vide1=g[k]
                        EG,EG_ind2,Evide1,Evide2 = __mod_type_cap(D1, D2, DV1, DV2, top1, top2, M, n1, n2, i2, -1, g, ind2, vide1, vide2)
                        T,S,V=__init_spanning_tree(EG)
                    elif n2 in par:
                        g=Flow[top1.index(n1)][top2.index(n2)]
                        k=len(g)-1
                        ind2=g[k]
                        k=k-1
                        T,S,V=g[k]
                        EG,EG_ind2,Evide1,Evide2 = __matching_nodes_graph(D1, D2, DV1, DV2, top1, top2, M, n1, n2, g, ind2)
                    mincost,G,pivot,T,S,V = __network_simplex_incremental(EG,T,S,V)
                    s_L+=pivot
                    g=[]
                    for (i,j) in G.edges():
                        if i!=0 and j!=0:
                            g.append((i,j, G[i][j]['flow']))
                    g.append(Evide1)
                    g.append(Evide2)
                    g.append((T,S,V))
                    g.append(EG_ind2)
                    Flow[top1.index(n1)][top2.index(n2)]=g
                    F=__dist_forest_nodes(D1, D2, DFV1, DFV2, top1, top2, F, mincost, n1, n2)
                    M=__dist_nodes(D1, D2, DV1, DV2, top1, top2, M, F, n1,n2, cost_sup, cost_adj)


    D2.my_structure.pop(i2)
    Ord2.remove(i2)
    j=top2.index(i2)
    top2.remove(i2)
    for n1 in top1:
        k=top1.index(n1)
        M[k]=M[k][:j]+M[k][j+1:]
        F[k]=F[k][:j]+F[k][j+1:]
        Flow[k]=Flow[k][:j]+Flow[k][j+1:]

    D2.assign_leaves()
    D2.get_property(compute_level=2)
    
    return M, F, Flow, DV1, DFV1, Ord1, Ord2, D2,s_L

###############################################################################################

def __incremental_tree_matching_add_any_leaf(tree1, tree2, i1, M, F, Flow, DV1, DFV1, Ord1, Ord2, cost_sup, cost_adj):
    i2=max(Ord2)+1

    #################################################
    tree2.my_structure[i2] = {'children': []}
    tree2.my_structure[i2]['attributes'] = {'random_walk_info':'added_node'}
    tree2.my_structure[i1]['children'].append(i2)
    info = {'node':i2,'parent':i1,'operation_name':'add_leaf'}
    #################################################

    Ord2=[i2]+Ord2
    T2=__size_tree(tree2, Ord2)
    DV2=__dist_VIDE_DAG(T2, cost_adj)
    DFV2=__dist_VIDE_Forest(T2, cost_adj)
        
    top1=Ord1[:]
    top1.reverse()
    top2=Ord2[:]
    top2.reverse()

    leaf=top2.index(i2)-1
    for n1 in top1:
        i=top1.index(n1)
        M[i]=M[i]+[M[i][leaf]]
        F[i]=F[i]+[F[i][leaf]]
        c=Flow[i][leaf]
        if c!=[]:
            ind2=c[len(c)-1]
            ind2[max(ind2.keys())]=i2
            c[len(c)-1]=ind2
        Flow[i]=Flow[i]+[c]
        """M[i]=M[i][:leaf2]+[M[i][leaf]]+M[i][leaf2:]
        F[i]=F[i][:leaf2]+[F[i][leaf]]+F[i][leaf2:]
        Flow[i]=Flow[i][:leaf2]+[Flow[i][leaf]]+Flow[i][leaf2:]"""
        

    if len(tree2.my_structure[i1]['children'])!=1:
        tree2.my_structure[i1]['children'].remove(i2)
        # tree2.get_property(compute_level=2)
        M,F,Flow,DV1,DFV1,Ord1,Ord2,tree2,s_L=__incremental_dag_matching_edge_inc(tree1, tree2, i1, i2, M, F, Flow, DV1, DFV1, Ord1, Ord2, 1, 1)
    else:
        tree2.assign_leaves()
        tree2.get_property(compute_level=2)

        par=[]
        j=Ord2.index(i1)
        i=Ord2.index(i1)+1
        while i<len(Ord2):
            if i==len(Ord2)-1:
                par.append(Ord2[i])
                i+=1
            elif Ord2[j] in tree2.my_structure[Ord2[i]]['children']:
                par.append(Ord2[i])
                j=i
            else:
                i+=1
                                       
        L={}
        pivot=0
        for n1 in Ord1:
            for n2 in Ord2[Ord2.index(i1):]:
                if n2==i1 or n2 in par:
                    if n2==i1:
                        G,EG_ind2,Evide1,Evide2,mincost=__matching_nodes_graph_add_leaf(tree1, tree2, DV1, DV2, top1, top2, M, n1, n2, i2)
                        T,S,V=__init_spanning_tree(G)
                    elif n2 in par:
                        g=Flow[top1.index(n1)][top2.index(n2)]
                        k=len(g)-1
                        ind2=g[k]
                        k=k-1
                        T,S,V=g[k]
                        EG,EG_ind2,Evide1,Evide2=__matching_nodes_graph(tree1, tree2, DV1, DV2, top1, top2, M, n1, n2, g, ind2)                              
                        mincost,G,pivot,T,S,V=__network_simplex_incremental(EG,T,S,V)
                        L[n1,n2]=pivot
                    g=[]
                    for (i,j) in G.edges():
                        if i!=0 and j!=0:
                            g.append((i,j, G[i][j]['flow']))
                    g.append(Evide1)
                    g.append(Evide2)
                    g.append((T,S,V))
                    g.append(EG_ind2)
                    Flow[top1.index(n1)][top2.index(n2)]=g                         
                    F=__dist_forest_nodes(tree1, tree2, DFV1, DFV2, top1, top2, F, mincost, n1, n2)
                    M=__dist_nodes(tree1, tree2, DV1, DV2, top1, top2, M, F, n1,n2, cost_sup, cost_adj)
        s_L = sum(list(L.values()))

    return M, F, Flow, DV1, DFV1, DV2, Ord1, Ord2, tree2, s_L, info

def __incremental_tree_matching_sub_any_leaf(tree1, tree2, i2, M, F, Flow, DV1, DFV1, Ord1, Ord2, cost_sup, cost_adj): # Private
    j=Ord2.index(i2)
    i=Ord2.index(i2)+1
    trouv=False
    while i<len(Ord2)and not trouv:
        if Ord2[j] in tree2.my_structure[Ord2[i]]['children']:
            i1=Ord2[i]
            trouv=True
        else:
            i+=1
    info = {'node':i2,'parent':i1,'operation_name':'del_leaf'}
    
    if len(tree2.my_structure[i1]['children'])!=1:
        T2=__size_tree(tree2, Ord2)
        DV2=__dist_VIDE_DAG(T2, cost_adj)
        DFV2=__dist_VIDE_Forest(T2, cost_adj)
    
        top1=Ord1[:]
        top1.reverse()
        top2=Ord2[:]
        top2.reverse()
        
        M,F,Flow,DV1,DFV1,Ord1,Ord2,tree2,s_L=__incremental_dag_matching_edge_dec(tree1, tree2, i1, i2, M, F, Flow, DV1, DFV1, Ord1, Ord2, 1, 1)

    else:

        top1=Ord1[:]
        top1.reverse()
        top2=Ord2[:]
        top2.reverse()

        # remplacer la colonne i1 par celle de la feuille supprimée i2
        l1=top2.index(i2)
        l3=top2.index(i1)
        for n1 in top1:
            l2=top1.index(n1)
            a=M[l2].pop(l1)
            M[l2][l3]=a
            b=F[l2].pop(l1)
            F[l2][l3]=b
            c=Flow[l2].pop(l1)
            if c!=[]:
                ind2=c[len(c)-1]
                ind2[max(ind2.keys())]=i1
                c[len(c)-1]=ind2
            Flow[l2][l3]=c

        Ord2.remove(i2)
        top2.remove(i2)

        #################################################
        tree2.my_structure[i1]=tree2.my_structure[i2]
        tree2.my_structure.pop(i2)
        tree2.assign_leaves()
        tree2.get_property(compute_level=2)
        #################################################

        T2=__size_tree(tree2, Ord2)
        DV2=__dist_VIDE_DAG(T2, cost_adj)
        DFV2=__dist_VIDE_Forest(T2, cost_adj)

        par=[]
        j=Ord2.index(i1)
        i=Ord2.index(i1)+1
        while i<len(Ord2):
            if i==len(Ord2)-1:
                par.append(Ord2[i])
                i+=1
            elif Ord2[j] in tree2.my_structure[Ord2[i]]['children']:
                par.append(Ord2[i])
                j=i
            else:
                i+=1

        #boucle sur les sommets de tree1
        L={}
        pivot=0
        for n1 in Ord1:
            #boucle sur les sommets de tree2
            for n2 in Ord2[Ord2.index(i1):]:    
                if n2 in par: # mod type cost
                    # calculer le graphe EG a partir de tree1 et tree2 et g
                    g=Flow[top1.index(n1)][top2.index(n2)]
                    k=len(g)-1
                    ind2=g[k]
                    k=k-1
                    T,S,V=g[k]
                    EG,EG_ind2,Evide1,Evide2=__matching_nodes_graph(tree1, tree2, DV1, DV2, top1, top2, M, n1, n2, g, ind2)
                    mincost,G,pivot,T,S,V=__network_simplex_incremental(EG,T,S,V)
                    L[n1,n2]=pivot
                    # compute g from G and update Flow
                    g=[]
                    for (i,j) in G.edges():
                        if i!=0 and j!=0:
                            g.append((i,j, G[i][j]['flow']))
                    g.append(Evide1)
                    g.append(Evide2)
                    g.append((T,S,V))
                    g.append(EG_ind2)
                    Flow[top1.index(n1)][top2.index(n2)]=g
                    F=__dist_forest_nodes(tree1, tree2, DFV1, DFV2, top1, top2, F, mincost, n1, n2) 
                    M=__dist_nodes(tree1, tree2, DV1, DV2, top1, top2, M, F, n1,n2, cost_sup, cost_adj) 
        s_L = sum(list(L.values()))
    
    return M, F, Flow, DV1, DFV1, DV2, Ord1, Ord2, tree2, s_L, info


def __incremental_tree_matching_sub_inter_node(tree1, tree2, i2, M, F, Flow, DV1, DFV1, Ord1, Ord2, cost_sup, cost_adj): # Private    
    j=Ord2.index(i2)
    i=Ord2.index(i2)+1
    trouv=False
    while i<len(Ord2)and not trouv:
        if Ord2[j] in tree2.my_structure[Ord2[i]]['children']:
            i1=Ord2[i]
            trouv=True
        else:
            i+=1
                          
    top1=Ord1[:]
    top1.reverse()
    top2=Ord2[:]
    top2.reverse()
    l1=top2.index(i2)
    l3=top2.index(i1)
    for n1 in top1:
        l2=top1.index(n1)
        a=M[l2].pop(l1)
        M[l2][l3]=a
        b=F[l2].pop(l1)
        F[l2][l3]=b
        c=Flow[l2].pop(l1)
        ind2=c[len(c)-1]
        ind2[max(ind2.keys())]=i1
        c[len(c)-1]=ind2
        Flow[l2][l3]=c

    Ord2.remove(i2)
    top2.remove(i2)

    #################################################
    tree2.my_structure[i1]=tree2.my_structure[i2]
    tree2.my_structure.pop(i2)
    tree2.get_property(compute_level=2)
    info = {'node':i2,'parent':i1,'operation_name':'del_internal_node'}
    #################################################

    T2=__size_tree(tree2, Ord2)
    DV2=__dist_VIDE_DAG(T2, cost_adj)
    DFV2=__dist_VIDE_Forest(T2, cost_adj)
                    
    par=[]
    j=Ord2.index(i1)
    i=Ord2.index(i1)+1
    while i<len(Ord2):
        if i==len(Ord2)-1:
            par.append(Ord2[i])
            i+=1
        elif Ord2[j] in tree2.my_structure[Ord2[i]]['children']:
            par.append(Ord2[i])
            j=i
        else:
            i+=1

    L={}
    pivot=0
    for n1 in Ord1:
        for n2 in Ord2[Ord2.index(i1):]:    
            if n2 in par:
                g=Flow[top1.index(n1)][top2.index(n2)]
                k=len(g)-1
                ind2=g[k]
                k=k-1
                T,S,V=g[k]
                EG,EG_ind2,Evide1,Evide2=__matching_nodes_graph(tree1, tree2, DV1, DV2, top1, top2, M, n1, n2, g, ind2)
                mincost,G,pivot,T,S,V=__network_simplex_incremental(EG,T,S,V)
                L[n1,n2]=pivot
                g=[]
                for (i,j) in G.edges():
                    if i!=0 and j!=0:
                        g.append((i,j, G[i][j]['flow']))
                g.append(Evide1)
                g.append(Evide2)
                g.append((T,S,V))
                g.append(EG_ind2)
                Flow[top1.index(n1)][top2.index(n2)]=g
                F=__dist_forest_nodes(tree1, tree2, DFV1, DFV2, top1, top2, F, mincost, n1, n2) 
                M=__dist_nodes(tree1, tree2, DV1, DV2, top1, top2, M, F, n1,n2, cost_sup, cost_adj) 
                
    s_L = sum(list(L.values()))

    return M, F, Flow, DV1, DFV1, DV2, Ord1, Ord2, tree2, s_L, info

def __incremental_tree_matching_add_inter_node(tree1, tree2, i2, M, F, Flow, DV1, DFV1, Ord1, Ord2, cost_sup, cost_adj): # Private
    pred=[]
    j=Ord2.index(i2)
    i=Ord2.index(i2)+1
    trouv=False
    while i<len(Ord2)and not trouv:
        if Ord2[j] in tree2.my_structure[Ord2[i]]['children']:
            pred.append(Ord2[i])
            trouv=True
        else:
            i+=1
    if pred!=[]:
        i1=pred[0]
                          
    top1=Ord1[:]
    top1.reverse()
    top2=Ord2[:]
    top2.reverse()
        
    l1=top2.index(i2)    
    for n1 in top1:
        l2=top1.index(n1)
        M[l2].insert(l1,0)
        F[l2].insert(l1,0)
        Flow[l2].insert(l1,[])
        
    i3=max(Ord2)+1
    top2.insert(l1,i3)
    Ord2=top2[:]
    Ord2.reverse()

    #################################################
    tree2.my_structure[i3] = {'children': [i2] ,'properties':{} , 'attributes':{}}
    tree2.my_structure[i3]['attributes'] = tree2.my_structure[i2]['attributes']
    tree2.my_structure[i2]['attributes'] = {'random_walk_info':'added_node'}
    info = {'node':i2,'parent':i3,'operation_name':'add_internal_node'}
    #################################################

    if pred!=[]:
        tree2.my_structure[i1]['children'].append(i3)
        tree2.my_structure[i1]['children'].remove(i2)
    else:
        tree2.assign_roots()
    tree2.get_property(compute_level=2)

    T2=__size_tree(tree2, Ord2)
    DV2=__dist_VIDE_DAG(T2, cost_adj)
    DFV2=__dist_VIDE_Forest(T2, cost_adj)

    s_L=0
    for n1 in Ord1:
        G,EG_ind2,Evide1,Evide2,mincost=__matching_nodes_graph_add_leaf(tree1, tree2, DV1, DV2, top1, top2, M, n1, i3, i2)
        T,S,V=__init_spanning_tree(G)
        g=[]
        for (i,j) in G.edges():
            if i!=0 and j!=0:
                g.append((i,j, G[i][j]['flow']))
        g.append(Evide1)
        g.append(Evide2)
        g.append((T,S,V))
        g.append(EG_ind2)
        Flow[top1.index(n1)][top2.index(i3)]=g                
        F = __dist_forest_nodes(tree1, tree2, DFV1, DFV2, top1, top2, F, mincost, n1, i3)
        M = __dist_nodes(tree1, tree2, DV1, DV2, top1, top2, M, F, n1,i3, cost_sup, cost_adj)
    
    if pred!=[]:
        par=[]
        j=Ord2.index(i1)
        i=Ord2.index(i1)+1
        while i<len(Ord2):
            if i==len(Ord2)-1:
                par.append(Ord2[i])
                i+=1
            elif Ord2[j] in tree2.my_structure[Ord2[i]]['children']:
                par.append(Ord2[i])
                j=i
            else:
                i+=1

        L={}
        pivot=0
        for n1 in Ord1:
            for n2 in Ord2[Ord2.index(i1):]:
                if n2==i1 or (n2 in par):
                    g=Flow[top1.index(n1)][top2.index(n2)]
                    k=len(g)-1
                    ind2=g[k]
                    k=k-1
                    T,S,V=g[k]
                    if n2==i1:
                        for (a,b) in list(ind2.items()):
                            if b==i2:
                                ind2[a]=i3
                    EG,EG_ind2,Evide1,Evide2=__matching_nodes_graph(tree1, tree2, DV1, DV2, top1, top2, M, n1, n2, g, ind2)
                    if n2==i1:
                        T,S,V=__init_spanning_tree(EG)
                    mincost,G,pivot,T,S,V=__network_simplex_incremental(EG,T,S,V)
                    L[n1,n2]=pivot
                    g=[]
                    for (i,j) in G.edges():
                        if i!=0 and j!=0:
                            g.append((i,j, G[i][j]['flow']))
                    g.append(Evide1)
                    g.append(Evide2)
                    g.append((T,S,V))
                    g.append(EG_ind2)
                    Flow[top1.index(n1)][top2.index(n2)]=g
                    F=__dist_forest_nodes(tree1, tree2, DFV1, DFV2, top1, top2, F, mincost, n1, n2) 
                    M=__dist_nodes(tree1, tree2, DV1, DV2, top1, top2, M, F, n1,n2, cost_sup, cost_adj) 
                        
        s_L = s_L + sum(list(L.values()))

    return M, F, Flow, DV1, DFV1, DV2, Ord1, Ord2, tree2, s_L, info


# def __incremental_tree_matching_add_inter_node2(tree1, tree2, i2, M, F, Flow, DV1, DFV1, Ord1, Ord2, cost_sup, cost_adj): # Private
#     pred=[]
#     j=Ord2.index(i2)
#     i=Ord2.index(i2)+1
#     trouv=False
#     while i<len(Ord2)and not trouv:
#         if Ord2[j] in tree2.my_structure[Ord2[i]]['children']:
#             pred.append(Ord2[i])
#             trouv=True
#         else:
#             i+=1
#     if pred!=[]:
#         i1=pred[0]
                          
#     top1=Ord1[:]
#     top1.reverse()
#     top2=Ord2[:]
#     top2.reverse()

#     i3=max(Ord2)+1    
#     l1=top2.index(i2)+1
#     l0=top2.index(i2)
#     for n1 in top1:
#         l2=top1.index(n1)
#         M[l2].insert(l1,M[l2][l0])
#         M[l2][l0]=0
#         F[l2].insert(l1,F[l2][l0])
#         F[l2][l0]=0
#         c=Flow[l2][l0]
#         if c!=[]:
#             ind2=c[len(c)-1]
#             ind2[max(ind2.keys())]=i3
#             c[len(c)-1]=ind2        
#         Flow[l2].insert(l1,c)
#         Flow[l2][l0]=[]   
#     top2.insert(l1,i3)
#     Ord2=top2[:]
#     Ord2.reverse()
#     #################################################
#     tree2.my_structure[i3] = {'children': [] ,'properties':{} , 'attributes':{}}
#     tree2.my_structure[i3]['children'] = tree2.my_structure[i2]['children']
#     tree2.my_structure[i3]['properties'] = tree2.my_structure[i2]['properties']
#     tree2.my_structure[i3]['attributes'] = tree2.my_structure[i2]['attributes']
#     tree2.my_structure[i2]['children'] = [i3]
#     tree2.my_structure[i2]['properties'] = {}
#     tree2.my_structure[i2]['attributes'] = {}
#     tree2.get_property(compute_level=2)
#     info = {'node':i3,'parent':i2,'operation_name':'add_internal_node'}
#     #################################################

#     T2=__size_tree(tree2, Ord2)
#     DV2=__dist_VIDE_DAG(T2, cost_adj)
#     DFV2=__dist_VIDE_Forest(T2, cost_adj)

#     s_L=0
#     for n1 in Ord1:
#         G,EG_ind2,Evide1,Evide2,mincost=__matching_nodes_graph_add_leaf(tree1, tree2, DV1, DV2, top1, top2, M, n1, i2, i3)
#         T,S,V=__init_spanning_tree(G)
#         g=[]
#         for (i,j) in G.edges():
#                 if i!=0 and j!=0:
#                     g.append((i,j, G[i][j]['flow']))
#         g.append(Evide1)
#         g.append(Evide2)
#         g.append((T,S,V))
#         g.append(EG_ind2)
#         Flow[top1.index(n1)][top2.index(i2)]=g                
#         F = __dist_forest_nodes(tree1, tree2, DFV1, DFV2, top1, top2, F, mincost, n1, i2)
#         M = __dist_nodes(tree1, tree2, DV1, DV2, top1, top2, M, F, n1,i2, cost_sup, cost_adj)
    
#     if pred!=[]:
#         par=[]
#         j=Ord2.index(i1)
#         i=Ord2.index(i1)+1
#         while i<len(Ord2):
#             if i==len(Ord2)-1:
#                 par.append(Ord2[i])
#                 i+=1
#             elif Ord2[j] in tree2.my_structure[Ord2[i]]['children']:
#                 par.append(Ord2[i])
#                 j=i
#             else:
#                 i+=1

#         L={}
#         pivot=0
#         for n1 in Ord1:
#             for n2 in Ord2[Ord2.index(i1):]:
#                 if n2==i1 or (n2 in par):
#                     g=Flow[top1.index(n1)][top2.index(n2)]
#                     k=len(g)-1
#                     ind2=g[k]
#                     k=k-1
#                     T,S,V=g[k]
#                     EG,EG_ind2,Evide1,Evide2=__matching_nodes_graph(tree1, tree2, DV1, DV2, top1, top2, M, n1, n2, g, ind2)
#                     mincost,G,pivot,T,S,V=__network_simplex_incremental(EG,T,S,V)
#                     L[n1,n2]=pivot
#                     g=[]
#                     for (i,j) in G.edges():
#                         if i!=0 and j!=0:
#                             g.append((i,j, G[i][j]['flow']))
#                     g.append(Evide1)
#                     g.append(Evide2)
#                     g.append((T,S,V))
#                     g.append(EG_ind2)
#                     Flow[top1.index(n1)][top2.index(n2)]=g
#                     F=__dist_forest_nodes(tree1, tree2, DFV1, DFV2, top1, top2, F, mincost, n1, n2) 
#                     M=__dist_nodes(tree1, tree2, DV1, DV2, top1, top2, M, F, n1,n2, cost_sup, cost_adj) 
                        
#         s_L = s_L + sum(list(L.values()))

#     return M, F, Flow, DV1, DFV1, DV2, Ord1, Ord2, tree2, s_L, info

# ------------------------------------------------------------------------------

def incremental_zhang_edit_distance(tree1,tree2,operation,zhang_edit_distance_tree1_tree2,copy=True):

    warn_str = ''
    nam_op = operation['operation_name']

    t1 = deepcopy(tree1)
    t2 = deepcopy(tree2)

    # if copy:
    #     M = deepcopy(zhang_edit_distance_tree1_tree2['M'])
    #     F = deepcopy(zhang_edit_distance_tree1_tree2['F'])
    #     Flow = deepcopy(zhang_edit_distance_tree1_tree2['Flow'])
    #     DV1 = deepcopy(zhang_edit_distance_tree1_tree2['DV1'])
    #     DFV1 = deepcopy(zhang_edit_distance_tree1_tree2['DFV1'])
    #     Ord1 = deepcopy(zhang_edit_distance_tree1_tree2['Ord1'])
    #     Ord2 = deepcopy(zhang_edit_distance_tree1_tree2['Ord2'])
    # else:
    M = zhang_edit_distance_tree1_tree2['M']
    F = zhang_edit_distance_tree1_tree2['F']
    Flow = zhang_edit_distance_tree1_tree2['Flow']
    DV1 = zhang_edit_distance_tree1_tree2['DV1']
    DFV1 = zhang_edit_distance_tree1_tree2['DFV1']
    Ord1 = zhang_edit_distance_tree1_tree2['Ord1']
    Ord2 = zhang_edit_distance_tree1_tree2['Ord2']

    # ------------------------------------------------------------------------------

    if nam_op == 'add_leaf':
        nod_op = operation['node']
        newM, newF, newFlow, newDV1, newDFV1, newDV2, newOrd1, newOrd2, D2, s_L, info =  __incremental_tree_matching_add_any_leaf(t1, t2, nod_op, M, F, Flow, DV1, DFV1, Ord1, Ord2, 1, 1)

    elif nam_op == 'del_leaf':
        nod_op = operation['node']
        newM, newF, newFlow, newDV1, newDFV1, newDV2, newOrd1, newOrd2, D2, s_L, info =  __incremental_tree_matching_sub_any_leaf(t1, t2, nod_op, M, F, Flow, DV1, DFV1, Ord1, Ord2, 1, 1)

    elif nam_op == 'del_internal_node':
        nod_op = operation['node']
        newM, newF, newFlow, newDV1, newDFV1, newDV2, newOrd1, newOrd2, D2, s_L, info = __incremental_tree_matching_sub_inter_node(t1, t2, nod_op, M, F, Flow, DV1, DFV1, Ord1, Ord2, 1, 1)

    elif nam_op == 'add_internal_node':
        nod_op = operation['node']
        newM, newF, newFlow, newDV1, newDFV1, newDV2, newOrd1, newOrd2, D2, s_L, info = __incremental_tree_matching_add_inter_node(t1, t2, nod_op, M, F, Flow, DV1, DFV1, Ord1, Ord2, 1, 1)

    else:
        warn_str = 'Invalid operation name'
        out = zhang_edit_distance_tree1_tree2
        out['edited_tree'] = t2

    # ------------------------------------------------------------------------------

    if warn_str == '':
        out_dict = {'edited_tree':D2,'distance':newM[0][0] , 'M':newM,'F':newF,'Flow':newFlow,'DV1':newDV1,'DFV1':newDFV1, 'DV2':newDV2, 'Ord1':newOrd1,'Ord2':newOrd2 , 'info':info}
        return out_dict

    else:
        print(warn_str)
        return out
