# -*- python -*-
# -*- coding: utf-8 -*-
#
#       src/ciphering
#
#       File author(s):
#			Florian Ingels <florian.ingels@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------

'''

Called by this module : treex.tree, treex.simulation, itertools.count, itertools.product, random, math, copy.deepcopy, random.randrange

This module implements the algorithms detailed in [ING]. The main goal is to determine whether between two (unordered) trees, `t1` and `t2`, there exists a tree isomorphism such that the implied mapping of the labels of `t1` and `t2` is also a bijection. We call such an isomorphism a tree ciphering.

.. rubric:: References

.. [ING] Ingels, F. & Azaïs R. "Isomorphic unordered labeled trees up to substitution ciphering." arXiv preprint arXiv:2105.05685 (2021).

- File author(s): Florian Ingels <florian.ingels@inria.fr>

'''

from treex.tree import *
from treex.simulation import *
from itertools import count, product
import random
import math
from copy import deepcopy
from random import randrange


# ------------------------------------------------------------------------------

class Bijection(dict):
    '''

    This class implements Bijection as bidirictional dictionaries, adapted from https://stackoverflow.com/questions/3318625/how-to-implement-an-efficient-bidirectional-hash-table.

    The proper way to construct a Bijection is to use the method extend().
    '''

    def __init__(self, *args, **kwargs):
        super(Bijection, self).__init__(*args, **kwargs)
        self.inverse = {}
        for key, value in self.items():
            self.inverse[value] = key

    def __setitem__(self, key, value):
        super(Bijection, self).__setitem__(key, value)
        self.inverse[value] = key

    def __delitem__(self, key):
        del self.inverse[self[key]]
        super(Bijection, self).__delitem__(key)

    def extend(self, a, b):
        '''
        The parameters `a` and `b` must non-mutable objects. This method returns True if the couple (a,b) can be added to the bijection (or if it already exists) and False otherwise. If True, the couple is added to the bijection.
        '''
        try:
            if a in self:
                assert self[a] == b
            if b in self.inverse:
                assert self.inverse[b] == a
            self.__setitem__(a, b)
        except ValueError:
            print('Cannot extend the bijection')

    def copy(self):
        '''
        Returns a deep copy of the bijection.
        '''
        return deepcopy(self)

    def compose(self,g):
        '''
        Returns the composition of two Bijections.
        :param g: Bijection
        :return: f o g
        '''
        assert set(g.inverse.keys()).issubset(set(self.keys())), 'Support does not match'
        h = Bijection()
        for a in g.keys():
            h.extend(a, self[g[a]])
        return h

# ------------------------------------------------------------------------------

class Bag(object):
    """
    This class implements Bags, that are made of two sets of nodes, one from T1, one from T2, such that they are candidates to be mapped together in the tree isomorphism.
    """
    __slots__ = 's1', 's2', 'id', 'card'

    __new_id__ = count()

    def __init__(self, s1, s2, force_id=None):
        """
        The two list of nodes myst be of equal length.
        :param s1: list of nodes of T1
        :param s2: list of nodes of T2
        :param force_id: optional. Force the id of the bag to be equal to the parameter.
        """
        assert len(s1) == len(s2), 'Size of sets do not match'
        self.card = len(s1)
        self.s1 = s1
        self.s2 = s2
        if force_id is None:
            self.id = next(Bag.__new_id__)
        else:
            self.id = force_id

        for elt in s1 + s2:
            elt.add_attribute_to_id('mapping', ('bag', self.id))

    def delete_couple(self, u, v):
        '''
        Remove the node `u` from the set of nodes from T1, and the node `v` from the set of nodes from T2.
        '''
        self.s1.remove(u)
        self.s2.remove(v)
        self.card -= 1


class BagDic(dict):
    """
    Implement a dictionary of Bags, meant to gather all the bags manipulated in the algorithm.
    """

    def __init__(self, *args, **kwargs):
        super(BagDic, self).__init__(*args, **kwargs)
        self.to_map = []

    def list_of_ids(self):
        """
        Return the list of bags' id contained in the BagDic.
        """
        return list(self.keys())

    def add(self, bag):
        """
        Add a Bag to the Bagdic.
        """
        assert isinstance(bag, Bag)
        if bag.card > 0:
            self[bag.id] = bag
        if bag.card == 1:
            self.to_map.append(bag.id)

    def deduction(self, state, display=False):
        """
        Whenever a bag is such that its sets of nodes contains each only 1 node, they must be mapped together. This method parses all bags and perform the adequate mappings.
        """
        deduction=False
        while len(self.to_map) > 0:
            deduction = True
            i = self.to_map.pop()
            if display:
                print('Deduction:', self[i].s1[0].my_id, self[i].s2[0].my_id)
            MapNodes(self[i].s1[0], self[i].s2[0], state)
        return deduction

    def extract(self, id):
        """
        Remove and return the bag of given id from the BagDic
        """
        if id in self.to_map:
            self.to_map.remove(id)
        return self.pop(id)

    def split_bag(self,bag_id,lu,lv,state):
        """
        When the nodes `u` and `v` are mapped together, all their children belonging to the same bag can be placed together in another bag, separated from the nodes that are not children of `u` or `v`. This method performs that splitting operation.
        """
        bag = self.extract(bag_id)
        s1p = [i for i in bag.s1 if i in lu]
        s2p = [i for i in bag.s2 if i in lv]
        s1pp = [i for i in bag.s1 if i not in lu]
        s2pp = [i for i in bag.s2 if i not in lv]

        if len(s1p)>0:
            self.add(Bag(s1p, s2p))
            recursive_split(s1p,s2p,state)
        if len(s1pp)>0:
            self.add(Bag(s1pp, s2pp))
            recursive_split(s1pp,s2pp,state)

# ------------------------------------------------------------------------------

class Collection(object):
    '''
    This class implements Collection, that are sets of nodes from T1 and T2 that are candidates to form bags together. All nodes from a same set msut share the same label.
    '''
    __slots__ = 'id', 's1', 's2', 'correct'

    __new_id__ = count()

    def __init__(self, force_id=None):
        """
        The parameter `force_id` is optional and force the id of the Collection to be equal to the value given.
        """
        if force_id == None:
            self.id = next(Collection.__new_id__)
        else:
            self.id = force_id
        self.s1 = {}
        self.s2 = {}
        self.correct = True

    def assert_correct(self):
        """
        Check whether the Collection is well built or not.
        """
        assert self.s1.keys() == self.s2.keys(), 'Integers cardinality do not match'
        for n in self.s1.keys():
            assert len(self.s1[n]) == len(self.s2[n]), 'Size of sets do not match'
        self.correct = True

    def add(self, switch, label, nodes):
        """
        :param switch: 1 or 2. Indicates the origin, T1 or T2.
        :param label: the value of the label shared by all nodes
        :param nodes: set of nodes from T1 or T2

        Add the given `nodes` to the collection. They will be stored according to their label and their number of elements.

        """
        n = len(nodes)

        if n > 0:
            if switch == 1:
                if n in self.s1.keys():
                    if label not in self.s1[n].keys():
                        self.s1[n][label] = [nodes]
                    else:
                        self.s1[n][label].append(nodes)
                else:
                    self.s1[n] = {label: [nodes]}
            else:
                if n in self.s2.keys():
                    if label not in self.s2[n].keys():
                        self.s2[n][label] = [nodes]
                    else:
                        self.s2[n][label].append(nodes)
                else:
                    self.s2[n] = {label: [nodes]}

        for elt in nodes:
            elt.add_attribute_to_id('mapping', ('collection', self.id, n, label))
        self.correct = False

    def extract(self, switch, n, label):
        """
        :param switch: 1 or 2. Indicates the origin, T1 or T2.
        :param n: integer
        :param label:
        :return: The set of nodes that have cardinality `n` and where all nodes share the same `label` value.
        """
        if switch == 1:
            nodes_list = self.s1[n].pop(label)
            if len(self.s1[n]) == 0:
                del self.s1[n]
        else:
            nodes_list = self.s2[n].pop(label)
            if len(self.s2[n]) == 0:
                del self.s2[n]
        self.correct = False
        return nodes_list

    def remove(self, switch, n, label, node):
        """
        Extracts and returns the list of nodes with given `switch`, `n` and `label` that contains the selected `node` - if found.
        It also remove the selected `node` from the set of nodes returned.
        """

        nodes_list = self.extract(switch, n, label)

        s=[]

        for nodes in nodes_list:
            if node in nodes:
                s = nodes
                s.remove(node)
            else:
                self.add(switch, label, nodes)
        return s

    def cardinal_list(self):
        """
        Return the list of all distinct values `n` of the cardinalities of the sets stored in the collection.
        """
        assert self.correct, 'Collection is not correct'
        return list(self.s1.keys())

    def deduction(self, state):
        """
        This method apply all the deduction rules on collections, as defined in [REF TO PROVIDE]
        """
        assert self.correct, 'Collection is not correct'
        deduction = False
        coll_dic = {}
        for n in self.cardinal_list():
            if len(self.s1[n]) == 1:
                a = list(self.s1[n].keys())[0]
                b = list(self.s2[n].keys())[0]
                if (a, b) not in state.f.items():
                    deduction = True
                    state.f.extend(a, b)
                if len(self.s1[n][a]) == 1:
                    deduction = True
                    state.bags.add(Bag(self.extract(1, n, a)[0], self.extract(2, n, b)[0]))
            else:
                inter1 = set(self.s1[n].keys()).intersection(set(state.f.keys()))
                inter2 = set(self.s2[n].keys()).intersection(set(state.f.inverse.keys()))
                if len(self.s1[n].keys()) > 1 and len(inter1) > 0:  # TODO: revérifier !
                    deduction = True
                    for a in inter1:
                        if a not in coll_dic.keys():
                            coll_dic[a] = Collection()
                        assert state.f[a] in self.s2[n].keys(), 'Labels do not correspond'
                        s1 = self.extract(1, n, a)
                        s2 = self.extract(2, n, state.f[a])
                        if len(s1) == 1:
                            state.bags.add(Bag(s1[0], s2[0]))
                        else:
                            for nodes in s1:
                                coll_dic[a].add(1, a, nodes)
                            for nodes in s2:
                                coll_dic[a].add(2, state.f[a], nodes)
                        inter2.remove(state.f[a])
                    assert len(inter2) == 0, 'Nodes remains in the collection'
        for a in coll_dic.keys():
            coll_dic[a].assert_correct()
            if len(coll_dic[a].cardinal_list()) > 0:
                state.collections.add(coll_dic[a])
        return deduction


class CollectionDic(dict):
    """
    Implement a dictionary of Collections, meant to gather all the collections manipulated in the algorithm.
    """

    def __init__(self, *args, **kwargs):
        super(CollectionDic, self).__init__(*args, **kwargs)

    def list_of_ids(self):
        """
        Return the list of bags' id contained in the BagDic.
        """
        return list(self.keys())

    def add(self, collection):
        """
        Add a Collection to the CollectionDic.
        """
        assert isinstance(collection, Collection), 'Not a collection'
        collection.assert_correct()
        if len(collection.cardinal_list()) > 0:
            self[collection.id] = collection

    def extract(self, id):
        """
        Remove and return the collection of given id from the CollectionDic.
        """
        return self.pop(id)

    def deduction_phase(self, state):
        """
        This method parses the deduction rules (deduction_label() and deduction_cardinal()) for the collection in the CollectionDic.
        """

        deduction = False
        for id in self.list_of_ids():
            collection = self.extract(id)
            deduction += collection.deduction(state)
            self.add(collection)
        if deduction:
            _ = self.deduction_phase(state)

        return deduction

    def split_collection(self,collections_to_split,state):
        for id in collections_to_split.keys():
            collection = self.extract(id)
            coll1 = Collection()
            coll2 = Collection()
            lup = {}
            lupp = {}
            lvp = {}
            lvpp = {}
            for switch, n, attribute,ll in collections_to_split[id]:
                nodes_list = collection.extract(switch, n, attribute)
                for nodes in nodes_list:
                    sp = [i for i in nodes if i in ll]
                    spp = [i for i in nodes if i not in ll]
                    if len(sp) > 0 and len(spp)>0:
                        coll1.add(switch, attribute, sp)
                        if switch==1:
                            if n not in lup.keys():
                                lup[n]=[]
                            lup[n]+=sp
                        else:
                            if n not in lvp.keys():
                                lvp[n]=[]
                            lvp[n]+=sp
                        coll2.add(switch, attribute, spp)
                        if switch==1:
                            if n not in lupp.keys():
                                lupp[n]=[]
                            lupp[n]+=spp
                        else:
                            if n not in lvpp.keys():
                                lvpp[n]=[]
                            lvpp[n]+=spp
                    else:
                        collection.add(switch, attribute, nodes)
            self.add(coll1)
            self.add(coll2)
            self.add(collection)

            assert lup.keys()==lvp.keys()
            for n in lup.keys():
                recursive_split(lup[n],lvp[n],state)
            assert lupp.keys()==lvpp.keys()
            for n in lupp.keys():
                recursive_split(lupp[n],lvpp[n],state)

# ------------------------------------------------------------------------------

def recursive_split(l1,l2,state):

    child1 = []
    for u in l1:
        child1+=u.my_children

    child2 = []
    for u in l2:
        child2+=u.my_children

    bags_to_split_1 = {}
    bags_to_split_2 = {}
    collections_to_split_1 = {}
    collections_to_split_2 = {}

    for u in child1:
        attr = u.get_attribute('mapping')
        if attr[0]== 'bag':
            if attr[1] not in bags_to_split_1.keys():
                bags_to_split_1[attr[1]]=[]
            bags_to_split_1[attr[1]].append(u)

        elif attr[0]=='collection':
            if attr[1:] not in collections_to_split_1.keys():
                collections_to_split_1[attr[1:]]=[]
            collections_to_split_1[attr[1:]].append(u)

    for v in child2:
        attr = v.get_attribute('mapping')
        if attr[0] == 'bag':
            if attr[1] not in bags_to_split_2.keys():
                bags_to_split_2[attr[1]] = []
            bags_to_split_2[attr[1]].append(v)

        elif attr[0] == 'collection':
            if attr[1:] not in collections_to_split_2.keys():
                collections_to_split_2[attr[1:]] = []
            collections_to_split_2[attr[1:]].append(v)

    assert bags_to_split_1.keys() == bags_to_split_2.keys()
    for id in bags_to_split_1.keys():
        assert len(bags_to_split_1[id]) == len(bags_to_split_2[id])

    assert len(collections_to_split_1.keys()) == len(collections_to_split_2.keys())

    for id in bags_to_split_1.keys():
        state.bags.split_bag(id,bags_to_split_1[id],bags_to_split_2[id],state)

    collections_to_split = {}

    for id, n, attribute in collections_to_split_1.keys():
        if id not in collections_to_split.keys():
            collections_to_split[id] = []
        collections_to_split[id].append((1, n, attribute, collections_to_split_1[id,n,attribute]))

    for id, n, attribute in collections_to_split_2.keys():
        if id not in collections_to_split.keys():
            collections_to_split[id] = []
        collections_to_split[id].append((2, n, attribute,collections_to_split_2[id,n,attribute]))

    state.collections.split_collection(collections_to_split,state)

def MapNodes(u, v, state):
    """
    This functions maps two nodes `u` and `v`, remove their unmapped children from their respective bags & collections and gather them in a specific bag or collection, and recursively map their parents if needed.
    """
    assert u.get_attribute('isom_unordered') == v.get_attribute('isom_unordered'), 'Nodes are not isomorphic'

    state.f.extend(u.get_attribute(state.labels), v.get_attribute(state.labels))
    state.phi.extend(u.my_id, v.my_id)

    if u.get_attribute('mapping')[0] == 'bag':
        assert v.get_attribute('mapping')[0] == 'bag'
        assert u.get_attribute('mapping')[1] == v.get_attribute('mapping')[1]

        bag = state.bags.extract(u.get_attribute('mapping')[1])
        bag.delete_couple(u, v)
        state.bags.add(bag)

    elif u.get_attribute('mapping')[0] == 'collection':
        assert v.get_attribute('mapping')[0] == 'collection'

        tupu = u.get_attribute('mapping')
        tupv = v.get_attribute('mapping')

        assert tupu[1:3] == tupv[1:3]

        collection = state.collections.extract(tupu[1])
        s1 = collection.remove(1, tupu[2], tupu[3], u)
        s2 = collection.remove(2, tupv[2], tupv[3], v)
        state.bags.add(Bag(s1, s2))
        state.collections.add(collection)

    recursive_split([u],[v],state)

    u.add_attribute_to_id('mapping', ('done',))
    v.add_attribute_to_id('mapping', ('done',))

    p, q = u.my_parent, v.my_parent
    if p is None:
        assert q is None, 'Root is not mapped with the root'
    else:
        if (p.my_id, q.my_id) not in state.phi.items():
            MapNodes(p, q, state)


# ------------------------------------------------------------------------------

def _save_ids(nodes):
    return [elt.my_id for elt in nodes]


def _retrieve_ids(ids, t):
    return [elt for elt in t.list_of_subtrees() if elt.my_id in ids]


# ------------------------------------------------------------------------------

class CipheringState(object):
    """
    This class implements the state of the system, gathering several informations.
    """

    __slots__ = 'phi', 'f', 'bags', 'collections', 'labels'

    def __init__(self):
        """
        :param phi: the bijection on the nodes of the trees
        :param f: the bijection on the labels of the trees
        :param bags: the bags containing unmapped nodes
        :param collections: the collections containing unmapped ndodes
        :param labels: the name of the label attribute on the trees
        """
        self.phi = Bijection({})
        self.f = Bijection({})
        self.bags = BagDic()
        self.collections = CollectionDic()
        self.labels = None

    # def init(self, t1, t2, labels):
    #     """
    #     Initialize the state.
    #     """
    #     self.bags.add(Bag(t1.list_of_subtrees(), t2.list_of_subtrees()))
    #     self.labels = labels

    def init(self,t1,t2,labels):
        """
        Initialize the state with histogram of labels.
        """
        self.labels=labels

        ### construction
        dic1 = {}
        for st in t1.list_of_subtrees():
            lab = st.get_attribute(labels)
            if lab not in dic1.keys():
                dic1[lab]=[]
            dic1[lab].append(st)
        dic2 = {}
        for st in t2.list_of_subtrees():
            lab = st.get_attribute(labels)
            if lab not in dic2.keys():
                dic2[lab]=[]
            dic2[lab].append(st)

        h1 = {}
        for label in dic1.keys():
            n = len(dic1[label])
            if n not in h1.keys():
                h1[n]=[]
            h1[n].append(label)

        h2 = {}
        for label in dic2.keys():
            n = len(dic2[label])
            if n not in h2.keys():
                h2[n]=[]
            h2[n].append(label)

        ### verification

        assert h1.keys()==h2.keys()
        for n in h1.keys():
            assert len(h1[n])==len(h2[n])

        ### initialization

        for n in h1.keys():
            s1 = []
            for label in h1[n]:
                s1+=dic1[label]
            s2 = []
            for label in h2[n]:
                s2+=dic2[label]
            self.bags.add(Bag(s1,s2))

    def depth_filter(self, display=False):
        """
        Filter the nodes by depth.
        :param display: boolean, optional (default is False). Whether the deductions made during the filter should be printed on screen or not.
        """
        if display:
            print('___ DEPTH ____')

        for id in self.bags.list_of_ids():
            bag = self.bags.extract(id)
            d1 = {}
            d2 = {}
            for elt in bag.s1:
                d = elt.get_property('depth')
                if d in d1.keys():
                    d1[d].append(elt)
                else:
                    d1[d] = [elt]
            for elt in bag.s2:
                d = elt.get_property('depth')
                if d in d2.keys():
                    d2[d].append(elt)
                else:
                    d2[d] = [elt]
            for d in d1.keys():
                self.bags.add(Bag(d1[d], d2[d]))

        _ = self.bags.deduction(self, display)

    def parent_filter(self, display=False):
        """
        Filter the nodes by parents.
        :param display: boolean, optional (default is False). Whether the deductions made during the filter should be printed on screen or not.
        """
        if display:
            print('___ PARENT ____')
        depth = {}
        for id in self.bags.list_of_ids():
            d = self.bags[id].s1[0].get_property('depth')
            if d not in depth.keys():
                depth[d] = [id]
            else:
                depth[d].append(id)

        for d in sorted(depth.keys()):
            for id in depth[d]:
                bag = self.bags.extract(id)
                p1 = {}
                for u in bag.s1:
                    sig = u.my_parent.get_attribute('mapping')
                    if sig not in p1.keys():
                        p1[sig] = [u]
                    else:
                        p1[sig].append(u)
                p2 = {}
                for u in bag.s2:
                    sig = u.my_parent.get_attribute('mapping')
                    if sig not in p2.keys():
                        p2[sig] = [u]
                    else:
                        p2[sig].append(u)
                assert p1.keys() == p2.keys()

                for sig in p1.keys():
                    self.bags.add(Bag(p1[sig], p2[sig]))

        _ =self.bags.deduction(self, display)

    def equivalence_class_filter(self, display=False):
        """
        Filter the nodes by equivalence class.
        :param display: boolean, optional (default is False). Whether the deductions made during the filter should be printed on screen or not.
        """
        if display:
            print('___ EQUIVALENCE CLASS ____')

        for id in self.bags.list_of_ids():
            bag = self.bags.extract(id)
            c1 = {}
            c2 = {}
            for elt in bag.s1:
                c = elt.get_attribute('isom_unordered')
                if c in c1.keys():
                    c1[c].append(elt)
                else:
                    c1[c] = [elt]
            for elt in bag.s2:
                c = elt.get_attribute('isom_unordered')
                if c in c2.keys():
                    c2[c].append(elt)
                else:
                    c2[c] = [elt]

            assert c1.keys() == c2.keys()

            for c in c1.keys():
                self.bags.add(Bag(c1[c], c2[c]))

        _ = self.bags.deduction(self, display)

    def label_filter(self, display=False):
        """
        Filter the nodes by labels.
        :param display: boolean, optional (default is False). Whether the deductions made during the filter should be printed on screen or not.
        """
        if display:
            print('___ LABELS ____')

        for id in self.bags.list_of_ids():
            bag = self.bags.extract(id)
            attr1 = {}
            attr2 = {}
            for elt in bag.s1:
                a = elt.get_attribute(self.labels)
                if a in attr1.keys():
                    attr1[a].append(elt)
                else:
                    attr1[a] = [elt]
            for elt in bag.s2:
                a = elt.get_attribute(self.labels)
                if a in attr2.keys():
                    attr2[a].append(elt)
                else:
                    attr2[a] = [elt]

            col = Collection()

            for a in attr1.keys():
                col.add(1, a, attr1[a])
            for a in attr2.keys():
                col.add(2, a, attr2[a])

            self.collections.add(col)

        deduction=True
        while deduction:
            deduction=False
            deduction += self.collections.deduction_phase(self)
            deduction += self.bags.deduction(self, display)

    def save(self):
        """
        Returns a save of the system.
        """
        b_list = []
        c_list = []
        for id in self.bags.list_of_ids():
            b_list.append((id, _save_ids(self.bags[id].s1), _save_ids(self.bags[id].s2)))
        for id in self.collections.list_of_ids():
            c = [id]
            for n in self.collections[id].cardinal_list():
                for a in self.collections[id].s1[n].keys():
                    for nodes in self.collections[id].s1[n][a]:
                        c.append((1, a, _save_ids(nodes)))
                for a in self.collections[id].s2[n].keys():
                    for nodes in self.collections[id].s2[n][a]:
                        c.append((2, a, _save_ids(nodes)))
            c_list.append(c)
        return b_list, c_list, self.f.copy(), self.phi.copy(), self.labels

    def generate_choices(self):
        """
        Generates a list of candidates to be mapped for the backtracking procedure.
        """
        if len(self.bags.list_of_ids()) == 0:
            if len(self.collections.list_of_ids()) == 0:
                return 'done',
            else:
                min_card = float('Inf')
                max_n = 0
                select_id = None
                for id in self.collections.list_of_ids():
                    for n in self.collections[id].cardinal_list():
                        if n>max_n:
                            min_card = len(self.collections[id].s1[n])
                            max_n = n
                            select_id = id
                        elif n==max_n and len(self.collections[id].s1[n]) < min_card :
                            min_card = len(self.collections[id].s1[n])
                            select_id = id

                couple_list = []
                for a in self.collections[select_id].s2[max_n]:
                    for i in range(len(self.collections[select_id].s2[max_n][a])):
                        couple_list.append((select_id, max_n, list(self.collections[select_id].s1[max_n].keys())[0], a, i))
                return 'collection', couple_list
        else:
            min_card = float('Inf')
            min_id = None
            for id in self.bags.list_of_ids():
                if self.bags[id].card < min_card:
                    min_card = self.bags[id].card
                    min_id = id
            couple_list = []
            for elt in self.bags[min_id].s2:
                couple_list.append((self.bags[min_id].s1[0].my_id, elt.my_id))
            return 'bag', couple_list

    def current_size(self, log=False):
        """
        The size of the search space for a ciphering depending on the current status of mappings, bags, and collections. If the optional parameter `log` is set to True (default is False), the log10 value of the number is returned.
        """
        try:
            if log:
                perm = 0
            else:
                perm = 1
            for bag in self.bags.values():
                if log:
                    perm += math.log10(math.factorial(bag.card))
                else:
                    perm *= math.factorial(bag.card)
            for collection in self.collections.values():
                for n in collection.cardinal_list():
                    if log:
                        perm += math.log10(math.factorial(n)) * (len(collection.s1[n])) + math.log10(
                            math.factorial(len(collection.s1[n])))
                    else:
                        perm *= (math.factorial(n) ** (len(collection.s1[n]))) * math.factorial(len(collection.s1[n]))
            return perm
        except ValueError:
            print('Calculation does not make sense')

    def merge_collections(self):
        """
        Merge the collections to reform bags.
        """
        for id in self.collections.list_of_ids():
            collection = self.collections.extract(id)
            for n in collection.cardinal_list():
                s1 = []
                for label in list(collection.s1[n].keys()):
                    for nodes in collection.extract(1, n, label):
                        s1 += nodes
                s2 = []
                for label in list(collection.s2[n].keys()):
                    for nodes in collection.extract(2, n, label):
                        s2 += nodes
                self.bags.add(Bag(s1, s2))


def restore_state(t1, t2, b_list, c_list, f, phi, labels):
    """
    Reconstruct a CipheringState from a previous save.
    """
    bags = BagDic()
    for id, s1, s2 in b_list:
        bags.add(Bag(_retrieve_ids(s1, t1), _retrieve_ids(s2, t2), force_id=id))
    collections = CollectionDic()
    for c in c_list:
        col = Collection(force_id=c[0])
        for i, a, nodes in c[1:]:
            if i == 1:
                col.add(i, a, _retrieve_ids(nodes, t1))
            else:
                col.add(i, a, _retrieve_ids(nodes, t2))
        collections.add(col)

    state = CipheringState()
    state.bags = bags
    state.collections = collections
    state.f = f.copy()
    state.phi = phi.copy()
    state.labels = labels
    return state


# ------------------------------------------------------------------------------

def PreProcessing(t1, t2, labels, display=False):
    """
    Perform the preprocessing phase of the ciphering algorithm, without the backtracking.

    t1,t2 : Trees

    labels: the name of the attribute that will be used as labels

    display: optional, boolean (default=False). Whether the steps of the algorithm should be displayed on the terminal or not.

    Returns a tuple whose first element is a boolean stating if the Preprocessing part went OK or not. If True, also returns the state of the two mappings (on labels and nodes) along with the remaining bags and collection, as a CipheringState object.

    """
    try:
        # if 'isom_unordered' not in t1.get_attribute() or 'isom_unordered' not in t2.get_attribute():
        assert t1.is_isomorphic_to(t2), 'The trees are not isomorphic'

        state = CipheringState()

        state.init(t1, t2, labels)

        state.depth_filter(display)

        state.equivalence_class_filter(display)

        state.parent_filter(display)

        state.label_filter(display)

        if display:
            print('___ END OF PREPROCESSING ____')

        return True, state
    except:
        return False,


# ------------------------------------------------------------------------------

def BackTracking(state, t1, t2, display=False, recursion=0):
    """
    Perform the backtracking phase of the ciphering algorithm, following the preprocessing.

    t1,t2 : Trees

    state: as returned by PreProcessing()

    display: optional, boolean (default=False). Whether the steps of the algorithm should be displayed on the terminal or not.

    Returns a tuple whose first element is a boolean stating if the BackTracking part went OK or not. If True, also returns the two mappings (on labels and nodes).

    """

    if display:
        print('\t' * recursion + 'START BACKTRACKING >')

    tu = state.generate_choices()
    save_tup = state.save()

    if tu[0] == 'done':
        if display:
            print('\t' * recursion + '< IT IS A WIN')
        return True, state.f, state.phi
    elif tu[0] == 'bag':
        for u, v in tu[1]:
            try:
                if display:
                    print('Mapped nodes:', u, v)
                MapNodes(t1.subtree(node=u), t2.subtree(node=v), state)
                deduction = True
                while deduction:
                    deduction = False
                    deduction += state.collections.deduction_phase(state)
                    deduction += state.bags.deduction(state, display)
                tu = BackTracking(state, t1, t2, display, recursion + 1)
                if tu[0]:
                    if display:
                        print('\t' * recursion + '< IT IS A WIN')
                    return tu
            except:
                if display:
                    print('\t' * recursion + '- UNDESIRABLE CHOICE -')
            state = restore_state(t1, t2, *save_tup)
        if display:
            print('\t' * recursion + '< OUT OF OPTIONS')
        return False,
    else:
        for id, n, a, b, i in tu[1]:
            try:
                if display:
                    print('Mapped attributes:', a, b)
                state.f.extend(a, b)
                state.bags.add(
                    Bag(state.collections[id].extract(1, n, a)[0], state.collections[id].extract(2, n, b)[i]))
                state.collections[id].assert_correct()
                deduction = True
                while deduction:
                    deduction = False
                    deduction += state.collections.deduction_phase(state)
                    deduction += state.bags.deduction(state, display)
                tu = BackTracking(state, t1, t2, display, recursion + 1)
                if tu[0]:
                    if display:
                        print('\t' * recursion + '< IT IS A WIN')
                    return tu
            except:
                if display:
                    print('\t' * recursion + '- UNDESIRABLE CHOICE -')
            state = restore_state(t1, t2, *save_tup)
        if display:
            print('\t' * recursion + '< OUT OF OPTIONS')
        return False,


# -----------------------------------------------------------------------------

def ciphering_isomorphism(t1, t2, labels):
    """
    Return a tuple whose first element is a boolean answering "is there a tree ciphering between `t1` and `t2` ?" If the answer is Yes, the tuple also contains the bijection between the labels and the bijection between the nodes. The parameter `labels` stand for the name of the attribute that will be used as labels.
    """

    tu = PreProcessing(t1, t2, labels)

    if not tu[0]:
        return False,
    else:
        _, state = tu
        tu = BackTracking(state, t1, t2)
        return tu

# -----------------------------------------------------------------------------

def isom_count(t, log=False):
    """
    :param t: Tree
    :param log: Boolean, optional (default is False)
    :return: the number of (unordered) isomorphic trees to `t`, or the log10 of this number if the parameter `log` is equal to True.
    """
    if 'isom_unordered' not in t.get_attribute():
        t.is_isomorphic_to(t)
    if log:
        isom = 0
    else:
        isom = 1
    for i in t.list_of_ids():
        lab = [c.get_attribute('isom_unordered') for c in t.subtree(i).my_children]
        for elt in set(lab):
            if log:
                isom += math.log10(math.factorial(lab.count(elt)))
            else:
                isom *= math.factorial(lab.count(elt))
    return isom

# -----------------------------------------------------------------------------

def regression_ciphering(t1,t2,labels,objective,estimate,error):
    """

    Computes a tree ciphering and the regression function estimated on the ciphering using the greedy algorithm of [XXX].

    :param t1,t2: Trees
    :param labels: As for ciphering_isomorphism
    :param objective: the parametric function used
    :param estimate: the function that estimates the objective from a list of data
    :param error: the function that computes the error committed by the estimation on a list of data
    """

    tu = PreProcessing(t1, t2, labels, False)

    if not tu[0]:
        return False,
    else:
        _, state = tu
        state.merge_collections()
        param = estimate(state.f,objective) ## TODO: prendre en compte le cas où il n'y a pas assez de points pour estimer

        while len(state.bags.list_of_ids())>0:
            min_couple = None
            min_error = float('inf')
            for id in state.bags.list_of_ids():
                for u, v in itertools.product(state.bags[id].s1, state.bags[id].s2):
                    e = error(Bijection({u.get_attribute(state.labels): v.get_attribute(state.labels)}), objective, param)
                    if e < min_error:
                        min_error = e
                        min_couple = u, v
            u, v = min_couple
            MapNodes(u, v, state)
            _ = state.bags.deduction(state)
            param = estimate(state.f,objective)

        return True,param, state.f, state.phi