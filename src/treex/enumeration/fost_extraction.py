# -*- python -*-
# -*- coding: utf-8 -*-
#
#       treex.enumeration.fost_extraction
#
#       File author(s):
#			Florian Ingels <florian.ingels@inria.fr>
#           
#
#       Distributed under the LGPL License.
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# ------------------------------------------------------------------------------
"""
Called by this module : treex.dag, copy.deepcopy

This module implements the algorithm proposed in [CITE] for extracting forests of subtrees (FoST) from DAGs. It introduces a
new class, Fost, as well as the methods and functions to process these objects.

.. rubric:: References

.. [ING] Enumeration of Unordered Forests, F. Ingels & R. Azaïs, https://arxiv.org/abs/2003.08144 [preprint]

- File author(s): Florian Ingels <florian.ingels@inria.fr>
"""
from treex.lossless_compression.dag import *

from copy import deepcopy


class Fost(object):
    """
    This class implements the forests of subtrees described in [CITE].

    A forest of subtrees of a DAG :math:`D=(V,E)` can be described only by a set of nodes :math:`V_0 \subseteq V`.

    Other parameters are also computed in order to factorize some algorithms.

    Attributes
    ----------

    nodes: list of int
        a list of nodes :math:`V_0` from the source DAG

    source: Dag
        a reference pointing to the DAG in which the FoST lives.

    candidates: list of int
        this attribute is meant to expand the FoST and is the list of nodes in the source DAG that can be added to the FoST such that the resulting object is still a FoST.

    origins: set
        The set of origins (from the source DAG) in which the FoST appears.

    presence: dict
        For each origin appearing in the source DAG, gives the number of times the FoST appears in the tree compressed by the DAG. Note that this number is non-zero only for the origins in the `origins` attribute.

    attributes: dict
        If some special attributes are computed, they will be stored here

    See Also
    --------
    create_fost(...), extract_fost(...)

    References
    ----------
    See [ING].

    """

    __slots__ = 'nodes', 'source', 'candidates', 'presence', 'origins', 'attributes'

    def __init__(self):
        self.nodes = []
        self.source = None
        self.candidates = []
        self.presence = {}
        self.origins = set()
        self.attributes={}

    def __str__(self):
        return self.__string_pattern()

    # --------------------------------------------------------------

    def __string_pattern(self):
        s = 'Forest of subtrees containing nodes ' + str(self.nodes) + ' from source DAG\n'
        return s

    def copy(self):
        """
        This method allows a deepcopy of a FoST object. The `source` attribute is not a deepcopy as it is meant to be just a reference to the DAG object.
        :return: Stuff
        """
        p = Fost()
        p.nodes = deepcopy(self.nodes)
        p.candidates = deepcopy(self.candidates)
        p.source = self.source
        p.presence = deepcopy(self.presence)
        p.origins = deepcopy(self.origins)
        p.attributes=deepcopy(self.attributes)
        return p

    def add_attribute(self,attribute_name,attribute_value):
        """
        This method add an attribute to the FoST object.
        """
        self.attributes[attribute_name]=attribute_value

    def get_attribute(self,attribute):
        """
        This method returns the desired attribute if it already exist.
        """
        try:
            return self.attributes[attribute]
        except:
            print('Requested attribute does not exist')

    # --------------------------------------------------------------

    def to_dag(self):
        """
        Given a FoST object, construct the implied DAG it represents.
        :return: Dag
        """
        d = Dag()
        d.my_leaves = self.source.my_leaves
        for node in self.nodes:
            d.my_structure[node] = self.source.my_structure[node]
        d.assign_roots()
        for root in d.my_roots:
            for origin in self.origins:
                if origin in d.get_attribute(root, 'origin'):
                    d.my_origins[origin] = root
        return d

    # --------------------------------------------------------------

    def __find_candidates(self, nodes_sorted):

        v = max(self.nodes)

        h = self.source.get_property(v, 'height')

        nodes_widening = nodes_sorted[h]
        nodes_elongation = nodes_sorted[h + 1] if h < max(nodes_sorted.keys()) else []
        s = []

        for n in nodes_widening:
            if set(self.source.children(n)) <= set(self.nodes) and self.source.children(n) > self.source.children(v):
                s.append(n)

        for n in nodes_elongation:
            if set(self.source.children(n)) <= set(self.nodes):
                s.append(n)

        self.candidates = s

    def __update_fost(self, node, nodes_sorted):  # Private

        self.nodes.append(node)

        s = [i for i in self.candidates if i > node]

        h = self.source.get_property(node, 'height')

        nodes_elongation = nodes_sorted[h + 1] if h < max(nodes_sorted.keys()) else []

        for n in nodes_elongation:
            if set(self.source.children(n)) <= set(self.nodes) and n not in s:
                s.append(n)

        self.candidates = s
        self.origins = self.origins.intersection(set(self.source.get_attribute(node, 'origin')))

        for origin in self.presence.keys():
            if origin in self.source.get_attribute(node, 'origin'):
                self.presence[origin] = min(self.presence[origin], self.source.get_attribute(node, 'origin')[origin])
            else:
                self.presence[origin] = 0

# --------------------------------------------------------------

def create_fost(dag, nodes):
    """
    This function allows the creation of a unique FoST object from the specified nodes.

    :param dag: Dag
        The source DAG for the FoST object
    :param nodes: list of int, or int
        All nodes in the FoST. They must be such that for each node, its children must also be in the set.
    :return: Fost
    """
    p = Fost()

    if not isinstance(nodes,list):
        nodes=[nodes]

    p.nodes = nodes
    p.source = dag

    o = set(dag.my_origins.keys())

    for n in nodes:
        if set(dag.children(n)) <= set(nodes):
            o = o.intersection(set(dag.get_attribute(n, 'origin')))
        else:
            raise ValueError('The "nodes" argument must be such that if it contains a node, it should also include its children.')

    p.origins = o

    p.presence = {}
    for origin in p.source.my_origins.keys():
        p.presence[origin] = float('inf')
        for n in nodes:
            if origin in p.source.get_attribute(n,'origin'):
                p.presence[origin] = min(p.presence[origin], p.source.get_attribute(n, 'origin')[origin])
            else:
                p.presence[origin]=0
    return p


# --------------------------------------------------------------

def extract_fost(dag, origins=False, threshold=0):
    """
    This function returns a list of all FoST objects in the source DAG, respecting some constraints during the enumeration.

    :param dag: Dag
        The source DAG for all FoST objects
    :param origins: bool, optional(default=`False`)
        Indicates whether the FoST should be relevant with respect to the origins in the source DAG. If `True`, any FoST object with an empty set of origins won't be enumerated.
    :param threshold: float, optional(default=`0)
        Due the combinatorial explosion of the objects to enumerate, this attributes only enumerates the FoST objects that appears in at least `threshold` percent of the origin of the source DAG. Note that this attribute should be passed only if `origin=True`. As it represents a percentage, it should be between 0 (no restriction during the enumeration) and 1 (nothing is enumerated).
    :return: list of Fost
    """
    nodes_sorted = dag.nodes_sorted_by('height')

    p = create_fost(dag, dag.my_leaves[0])
    p._Fost__find_candidates(nodes_sorted)

    to_explore = [p]

    patterns = []

    while len(to_explore) > 0:
        p = to_explore.pop()
        if not (origins and len(p.origins) == 0):
            if not (origins and len(p.origins) / len(p.source.my_origins.keys()) <= threshold):
                patterns.append(p)
                for candidate in p.candidates:
                    pp = p.copy()
                    pp._Fost__update_fost(candidate, nodes_sorted)
                    to_explore.append(pp)

    return patterns

# -------------------------------------------------------------
